﻿namespace Contract.DataAccessLayer.EF
{
    using Contract.Infrastructure.DataContracts.Sprint;

    using Microsoft.EntityFrameworkCore;

    public class WebServiceContext : DbContext
    {
        public WebServiceContext(DbContextOptions<WebServiceContext> options)
            : base(options)
        {

        }

        public DbSet<SprintModel> SprintModels { get; set; }

        public DbSet<TaskModel> TaskModels { get; set; }
        
        public DbSet<PaginatorModel> PaginatorModels { get; set; }

        public DbSet<UrlModel> UrlModels { get; set; }

        public DbSet<UrlResultModel> UrlResultModels { get; set; }

        public DbSet<TechnologyModel> TechnologyModels  { get; set; }

        public DbSet<TechnologyFilterModel> TechnologyFilterModels { get; set; }

        public DbSet<TechnologySiteDataModel> TechnologySiteDataModels { get; set; }

        public DbSet<SiteDataModel> SiteDataModels { get; set; }

        public DbSet<SiteResponsiveModel> SiteResponsiveModels { get; set; }
    }
}