﻿namespace Contract.DataAccessLayer.EF
{
    using Contract.DataAccessLayer.Models;

    using Microsoft.EntityFrameworkCore;

    public class ProxyContext : DbContext
    {
        public ProxyContext(DbContextOptions<ProxyContext> options)
            : base(options)
        {
            
        }

        public DbSet<LocationModel> LocationModels { get; set; }

        public DbSet<ProxyModel> ProxyModels { get; set; }

        public DbSet<SiteModel> SiteModels { get; set; }

        public DbSet<IpRange> IpRanges { get; set; }
    }
}