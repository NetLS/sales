﻿namespace Contract.DataAccessLayer.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Common;
    using System.Data.SqlClient;

    using Contract.DataAccessLayer.EF;
    using Contract.DataAccessLayer.Models;
    using Contract.Infrastructure.Repositories;

    using Microsoft.SqlServer.Server;

    public class ProxyRepository : IProxyRepository
    {
        private ProxyContext context;

        private BaseDataAccess baseDataAccess;

        public ProxyRepository(ProxyContext context, BaseDataAccess baseDataAccess)
        {
            this.context = context;

            this.baseDataAccess = baseDataAccess;
        }

        public IEnumerable<ProxyModel> GetAll(ProxyFilterModel filterModel)
        {
            var proxyModels = new List<ProxyModel>();

            var dataRecordsSiteId = new List<SqlDataRecord>();

            var sqlMetaDataSiteId = new SqlMetaData[1];
            sqlMetaDataSiteId[0] = new SqlMetaData("siteId", SqlDbType.Int);
            if (filterModel.SiteIds != null)
            {
                foreach (var item in filterModel.SiteIds)
                {
                    var row = new SqlDataRecord(sqlMetaDataSiteId);
                    row.SetValues(new object[] { item });
                    dataRecordsSiteId.Add(row);
                }
            }
           
            var dataRecordsLocationId = new List<SqlDataRecord>();

            var sqlMetaDataLocationId = new SqlMetaData[1];
            sqlMetaDataLocationId[0] = new SqlMetaData("locateId", SqlDbType.Int);
            if (filterModel.LocateIds != null)
            {
                foreach (var item in filterModel.LocateIds)
                {
                    var row = new SqlDataRecord(sqlMetaDataLocationId);
                    row.SetValues(new object[] { item });
                    dataRecordsLocationId.Add(row);
                }
            }
            

            var dataRecordsTypeAnonymity = new List<SqlDataRecord>();

            var sqlMetaDataTypeAnonymity = new SqlMetaData[1];
            sqlMetaDataTypeAnonymity[0] = new SqlMetaData("typeAnonymity", SqlDbType.Int);

            if (filterModel.TypeAnonymity != null)
            {
                foreach (var item in filterModel.TypeAnonymity)
                {
                    var row = new SqlDataRecord(sqlMetaDataTypeAnonymity);
                    row.SetValues(new object[] { item });
                    dataRecordsTypeAnonymity.Add(row);
                }
            }
            
            var parameterList = new List<DbParameter>();

            if (dataRecordsSiteId.Count != 0)
            {
                parameterList.Add(new SqlParameter("@siteIds", SqlDbType.Structured) { SqlValue = dataRecordsSiteId });
            }
            
            if (dataRecordsLocationId.Count != 0)
            {
                parameterList.Add(
                    new SqlParameter("@locateIds", SqlDbType.Structured) { SqlValue = dataRecordsLocationId });
            }

            if (dataRecordsTypeAnonymity.Count != 0)
            {
                parameterList.Add(
                    new SqlParameter("@typeAnonymity", SqlDbType.Structured) { SqlValue = dataRecordsTypeAnonymity });
            }

            parameterList.Add(
                new SqlParameter("@isHttpsAllowed", SqlDbType.Bit) { SqlValue = filterModel.IsHttpAllowed });
            parameterList.Add(new SqlParameter("@limit", SqlDbType.Int) { SqlValue = filterModel.limit });
            parameterList.Add(new SqlParameter("@offset", SqlDbType.Int) { SqlValue = filterModel.offset });

            using (var dataReader = this.baseDataAccess.GetDataReader(
                "dbo.GetProxies",
                parameterList,
                CommandType.StoredProcedure))
            {
                if (dataReader != null && dataReader.HasRows)
                {
                    while (dataReader.Read())
                    {
                        try
                        {
                            var proxyModel = new ProxyModel();
                            proxyModel.SiteId = (int)dataReader["SiteId"];
                            proxyModel.ProxyIp = (string)dataReader["ProxyIp"];
                            proxyModel.ProxyPort = (int)dataReader["ProxyPort"];
                            proxyModel.Location = (int?)(dataReader["Location"] == DBNull.Value
                                                             ? null
                                                             : dataReader["Location"]);
                            proxyModel.Created =
                                (DateTime?)(dataReader["Created"] == DBNull.Value ? null : dataReader["Created"]);
                            proxyModel.IsHttpsAllowed = (bool)dataReader["IsHttpsAllowed"];
                            proxyModel.TypeAnonymity = Convert.ToInt32(dataReader["TypeAnonymity"]);

                            proxyModels.Add(proxyModel);
                        }
                        catch (Exception e)
                        {
                            throw;
                        }
                    }
                }
            }

            return proxyModels;
        }

        public void CreateOrUpdate(IEnumerable<AddDeleteProxyModel> proxyModels)
        {
            var dataRecords = new List<SqlDataRecord>();

            var sqlMetaData = new SqlMetaData[5];

            sqlMetaData[0] = new SqlMetaData("ip", SqlDbType.NVarChar, 15);
            sqlMetaData[1] = new SqlMetaData("port", SqlDbType.Int);
            sqlMetaData[2] = new SqlMetaData("siteId", SqlDbType.Int);
            sqlMetaData[3] = new SqlMetaData("isHttpsAllowed", SqlDbType.Bit);
            sqlMetaData[4] = new SqlMetaData("typeAnonymity", SqlDbType.Int);

            foreach (var item in proxyModels)
            {
                var row = new SqlDataRecord(sqlMetaData);
                row.SetValues(
                    new object[]
                        { item.Ip, item.Port, item.SiteId, item.IsHttpsAllowed.Value, item.TypeAnonymity.Value });
                dataRecords.Add(row);
            }

            var parameterList = new List<DbParameter>();
            parameterList.Add(
                new SqlParameter(
                    "@proxyList",
                    SqlDbType.Structured) { Value = dataRecords, TypeName = "dbo.ProxyList" });

            this.baseDataAccess.ExecuteNonQuery("dbo.CreateOrUpdateProxyList", parameterList, CommandType.StoredProcedure);
        }

        public void Delete(IEnumerable<AddDeleteProxyModel> proxyModels)
        {
            var dataRecords = new List<SqlDataRecord>();

            var sqlMetaData = new SqlMetaData[3];
            sqlMetaData[0] = new SqlMetaData("ip", SqlDbType.NVarChar, 15);
            sqlMetaData[1] = new SqlMetaData("port", SqlDbType.Int);
            sqlMetaData[2] = new SqlMetaData("siteId", SqlDbType.Int);

            foreach (var item in proxyModels)
            {
                var row = new SqlDataRecord(sqlMetaData);
                row.SetValues(new object[] { item.Ip, item.Port, item.SiteId });
                dataRecords.Add(row);
            }

            var parameterList = new List<DbParameter>();
            parameterList.Add(new SqlParameter("@proxyList", SqlDbType.Structured) { SqlValue = dataRecords });

            this.baseDataAccess.ExecuteNonQuery("dbo.DeleteProxyList", parameterList, CommandType.StoredProcedure);
        }

        public void CreateOrUpdate(AddDeleteProxyModel proxyModel)
        {
            var parameterList = new List<DbParameter>();
            parameterList.Add(new SqlParameter("@ip", SqlDbType.NVarChar, 15) { SqlValue = proxyModel.Ip });
            parameterList.Add(new SqlParameter("@locId", SqlDbType.Int) { SqlValue = proxyModel.Port });
            parameterList.Add(new SqlParameter("@countProxy", SqlDbType.Int) { SqlValue = proxyModel.SiteId });
            parameterList.Add(
                new SqlParameter("@isHttpsAllowed", SqlDbType.Bit)
                    {
                        SqlValue =
                            proxyModel.IsHttpsAllowed != null
                            && proxyModel.IsHttpsAllowed.Value
                    });
            parameterList.Add(
                new SqlParameter("@typeAnonymity", SqlDbType.TinyInt)
                    {
                        SqlValue =
                            proxyModel.TypeAnonymity.HasValue
                                ? proxyModel.TypeAnonymity.Value
                                : 0
                    });

            this.baseDataAccess.ExecuteNonQuery("dbo.CreateOrUpdateProxy", parameterList, CommandType.StoredProcedure);
        }

        public void Delete(AddDeleteProxyModel proxyModel)
        {
            var parameterList = new List<DbParameter>();
            parameterList.Add(new SqlParameter("@ip", SqlDbType.NVarChar, 15) { SqlValue = proxyModel.Ip });
            parameterList.Add(new SqlParameter("@locId", SqlDbType.Int) { SqlValue = proxyModel.Port });
            parameterList.Add(new SqlParameter("@countProxy", SqlDbType.Int) { SqlValue = proxyModel.SiteId });

            this.baseDataAccess.ExecuteNonQuery("dbo.DeleteProxy", parameterList, CommandType.StoredProcedure);
        }
    }
}