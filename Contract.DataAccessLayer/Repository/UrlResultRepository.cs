﻿namespace Contract.DataAccessLayer.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Common;
    using System.Data.SqlClient;
    using System.Linq;

    using Contract.Infrastructure.DataContracts.SiteAnalyzer;
    using Contract.Infrastructure.DataContracts.Sprint;
    using Contract.Infrastructure.Repositories;

    using Microsoft.SqlServer.Server;

    public class UrlResultRepository : IUrlResultRepository
    {
        private BaseDataAccess baseDataAccess;

        public UrlResultRepository(BaseDataAccess baseDataAccess)
        {
            this.baseDataAccess = baseDataAccess;
        }

        public void Create(IEnumerable<UrlResultModel> resultModels)
        {
            var urlResultDataRecords = new List<SqlDataRecord>();
            var siteDataRecords = new List<SqlDataRecord>();
            var siteResponsiveRecords = new List<SqlDataRecord>();

            var urlResultMetaData = new SqlMetaData[9];

            urlResultMetaData[0] = new SqlMetaData("urlResultId", SqlDbType.Int);
            urlResultMetaData[1] = new SqlMetaData("taskId", SqlDbType.Int);
            urlResultMetaData[2] = new SqlMetaData("paginatorId", SqlDbType.Int);
            urlResultMetaData[3] = new SqlMetaData("siteDataId", SqlDbType.Bit);
            urlResultMetaData[4] = new SqlMetaData("urlLink", SqlDbType.NVarChar, -1);
            urlResultMetaData[5] = new SqlMetaData("[status]", SqlDbType.Int);
            urlResultMetaData[6] = new SqlMetaData("urlId", SqlDbType.Int);
            urlResultMetaData[7] = new SqlMetaData("isMatch", SqlDbType.Bit);
            urlResultMetaData[8] = new SqlMetaData("countUrl", SqlDbType.Int);

            var resultList = resultModels.ToList();

            for (int i = 0; i < resultList.Count; i++)
            {
                var row = new SqlDataRecord(urlResultMetaData);
                row.SetValues(
                    new object[]
                        {
                            i, resultList[i].TaskId, resultList[i].PaginatorId, resultList[i].SiteDataId,
                            resultList[i].UrlLink, resultList[i].Status, resultList[i].UrlId,
                            resultList[i].IsMatch, resultList[i].CountUrl
                        });
                urlResultDataRecords.Add(row);
            }

            var siteDataMetaData = new SqlMetaData[4];

            siteDataMetaData[0] = new SqlMetaData("technologySiteDataId", SqlDbType.Int);
            siteDataMetaData[1] = new SqlMetaData("siteId", SqlDbType.Int);
            siteDataMetaData[2] = new SqlMetaData("technologyId", SqlDbType.Int);
            siteDataMetaData[3] = new SqlMetaData("[version]", SqlDbType.NVarChar, -1);

            for (int i = 0; i < resultList.Count; i++)
            {
                var list = resultList[i].TechnologySiteDataModels.ToList();
                for (int j = 0; j < list.Count; j++)
                {
                    var row = new SqlDataRecord(siteDataMetaData);
                    row.SetValues(new object[] { list[j].SiteDataId, i, list[j].TechnologyId, list[j].Version });
                    siteDataRecords.Add(row);
                }
            }

            var siteResponsiveMetaData = new SqlMetaData[8];

            siteResponsiveMetaData[0] = new SqlMetaData("siteDataId", SqlDbType.Int);
            siteResponsiveMetaData[1] = new SqlMetaData("alertStatus", SqlDbType.TinyInt);
            siteResponsiveMetaData[2] = new SqlMetaData("docWidthStatus", SqlDbType.TinyInt);
            siteResponsiveMetaData[3] = new SqlMetaData("flashStatus", SqlDbType.TinyInt);
            siteResponsiveMetaData[4] = new SqlMetaData("fontSizeStatus", SqlDbType.TinyInt);
            siteResponsiveMetaData[5] = new SqlMetaData("touchScreenStatus", SqlDbType.TinyInt);
            siteResponsiveMetaData[6] = new SqlMetaData("viewPortStatus", SqlDbType.TinyInt);
            siteResponsiveMetaData[7] = new SqlMetaData("screenShot", SqlDbType.NVarChar, 200);

            for (int i = 0; i < resultList.Count; i++)
            {
                var list = resultList[i].SiteResponsive.ToList();
                for (int j = 0; j < list.Count; j++)
                {
                    var row = new SqlDataRecord(siteResponsiveMetaData);
                    row.SetValues(new object[] { i, list[j].Alert, list[j].DocWidth, list[j].Flash, list[j].FontSize, list[j].TouchScreen, list[j].ViewPort, list[j].ScreenShot });
                    siteResponsiveRecords.Add(row);
                }
            }

            var parameterList = new List<DbParameter>();
            parameterList.Add(
                new SqlParameter("@urlResultList", SqlDbType.Structured)
                    {
                        Value = urlResultDataRecords,
                        TypeName = "dbo.UrlResultList"
                    });
            parameterList.Add(
                new SqlParameter("@siteDataList", SqlDbType.Structured)
                    {
                        Value = siteDataRecords,
                        TypeName = "dbo.SiteDataList"
                });
            parameterList.Add(
                new SqlParameter("@siteResponsiveList", SqlDbType.Structured)
                    {
                        Value = siteResponsiveRecords,
                        TypeName = "dbo.SiteResponsiveList"
                });

            this.baseDataAccess.ExecuteNonQuery("dbo.CreateUrlResultList", parameterList, CommandType.StoredProcedure);
        }

        public IEnumerable<UrlResultModel> Get(UrlResultFilterModel filter, out int count)
        {
            count = 0;

            var technologyDataRecords = (filter?.TechnologyFilterViewModels != null && filter.TechnologyFilterViewModels.Count > 0) ? new List<SqlDataRecord>() : null;

            var techVerFilter = new SqlMetaData[2];

            techVerFilter[0] = new SqlMetaData("technologyId", SqlDbType.Int);
            techVerFilter[1] = new SqlMetaData("version", SqlDbType.NVarChar, -1);

            for (int i = 0; i < filter?.TechnologyFilterViewModels?.Count; i++)
            {
                var row = new SqlDataRecord(techVerFilter);
                row.SetValues(
                    new object[]
                        {
                            filter.TechnologyFilterViewModels[i].TechnologyId, filter.TechnologyFilterViewModels[i].VersionJson
                        });
                technologyDataRecords?.Add(row);
            }

            var taskDataRecords = (filter?.TaskIdList != null && filter.TaskIdList.Count > 0) ? new List<SqlDataRecord>() : null;
            
            var taskFilter = new SqlMetaData[1];

            taskFilter[0] = new SqlMetaData("taskId", SqlDbType.Int);

            for (int i = 0; i < filter?.TaskIdList?.Count; i++)
            {
                var row = new SqlDataRecord(taskFilter);
                row.SetValues(
                    new object[]
                        {
                            filter.TaskIdList[i]
                        });
                taskDataRecords?.Add(row);
            }

            var urlDataRecords = (filter?.UrlList != null && filter.UrlList.Count > 0) ? new List<SqlDataRecord>() : null;

            var urlFilter = new SqlMetaData[1];

            urlFilter[0] = new SqlMetaData("url", SqlDbType.NVarChar, -1);

            for (int i = 0; i < filter?.UrlList?.Count; i++)
            {
                var row = new SqlDataRecord(urlFilter);
                row.SetValues(
                    new object[]
                        {
                            filter.UrlList[i]
                        });
                urlDataRecords?.Add(row);
            }

            var parameterList = new List<DbParameter>();
            parameterList.Add(
                new SqlParameter("@techFilterList", SqlDbType.Structured)
                    {
                        Value = technologyDataRecords,
                        TypeName = "dbo.TechFilter"
                });

            parameterList.Add(
                new SqlParameter("@taskIdList", SqlDbType.Structured)
                    {
                        Value = taskDataRecords,
                        TypeName = "dbo.TaskFilter"
                });

            parameterList.Add(
                new SqlParameter("@urlList", SqlDbType.Structured)
                    {
                        Value = urlDataRecords,
                        TypeName = "dbo.UrlFilter"
                });

            parameterList.Add(new SqlParameter("@countUrlStart", SqlDbType.Int) { SqlValue = filter?.CountUrlStart });
            parameterList.Add(new SqlParameter("@countUrlEnd", SqlDbType.Int) { SqlValue = filter?.CountUrlEnd });
            parameterList.Add(new SqlParameter("@checkDateStart", SqlDbType.DateTime) { SqlValue = filter?.CheckDateStart });
            parameterList.Add(new SqlParameter("@checkDateEnd", SqlDbType.DateTime) { SqlValue = filter?.CheckDateEnd });
            parameterList.Add(new SqlParameter("@limit", SqlDbType.Int) { SqlValue = filter?.limit });
            parameterList.Add(new SqlParameter("@offset", SqlDbType.Int) { SqlValue = filter?.offset });

            if (filter?.SiteResponsiveModel?.AlertStatus != AlertStatus.Unknown)
            {
                parameterList.Add(new SqlParameter("@alertStatus", SqlDbType.TinyInt) { SqlValue = filter?.SiteResponsiveModel?.Alert });
            }

            if (filter?.SiteResponsiveModel?.DocWidthStatus != DocWidthStatus.Unknown)
            {
                parameterList.Add(new SqlParameter("@docWidth", SqlDbType.TinyInt) { SqlValue = filter?.SiteResponsiveModel?.DocWidth });
            }

            if (filter?.SiteResponsiveModel?.FlashStatus != FlashStatus.Unknown)
            {
                parameterList.Add(new SqlParameter("@flash", SqlDbType.TinyInt) { SqlValue = filter?.SiteResponsiveModel?.Flash });
            }

            if (filter?.SiteResponsiveModel?.FontSizeStatus != FontSizeStatus.Unknown)
            {
                parameterList.Add(new SqlParameter("@fontSize", SqlDbType.TinyInt) { SqlValue = filter?.SiteResponsiveModel?.FontSize });
            }

            if (filter?.SiteResponsiveModel?.TouchScreenStatus != TouchScreenStatus.Unknown)
            {
                parameterList.Add(new SqlParameter("@touchScreen", SqlDbType.TinyInt) { SqlValue = filter?.SiteResponsiveModel?.TouchScreen });
            }

            if (filter?.SiteResponsiveModel?.PortStatus != ViewPortStatus.Unknown)
            {
                parameterList.Add(new SqlParameter("@viewPort", SqlDbType.TinyInt) { SqlValue = filter?.SiteResponsiveModel?.ViewPort });
            }

            var listUrlResult = new List<UrlResultModel>();

            using (var dataReader = this.baseDataAccess.GetDataReader(
                "[dbo].[SearchSite]",
                parameterList,
                CommandType.StoredProcedure))
            {
                if (dataReader != null && dataReader.HasRows)
                {
                    while (dataReader.Read())
                    {
                        try
                        {
                            var urlResultModel = new UrlResultModel();
                            urlResultModel.UrlResultId = (int)dataReader["UrlResultId"];

                            urlResultModel.TaskId = (int?)(dataReader["TaskId"] == DBNull.Value
                                                                  ? null
                                                                  : dataReader["TaskId"]);

                            urlResultModel.PaginatorId = (int?)(dataReader["PaginatorId"] == DBNull.Value
                                                               ? null
                                                               : dataReader["PaginatorId"]);

                            urlResultModel.SiteDataId = (int?)(dataReader["SiteDataId"] == DBNull.Value
                                                                    ? null
                                                                    : dataReader["SiteDataId"]);

                            urlResultModel.UrlLink = (string)(dataReader["UrlLink"] == DBNull.Value
                                                                   ? null
                                                                   : dataReader["UrlLink"]);

                            urlResultModel.Status = (int?)(dataReader["Status"] == DBNull.Value
                                                                   ? null
                                                                   : dataReader["Status"]);

                            urlResultModel.UrlId = (int?)(dataReader["UrlId"] == DBNull.Value
                                                               ? null
                                                               : dataReader["UrlId"]);

                            urlResultModel.IsMatch = (bool?)(dataReader["IsMatch"] == DBNull.Value
                                                              ? null
                                                              : dataReader["IsMatch"]);
                            count = (int)dataReader["Count"];

                            listUrlResult.Add(urlResultModel);
                        }
                        catch (Exception e)
                        {
                            // Need log, something wrong in db
                            throw;
                        }
                    }
                }
            }

            return listUrlResult;

        }
    }
}