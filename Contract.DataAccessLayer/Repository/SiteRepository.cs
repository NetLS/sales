﻿namespace Contract.DataAccessLayer.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Common;
    using System.Data.SqlClient;

    using Contract.DataAccessLayer.EF;
    using Contract.DataAccessLayer.Models;
    using Contract.Infrastructure.Repositories;

    public class SiteRepository : ISiteRepository
    {
        private ProxyContext context;

        private BaseDataAccess baseDataAccess;

        public SiteRepository(ProxyContext context, BaseDataAccess baseDataAccess)
        {
            this.context = context;

            this.baseDataAccess = baseDataAccess;
        }

        public IEnumerable<SiteModel> GetAll(SiteFilterModel filterModel)
        {
            var siteModels = new List<SiteModel>();

            var parameterList = new List<DbParameter>();

            parameterList.Add(new SqlParameter("@limit", SqlDbType.Int) { SqlValue = filterModel.limit });
            parameterList.Add(new SqlParameter("@offset", SqlDbType.Int) { SqlValue = filterModel.offset });

            using (var dataReader = this.baseDataAccess.GetDataReader(
                "dbo.GetSites",
                parameterList,
                CommandType.StoredProcedure))
            {
                if (dataReader != null && dataReader.HasRows)
                {
                    while (dataReader.Read())
                    {
                        try
                        {
                            var siteModel = new SiteModel();
                            siteModel.SiteId = (int)dataReader["SiteId"];
                            siteModel.Url = (string)dataReader["Url"];
                            siteModel.XPath = (string)dataReader["XPath"];

                            siteModel.FirstProxyIp = (string)(dataReader["FirstProxyIp"] == DBNull.Value
                                                                ? null
                                                                : dataReader["FirstProxyIp"]);
                            siteModel.FirstProxyPort =
                                (int?)(dataReader["FirstProxyPort"] == DBNull.Value
                                           ? null
                                           : dataReader["FirstProxyPort"]);
                            siteModel.NextCheck = (DateTime)dataReader["NextCheck"];
                            siteModel.Interval = (int)dataReader["Interval"];

                            siteModel.MoveTemplate = (string)(dataReader["MoveTemplate"] == DBNull.Value
                                                                ? null
                                                                : dataReader["MoveTemplate"]);

                            siteModel.MoveStep = (int?)(dataReader["MoveStep"] == DBNull.Value
                                                            ? null
                                                            : dataReader["MoveStep"]);

                            siteModel.MoveFinish = (int?)(dataReader["MoveFinish"] == DBNull.Value
                                                              ? null
                                                              : dataReader["MoveFinish"]);
                            siteModel.TypeContent = (int)dataReader["TypeContent"];
                            siteModel.Active = (bool)dataReader["Active"];
                            siteModel.IsJSNeed = (bool)dataReader["IsJSNeed"];

                            siteModels.Add(siteModel);
                        }
                        catch (Exception e)
                        {
                            // Need log, something wrong in db
                            throw;
                        }
                    }
                }
            }

            return siteModels;
        }

        public void Create(SiteModel siteModel)
        {
            var parameterList = new List<DbParameter>();
            parameterList.Add(new SqlParameter("@Url", SqlDbType.NVarChar) { SqlValue = siteModel.Url });
            parameterList.Add(new SqlParameter("@XPath", SqlDbType.NVarChar) { SqlValue = siteModel.XPath });
            parameterList.Add(new SqlParameter("@FirstProxyIp", SqlDbType.NVarChar) { SqlValue = siteModel.FirstProxyIp });
            parameterList.Add(new SqlParameter("@FirstProxyPort", SqlDbType.Int) { SqlValue = siteModel.FirstProxyPort });
            parameterList.Add(new SqlParameter("@NextCheck", SqlDbType.DateTime) { SqlValue = siteModel.NextCheck });
            parameterList.Add(new SqlParameter("@Interval", SqlDbType.Int) { SqlValue = siteModel.Interval });
            parameterList.Add(new SqlParameter("@MoveTemplate", SqlDbType.NVarChar) { SqlValue = siteModel.MoveTemplate });
            parameterList.Add(new SqlParameter("@MoveStep", SqlDbType.Int) { SqlValue = siteModel.MoveStep });
            parameterList.Add(new SqlParameter("@MoveFinish", SqlDbType.Int) { SqlValue = siteModel.MoveFinish });
            parameterList.Add(new SqlParameter("@TypeContent", SqlDbType.Int) { SqlValue = siteModel.TypeContent });
            parameterList.Add(new SqlParameter("@Active", SqlDbType.Bit) { SqlValue = siteModel.Active });
            parameterList.Add(new SqlParameter("@IsJSNeed", SqlDbType.Bit) { SqlValue = siteModel.IsJSNeed });

            this.baseDataAccess.ExecuteNonQuery(
                "dbo.CreateSite",
                parameterList,
                CommandType.StoredProcedure);
        }

        public void Update(SiteModel siteModel)
        {
            var parameterList = new List<DbParameter>();
            parameterList.Add(new SqlParameter("@SiteId", SqlDbType.Int) { SqlValue = siteModel.SiteId });
            parameterList.Add(new SqlParameter("@Url", SqlDbType.NVarChar) { SqlValue = siteModel.Url });
            parameterList.Add(new SqlParameter("@XPath", SqlDbType.NVarChar) { SqlValue = siteModel.XPath });
            parameterList.Add(new SqlParameter("@FirstProxyIp", SqlDbType.NVarChar) { SqlValue = siteModel.FirstProxyIp });
            parameterList.Add(new SqlParameter("@FirstProxyPort", SqlDbType.Int) { SqlValue = siteModel.FirstProxyPort });
            parameterList.Add(new SqlParameter("@NextCheck", SqlDbType.DateTime) { SqlValue = siteModel.NextCheck });
            parameterList.Add(new SqlParameter("@Interval", SqlDbType.Int) { SqlValue = siteModel.Interval });
            parameterList.Add(new SqlParameter("@MoveTemplate", SqlDbType.NVarChar) { SqlValue = siteModel.MoveTemplate });
            parameterList.Add(new SqlParameter("@MoveStep", SqlDbType.Int) { SqlValue = siteModel.MoveStep });
            parameterList.Add(new SqlParameter("@MoveFinish", SqlDbType.Int) { SqlValue = siteModel.MoveFinish });
            parameterList.Add(new SqlParameter("@TypeContent", SqlDbType.Int) { SqlValue = siteModel.TypeContent });
            parameterList.Add(new SqlParameter("@Active", SqlDbType.Bit) { SqlValue = siteModel.Active });
            parameterList.Add(new SqlParameter("@IsJSNeed", SqlDbType.Bit) { SqlValue = siteModel.IsJSNeed });

            this.baseDataAccess.ExecuteNonQuery(
                "dbo.UpdateSite",
                parameterList,
                CommandType.StoredProcedure);
        }

        public void Delete(int id)
        {
            var parameterList = new List<DbParameter>();
            parameterList.Add(new SqlParameter("@siteId", SqlDbType.Int) { SqlValue = id });

            this.baseDataAccess.ExecuteNonQuery(
                "dbo.DeleteSite",
                parameterList,
                CommandType.StoredProcedure);
        }

        public void Update(int id)
        {
            var parameterList = new List<DbParameter>();
            parameterList.Add(new SqlParameter("@siteId", SqlDbType.Int) { SqlValue = id });

            this.baseDataAccess.ExecuteNonQuery(
                "dbo.UpdateSiteCheck",
                parameterList,
                CommandType.StoredProcedure);
        }
    }
}