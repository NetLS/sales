﻿namespace Contract.DataAccessLayer.Repository
{
    using System;

    using Contract.DataAccessLayer.EF;
    using Contract.DataAccessLayer.Interfaces;
    using Contract.Infrastructure.Repositories;

    using Microsoft.EntityFrameworkCore;

    public class EFUnitOfWork : IUnitOfWork
    {
        private ProxyRepository proxyRepository;

        private SiteRepository siteRepository;

        private ProxyContext context;

        private BaseDataAccess baseDataAccess;

        private bool disposed = false;

        public EFUnitOfWork(string connectionString)
        {
            var optionsBuilder = new DbContextOptionsBuilder<ProxyContext>();
            optionsBuilder.UseSqlServer(connectionString);

            this.context = new ProxyContext(optionsBuilder.Options);
            this.baseDataAccess = new BaseDataAccess(connectionString);
        }

        public IProxyRepository ProxyRepository
        {
            get
            {
                return this.proxyRepository ?? (this.proxyRepository =
                                                    new ProxyRepository(this.context, this.baseDataAccess));
            }
        }

        public ISiteRepository SiteRepository
        {
            get
            {
                return this.siteRepository ?? (this.siteRepository =
                                                   new SiteRepository(this.context, this.baseDataAccess));
            }
        }

        public void Save()
        {
            this.context.SaveChanges();
        }

        public virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    this.context.Dispose();
                }
                this.disposed = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}