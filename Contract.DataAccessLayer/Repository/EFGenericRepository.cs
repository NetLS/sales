﻿namespace Contract.DataAccessLayer.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;

    using Contract.DataAccessLayer.EF;
    using Contract.DataAccessLayer.Interfaces;

    using Microsoft.EntityFrameworkCore;

    public class EFGenericRepository<T> : IRepository<T> where T : class
    {
        DbContext _context;
        DbSet<T> _dbSet;

        public EFGenericRepository(WebServiceContext context)
        {
            _context = context;
            _dbSet = context.Set<T>();
        }

        public IEnumerable<T> Get(params Expression<Func<T, object>>[] properties)
        {
            if (properties == null)
                return _dbSet.AsNoTracking().ToList();

            var query = _dbSet as IQueryable<T>;

            query = properties
                .Aggregate(query, (current, property) => current.Include(property));

            return query.AsNoTracking().ToList();
        }

        public IEnumerable<T> Get(Func<T, bool> predicate, params Expression<Func<T, object>>[] properties)
        {
            if (properties == null)
                _dbSet.AsNoTracking().Where(predicate).ToList();

            var query = _dbSet as IQueryable<T>; 

            query = properties
                .Aggregate(query, (current, property) => current.Include(property));

            return query.AsNoTracking().Where(predicate).ToList();
        }
        public T FindById(int id, params Expression<Func<T, object>>[] properties)
        {
            if (properties == null)
                return _dbSet.Find(id);

            var query = _dbSet as IQueryable<T>;
            var item = _dbSet.Find(id);
            query = properties
                .Aggregate(query, (current, property) => current.Include(property)).AsNoTracking();
            var ctx1 = query.AsNoTracking().Where(arg => arg == item).ToList().FirstOrDefault();
            return ctx1;
        }

        public void Add(T item)
        {
            _dbSet.Add(item);
            _context.SaveChanges();
        }
        public void Edit(T item)
        {
            _context.Entry(item).State = EntityState.Modified;
            _context.SaveChanges();
        }
        public void Delete(T item)
        {
            _dbSet.Remove(item);
            _context.SaveChanges();
        }
    }
}