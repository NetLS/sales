﻿using System.Collections.Generic;
using System.Web.Mvc;

namespace NetworkInfo.Controllers
{
    public class TestController : Controller
    {
        // GET: Test
        public ActionResult Proxy()
        {
            List<string> list = new List<string>();
            foreach (string s in this.Request.ServerVariables)
            {
                if ((s.Contains("REMOTE") || s.Contains("HTTP") || s.Contains("REQUEST")) && !s.Contains("ALL_HTTP") && !string.IsNullOrEmpty(this.Request.ServerVariables.Get(s)))
                {
                    list.Add(s + " = " + this.Request.ServerVariables.Get(s));
                }
            }

            return View(list);
        }
    }
}