﻿namespace Contract.Infrastructure.DataContracts.SiteAnalyzer
{
    public enum AlertStatus
    {
        Unknown,
        AlertDetected,
        Done
    }
}