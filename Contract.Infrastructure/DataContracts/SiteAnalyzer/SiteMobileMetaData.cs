﻿namespace Contract.Infrastructure.DataContracts.SiteAnalyzer
{
    using System.Collections.Generic;
    using System.Drawing;

    public class SiteMobileMetaData
    {
        public DocWidthStatus DocWidthStatus { get; set; }

        public FontSizeStatus FontSizeStatus { get; set; }

        public TouchScreenStatus TouchScreenStatus { get; set; }

        public ViewPortStatus PortStatus { get; set; }

        public AlertStatus AlertStatus { get; set; }

        public FlashStatus FlashStatus { get; set; }

        public byte[] ImageScreenShot { get; set; }

    }
}