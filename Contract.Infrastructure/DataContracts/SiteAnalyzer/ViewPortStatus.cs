﻿namespace Contract.Infrastructure.DataContracts.SiteAnalyzer
{
    public enum ViewPortStatus
    {
        Unknown,
        NoViewPortDeclared,
        ContentMissed,
        ResponsiveWidth,
        HardCodedWidth
    }
}