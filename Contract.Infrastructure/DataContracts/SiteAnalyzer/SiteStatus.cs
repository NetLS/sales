﻿namespace Contract.Infrastructure.DataContracts.SiteAnalyzer
{
    public enum SiteStatus
    {
        NotAvailable,
        Available
    }
}