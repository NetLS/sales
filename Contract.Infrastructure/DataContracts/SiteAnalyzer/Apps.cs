﻿namespace Contract.Infrastructure.DataContracts.SiteAnalyzer
{
    using System.Collections.Generic;

    public class Apps
    {
        public IList<string> Cats { get; set; }
        public IDictionary<string, string> Headers { get; set; }
        public IList<string> Env { get; set; }
        public string Url { get; set; }
        public IList<string> Html { get; set; }
        public string Icon { get; set; }
        public IList<string> Implies { get; set; }
        public IDictionary<string, string> Meta { get; set; }
        public IList<string> Script { get; set; }
        public IList<string> Exclude { get; set; }
        public string Website { get; set; } 
    }
}