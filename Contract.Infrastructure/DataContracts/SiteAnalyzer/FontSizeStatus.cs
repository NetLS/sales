﻿namespace Contract.Infrastructure.DataContracts.SiteAnalyzer
{
    public enum FontSizeStatus
    {
        Unknown,
        TooSmallSizeFonts,
        Done
    }
}