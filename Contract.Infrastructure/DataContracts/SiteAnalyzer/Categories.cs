﻿namespace Contract.Infrastructure.DataContracts.SiteAnalyzer
{
    public class Categories
    {
        public string Name { get; set; }

        public int Priority { get; set; }
    }
}