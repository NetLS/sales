﻿namespace Contract.Infrastructure.DataContracts.SiteAnalyzer
{
    public class AppFull
    {
        public string Name { get; set; }

        public string Version { get; set; }

        public Apps App { get; set; }
    }
}