﻿namespace Contract.Infrastructure.DataContracts.SiteAnalyzer
{
    public enum DocWidthStatus
    {
        Unknown,
        DocTooWide,
        Done
    }
}