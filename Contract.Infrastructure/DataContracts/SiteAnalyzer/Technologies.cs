﻿namespace Contract.Infrastructure.DataContracts.SiteAnalyzer
{
    using System.Collections.Generic;

    public class Technologies
    {
        public Dictionary<string, Apps> Apps { get; set; }

        public Dictionary<string, Categories> Categories { get; set; }
    }
}