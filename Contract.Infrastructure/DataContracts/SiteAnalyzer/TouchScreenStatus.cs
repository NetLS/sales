﻿namespace Contract.Infrastructure.DataContracts.SiteAnalyzer
{
    public enum TouchScreenStatus
    {
        Unknown,
        TooSmallTouchScreen,
        Done
    }
}