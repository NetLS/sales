﻿namespace Contract.Infrastructure.DataContracts.SiteAnalyzer
{
    public enum FlashStatus
    {
        Unknown,
        FlashDetected,
        Done
    }
}