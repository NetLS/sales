﻿namespace Contract.Infrastructure.DataContracts.SiteAnalyzer
{
    using System.Collections.Generic;

    public class SiteMetaData
    {
        public string Host { get; set; }

        public List<string> Urls { get; set; }

        public List<string> EnvironmentVariables { get; set; }

        public string Html { get; set; }

        public List<KeyValuePair<string, string>> Headers { get; set; }

        public List<AppFull> AppFulls { get; set; }

        public SiteStatus Status { get; set; }

        public SiteMobileMetaData SiteMobileMetaData { get; set; }
    }
}