﻿namespace Sales.Services
{
    using System;
    using System.Net;

    using Sales.Models;

    public class NetworkProxy : IWebProxy
    {
        public readonly Uri Uri;
        private readonly bool bypass;

        public NetworkProxy(Proxy proxy, ICredentials credentials = null, bool bypass = false)
        {
            this.Uri = new Uri(string.Format("http://{0}:{1}", proxy.Ip, proxy.Port));
            this.bypass = bypass;
            this.Credentials = credentials;
        }

        public ICredentials Credentials { get; set; }

        public Uri GetProxy(Uri destination) => this.Uri;

        public bool IsBypassed(Uri host) => this.bypass;

    }
}