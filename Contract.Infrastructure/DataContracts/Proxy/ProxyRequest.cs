﻿namespace Contract.Infrastructure.DataContracts.Proxy
{
    using System.Net;

    using Sales.Models;

    public class ProxyRequest
    {
        public string Url { get; set; }

        public Proxy Proxy { get; set; }

        public NetworkCredential Credential { get; set; }
    }
}