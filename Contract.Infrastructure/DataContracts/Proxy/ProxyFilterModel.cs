﻿namespace Contract.DataAccessLayer.Models
{
    using System.Collections.Generic;
    using System.Linq;

    using Sales.Models;

    public class ProxyFilterModel
    {
        public IEnumerable<int> SiteIds { get; set; }

        public IEnumerable<int> LocateIds { get; set; }

        public IEnumerable<int> TypeAnonymity { get; set; }

        public IEnumerable<ProxyAnonymity> ProxyType
        {
            get
            {
                return this.TypeAnonymity.Select(item => (ProxyAnonymity)item).ToList();
            }
            set
            {
                this.TypeAnonymity = value.Select(item => (int)item);
            }
        }

        public bool? IsHttpAllowed { get; set; }

        public int limit { get; set; }

        public int offset { get; set; }
    }
}