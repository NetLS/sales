﻿namespace Sales.Models
{
    public enum ProxyAnonymity
    {
        None, 
        Low,
        High,

        Offline
    }
}