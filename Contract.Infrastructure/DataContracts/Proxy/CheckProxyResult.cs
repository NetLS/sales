﻿namespace Sales.Models
{
    public class CheckProxyResult
    {
        public bool IsAvailable { get; set; }

        public bool IsHttpsAllowed { get; set; }

        public ProxyAnonymity ProxyAnonymity { get; set; }

        public Proxy Proxy { get; set; }
    }
}