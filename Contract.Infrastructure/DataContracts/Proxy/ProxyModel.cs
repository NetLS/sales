﻿namespace Contract.DataAccessLayer.Models
{
    using System;

    using Sales.Models;

    public class ProxyModel
    {
        public int SiteId { get; set; }

        public string ProxyIp { get; set; }

        public int ProxyPort { get; set; }

        public int? Location { get; set; }

        public DateTime? Created { get; set; }

        public bool IsHttpsAllowed { get; set; }

        public int TypeAnonymity { get; set; }

        public ProxyAnonymity ProxyAnonymity
        {
            get => (ProxyAnonymity)this.TypeAnonymity;
            set => this.TypeAnonymity = (int)value;
        }
    }
}