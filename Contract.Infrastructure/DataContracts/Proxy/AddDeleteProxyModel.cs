﻿namespace Contract.DataAccessLayer.Models
{
    public class AddDeleteProxyModel
    {
        public int SiteId { get; set; }

        public string Ip { get; set; }

        public int Port { get; set; }

        public bool? IsHttpsAllowed { get; set; }

        public int? TypeAnonymity { get; set; }
    }
}