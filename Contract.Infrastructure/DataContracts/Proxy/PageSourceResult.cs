﻿namespace Contract.Infrastructure.DataContracts.Proxy
{
    public class PageSourceResult
    {
        public string PageSource { get; set; }

        public bool IsSuccess { get; set; }
}
}