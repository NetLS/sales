﻿namespace Sales.Models
{
    public class Proxy
    {
        public string Ip { get; set; }

        public int Port { get; set; }
    }
}