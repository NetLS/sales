﻿namespace Contract.DataAccessLayer.Models
{
    public class LocationModel
    {
        public int LocationId { get; set; }

        public string CountryName { get; set; }

        public string CountryCode { get; set; }
    }
}