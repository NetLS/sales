﻿namespace Contract.DataAccessLayer.Models
{
    public class IpRange
    {
        public int IpRangeId { get; set; }

        public int LocationId { get; set; }

        public string IpFrom { get; set; }

        public string IpTo { get; set; }
    }
}