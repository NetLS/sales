﻿namespace Contract.DataAccessLayer.Models
{
    using System;
    using System.ComponentModel.DataAnnotations.Schema;

    public class SiteModel
    {
        public int SiteId { get; set; }

        public string Url { get; set; }

        public string XPath { get; set; }

        public string FirstProxyIp { get; set; }

        public int? FirstProxyPort { get; set; }

        public DateTime NextCheck { get; set; }

        public int Interval { get; set; }

        public string MoveTemplate { get; set; }

        public int? MoveStep { get; set; }

        public int? MoveFinish { get; set; }

        public int TypeContent { get; set; }

        public bool Active { get; set; }

        public bool IsJSNeed { get; set; }

        [NotMapped]
        public TypeContent Content
        {
            get => (TypeContent)(this.TypeContent);
            set => this.TypeContent = (int)value;
        }
    }
}