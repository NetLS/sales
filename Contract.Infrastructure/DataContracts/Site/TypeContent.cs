﻿namespace Contract.DataAccessLayer.Models
{
    public enum TypeContent
    {
        ConsistentData,
        DataWithSeparator,
        JsonData
    }
}