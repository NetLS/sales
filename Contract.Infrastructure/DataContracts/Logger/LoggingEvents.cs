﻿namespace Contract.Infrastructure.DataContracts.Proxy
{
    public class LoggingEvents
    {
        public const int GET_ALL_EXCEPTION = 1000;

        public const int PARSE_ALL_EXCEPTION = 2000;
        public const int PARSE_HTML_EXCEPTION_NON_EVEN_NUMBER = 2001;
        public const int PARSE_HTML_EXCEPTION_DONT_MATCH_PATTERN = 2002;
        public const int PARSE_HTML_EXCEPTION_XPATH_INVALID = 2003;
        public const int PARSE_JSON_EXCEPTION_JSONPATH_INVALID = 2004;
        public const int PARSE_JSON_EXCEPTION_DONT_MATCH_PATTERN = 2005;

        public const int PROXY_CONNECTION_ALL_EXCEPTION = 3000;
        public const int PROXY_CONNECTION_UNAVAILABLE = 3001;
        public const int CANNOT_GET_ACCESS_TO_SITE = 3002;

        public const int PROXY_CRUD_ALL_EXCEPTION = 4000;

        public const int APPS_LOAD_ALL_EXCEPTION = 5000;
        public const int APPS_LOAD_FILE_ERROR = 5001;
        public const int APPS_LOAD_PARSE_ERROR = 5002;
    }
}