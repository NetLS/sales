﻿namespace Sales
{
    public class NetworkSettings
    {
        public string LocalIp { get; set; }

        public int CountCheckProxy { get; set; }

        public string UrlForTestProxy { get; set; }

        public string UrlForTestHttps { get; set; }

        public int Timeout { get; set; }

        public int ProxyTimeout { get; set; }

        public string DriverPath { get; set; }

        public int CountCheckPageSource { get; set; }

        public string HeadersJs { get; set; }

        public string EnvironmentVariableJs { get; set; }
    }
}