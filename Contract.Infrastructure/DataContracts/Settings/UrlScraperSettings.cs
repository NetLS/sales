﻿namespace Sales
{
    public class UrlScraperSettings
    {
        public string ScreenShotPath { get; set; }

        public int CountProxyForScrapeSearchPlatform { get; set; }

        public int CountScrapeSiteThread { get; set; }

        public int CountTryToScrapeSite { get; set; }
    }
}