﻿namespace Sales
{
    using System.Collections.Generic;

    public class DataDotComSettings
    {
        public string DriverPath { get; set; }

        public int Timeout { get; set; }

        public string LoginUrl { get; set; }

        public string HomeUrl { get; set; }

        public string SearchUrl { get; set; }

        public string Login { get; set; }

        public string Password { get; set; }


        public string LoginSetUp { get; set; }

        public string PasswordSetUp { get; set; }

        public string CheckBoxSetUp { get; set; }

        public string ButtonSetUp { get; set; }


        public IEnumerable<string> LocationXPath { get; set; }

        public IEnumerable<string> IndustryXPath { get; set; }

        public string ButtonSearchXPath { get; set; }

        public string UrlXPath { get; set; }


        public string ExpandAreasJS { get; set; }

        public string ExpandDropDownJS { get; set; }

        public string ElementClickJS { get; set; }


        public string NextPageXPath { get; set; }

        public string ClassNextPage { get; set; }
    }
}