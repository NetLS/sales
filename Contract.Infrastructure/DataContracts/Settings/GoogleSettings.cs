﻿namespace Sales
{
    public class GoogleSettings
    {
        public string DriverPath { get; set; }

        public int Timeout { get; set; }

        public string SiteNameLabelsXpath { get; set; }

        public string UrlXPath { get; set; }

        public string SearchUrl { get; set; }

        public string NextPageXPath { get; set; }

        public string NextPageLabelXpath { get; set; }
    }
}