﻿namespace Sales
{
    public class SiteAnalyzerSettings
    {
        public string AppsPath { get; set; } 

        public string VersionSplitter { get; set; } 

        public string TernaryFinder { get; set; } 

        public string VersionReplace { get; set; } 

        public string ScriptRegex { get; set; } 
    }
}