﻿namespace Sales
{
    public class ProxyScraperSettings
    {
        /// <summary>
        /// Gets or sets the count scrape site thread.
        /// This property contain max of thread for scrape sites from db in one time, because maybe don't enough memory and application crash (this can be when use phantom js for download page source)  
        /// </summary>
        public int CountScrapeSiteThread { get; set; }

        /// <summary>
        /// Gets or sets the count proxy for scrape.
        /// This property contain count proxy which give db for scrape proxy list sites
        /// </summary>
        public int CountProxyForScrape { get; set; }

        /// <summary>
        /// Gets or sets the coefficient count for scrape pagination.
        /// It is count proxy for every one url
        /// </summary>
        public int CoefficientCountForScrapePagination { get; set; }

        public int DbSelectStep { get; set; }

        /// <summary>
        /// Gets or sets the count try to scrape site.
        /// Max count try to load and parse html 
        /// </summary>
        public int CountTryToScrapeSite { get; set; }

        public int MaxTimeout { get; set; }

    }
}