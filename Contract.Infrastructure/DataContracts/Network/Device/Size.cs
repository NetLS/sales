﻿namespace Contract.Infrastructure.DataContracts.Network.Device
{
    public class Size
    {
        public int Width { get; }

        public int Height { get; }

        public Size(int width, int height)
        {
            this.Width = width;
            this.Height = height;
        }

        static Size()
        {
            Mobile = new Size(320, 480);
            LargeMobile = new Size(480, 640);
            Tablet = new Size(640, 800);
            LargeTablet = new Size(960, 840);
            Desktop = new Size(1440, 900);
            LargeDesktop = new Size(1920, 1080);
            Default = new Size(720, 1280);
        }

        public static Size Mobile { get; }
        public static Size LargeMobile { get; }
        public static Size Tablet { get; } 
        public static Size LargeTablet { get; }
        public static Size Desktop { get; }
        public static Size LargeDesktop { get; }
        public static Size Default { get; }
    }
}