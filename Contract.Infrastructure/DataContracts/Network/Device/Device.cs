﻿namespace Contract.Infrastructure.DataContracts.Network.Device
{
    public class Device
    {
        public string Agent { get; private set; }

        public Size Size { get; set; }

        public Device(string Device, Size size = null)
        {
            this.Agent = Device;
            this.Size = size ?? Size.Default;
        }

        public static Device DesktopWin64Chrome => new Device("Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36");
        public static Device DesktopLinuxChrome => new Device("Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_3) AppleWebKit/602.4.8 (KHTML, like Gecko) Version/10.0.3 Safari/602.4.8");
        public static Device DesktopMacOsSafari => new Device("Mozilla/5.0 (iPad; CPU OS 10_2 like Mac OS X) AppleWebKit/602.3.12 (KHTML, like Gecko) Version/10.0 Mobile/14C92 Safari/602.1");
        public static Device TabletIPadSafari => new Device("Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_3) AppleWebKit/602.4.8 (KHTML, like Gecko) Version/10.0.3 Safari/602.4.8");
        public static Device TabletAndroidChrome => new Device("Mozilla/5.0 (Linux; Android 4.4.2; en-us; SAMSUNG SM-T330NU Build/KOT49H) AppleWebKit/537.36 (KHTML, like Gecko) Version/1.5 Chrome/28.0.1500.94 Safari/537.36");
        public static Device MobileAndroidChrome => new Device("Mozilla/5.0 (Linux; Android 5.1.1; YU5010A Build/LMY47V) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.107 Mobile Safari/537.36");
        public static Device MobileIphoneSafari => new Device("Mozilla/5.0 (iPhone; CPU iPhone OS 10_2_1 like Mac OS X) AppleWebKit/602.4.6 (KHTML, like Gecko) Version/10.0 Mobile/14D27 Safari/602.1");


    }
}