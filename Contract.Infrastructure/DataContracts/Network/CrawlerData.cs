﻿namespace Contract.Infrastructure.DataContracts.Network
{
    using System;
    using System.Collections.Generic;

    public class CrawlerData
    {
        public CrawlerData()
        {
            this.LinkList = new List<CrawlerData>();
        }

        public CrawlerData(string url)
        {
            this.Url = new Uri(url);
            this.LinkList = new List<CrawlerData>();
        }

        public Uri Url { get; set; }

        public List<CrawlerData> LinkList { get; set; }

        public bool IsVisited { get; set; }

    }
}