﻿namespace Contract.Infrastructure.DataContracts.Platform.DataDotCom
{
    using System.Collections.Generic;

    public class DataDotComFilterModel : BaseFilterModel
    {
        public IEnumerable<string> Location { get; set; }

        public IEnumerable<string> Industry { get; set; }
    }
}