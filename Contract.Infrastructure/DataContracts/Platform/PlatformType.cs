﻿namespace Contract.Infrastructure.DataContracts.Platform
{
    public enum PlatformType
    {
        None,
        DataDotCom,
        Google
    }
}