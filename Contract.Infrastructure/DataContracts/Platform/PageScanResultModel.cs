﻿namespace Contract.Infrastructure.DataContracts.Platform
{
    using System.Collections.Generic;

    public class PageScanResultModel
    {
        public string UrlPage { get; set; }

        public IEnumerable<string> Urls { get; set; }
    }
}