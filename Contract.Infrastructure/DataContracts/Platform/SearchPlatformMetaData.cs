﻿namespace Contract.Infrastructure.DataContracts.Platform
{
    using System.Collections.Generic;

    public class SearchPlatformMetaData
    {
        public List<PageScanResultModel> ScanResultModels { get; set; }

        public bool IsSuccesses { get; set; }

        public SearchPlatformMetaData()
        {
            this.ScanResultModels = new List<PageScanResultModel>();
        }
    }
}