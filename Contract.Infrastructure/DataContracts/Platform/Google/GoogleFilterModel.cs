﻿namespace Contract.Infrastructure.DataContracts.Platform.Google
{
    using System.Collections.Generic;

    public class GoogleFilterModel : BaseFilterModel
    {
        public IEnumerable<string> Location { get; set; }

        public IEnumerable<string> Industry { get; set; }
    }
}