﻿namespace Contract.Infrastructure.DataContracts.Sprint
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("TechnologyFilter")]
    public class TechnologyFilterModel
    {
        [Key]
        public int TechnologyFilterId { get; set; }

        public int? TaskId { get; set; }

        public int? TechnologyId { get; set; }

        public string VersionJson { get; set; }

        public TechnologyModel TechnologyModel { get; set; }

        public TaskModel TaskModel { get; set; }
    }
}