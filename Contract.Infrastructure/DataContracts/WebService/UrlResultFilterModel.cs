﻿namespace Contract.Infrastructure.DataContracts.Sprint
{
    using System;
    using System.Collections.Generic;

    public class UrlResultFilterModel
    {
        public int? limit { get; set; }

        public int? offset { get; set; }

        public List<TechnologyFilterModel> TechnologyFilterViewModels { get; set; }

        public List<TaskModel> TaskModels { get; set; }

        public int? CountUrlStart { get; set; }

        public int? CountUrlEnd { get; set; }

        public DateTime? CheckDateStart { get; set; }

        public DateTime? CheckDateEnd { get; set; }

        public SiteResponsiveModel SiteResponsiveModel { get; set; }

        public List<int> TaskIdList { get; set; }

        public List<string> UrlList { get; set; }
    }
}