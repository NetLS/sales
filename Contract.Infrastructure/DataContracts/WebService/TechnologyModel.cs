﻿namespace Contract.Infrastructure.DataContracts.Sprint
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("Technology")]
    public class TechnologyModel
    {
        [Key]
        public int TechnologyId { get; set; }

        public string TechnologyName { get; set; }

        public string Icon { get; set; }

        public string Website { get; set; }
    }
}