﻿namespace Contract.Infrastructure.DataContracts.Sprint
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    using Contract.Infrastructure.DataContracts.SiteAnalyzer;

    [Table("UrlResult")]
    public class UrlResultModel
    {
        [Key]
        public int UrlResultId { get; set; }

        public int? TaskId { get; set; }

        public int? PaginatorId { get; set; }

        public int? SiteDataId { get; set; }
        
        public string UrlLink { get; set; }

        public int? Status { get; set; }

        public int? UrlId { get; set; }

        public bool? IsMatch { get; set; }

        public int? CountUrl { get; set; }

        [NotMapped]
        public SiteStatus SiteStatus
        {
            get => (SiteStatus)(this.Status ?? 0);
            set => this.Status = (int)value;
        }

        [NotMapped]
        public ICollection<TechnologySiteDataModel> TechnologySiteDataModels { get; set; }

        [NotMapped]
        public ICollection<SiteResponsiveModel> SiteResponsive { get; set; }
    }
}