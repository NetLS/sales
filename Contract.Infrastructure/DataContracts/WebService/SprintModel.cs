﻿namespace Contract.Infrastructure.DataContracts.Sprint
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("Sprint")]
    public class SprintModel
    {
        [Key]
        public int SprintId { get; set; }

        public string Name { get; set; }

        public DateTime? From { get; set; }

        public DateTime? To { get; set; }

        public virtual ICollection<TaskModel> TaskModels { get; set; }

        public SprintModel()
        {
            this.TaskModels = new List<TaskModel>();
        }

    }
}