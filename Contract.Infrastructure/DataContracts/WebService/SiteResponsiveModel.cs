﻿namespace Contract.Infrastructure.DataContracts.Sprint
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    using Contract.Infrastructure.DataContracts.SiteAnalyzer;

    [Table("SiteResponsive")]
    public class SiteResponsiveModel
    {
        [Key]
        public int SiteResponsiveId { get; set; }

        public int SiteDataId { get; set; }

        public byte DocWidth { get; set; }

        public byte FontSize { get; set; }

        public byte TouchScreen { get; set; }

        public byte ViewPort { get; set; }

        public byte Alert { get; set; }

        public byte Flash { get; set; }

        [NotMapped]
        public DocWidthStatus DocWidthStatus
        {
            get => (DocWidthStatus)(this.DocWidth);
            set => this.DocWidth = (byte)value;
        }

        [NotMapped]
        public FontSizeStatus FontSizeStatus
        {
            get => (FontSizeStatus)(this.FontSize);
            set => this.FontSize = (byte)value;
        }

        [NotMapped]
        public SiteStatus Status { get; set; }

        [NotMapped]
        public TouchScreenStatus TouchScreenStatus
        {
            get => (TouchScreenStatus)(this.TouchScreen);
            set => this.TouchScreen = (byte)value;
        }

        [NotMapped]
        public ViewPortStatus PortStatus
        {
            get => (ViewPortStatus)(this.ViewPort);
            set => this.ViewPort = (byte)value;
        }

        [NotMapped]
        public AlertStatus AlertStatus
        {
            get => (AlertStatus)(this.Alert);
            set => this.Alert = (byte)value;
        }

        [NotMapped]
        public FlashStatus FlashStatus
        {
            get => (FlashStatus)(this.Flash);
            set => this.Flash = (byte)value;
        }

        public string ScreenShot { get; set; }
    }
}