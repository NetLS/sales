﻿namespace Contract.Infrastructure.DataContracts.Sprint
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("Url")]
    public class UrlModel
    {
        [Key]
        public int UrlId { get; set; }

        public string UrlLink { get; set; }

        public DateTime? LastCheck { get; set; }

        public List<SiteDataModel> SiteDataModels { get; set; }
    }
}