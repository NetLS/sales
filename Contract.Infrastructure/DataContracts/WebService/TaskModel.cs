﻿namespace Contract.Infrastructure.DataContracts.Sprint
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    using Contract.Infrastructure.DataContracts.Platform;

    [Table("Tasks")]
    public class TaskModel
    {
        [Key]
        public int TaskId { get; set; }

        public int? SprintId { get; set; }

        public string TaskName { get; set; }

        public int? TargetId { get; set; }

        public string FiltersJson { get; set; }

        public int Status { get; set; }

        [NotMapped]
        public TaskStatus TaskStatus
        {
            get => (TaskStatus)this.Status;
            set => this.Status = (int)value;
        }

        [NotMapped]
        public PlatformType PlatformType
        {
            get => (PlatformType)(this.TargetId ?? 0);
            set => this.Status = (int)value;
        }

        public virtual ICollection<PaginatorModel> PaginatorModels { get; set; }

        public virtual ICollection<UrlResultModel> UrlResultModels { get; set; }

        public virtual ICollection<TechnologyFilterModel> TechnologyFilterModels { get; set; }



        public TaskModel()
        {
            this.PaginatorModels = new List<PaginatorModel>();

            this.UrlResultModels = new List<UrlResultModel>();

            this.TechnologyFilterModels = new List<TechnologyFilterModel>();
        }

    }
}