﻿namespace Contract.Infrastructure.DataContracts.Sprint
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("TechnologySiteData")]
    public class TechnologySiteDataModel
    {
        [Key]
        public int TechnologySiteDataId { get; set; }

        public int? SiteDataId { get; set; }

        public int? TechnologyId { get; set; }

        public string Version { get; set; }

        [NotMapped]
        public TechnologyModel TechnologyModel { get; set; }

        [NotMapped]
        public SiteDataModel SiteDataModel { get; set; }
    }
}