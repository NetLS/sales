﻿namespace Contract.Infrastructure.DataContracts.Sprint
{
    public enum TaskStatus
    {
        New,
        InProgress,
        Executed,
        Failed
    }
}