﻿namespace Contract.Infrastructure.DataContracts.Sprint
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("Paginator")]
    public class PaginatorModel
    {
        [Key]
        public int PaginatorId { get; set; }

        public int? TaskId { get; set; }

        public string PaginatorUrl { get; set; }

        public virtual ICollection<UrlResultModel> UrlResultModels { get; set; }

        public PaginatorModel()
        {
            this.UrlResultModels = new List<UrlResultModel>();
        }
    }
}