﻿namespace Contract.Infrastructure.DataContracts.Sprint
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("SiteData")]
    public class SiteDataModel
    {
        [Key]
        public int SiteDataId { get; set; }

        public int? UrlId { get; set; }

        public DateTime? Check { get; set; }
    }
}