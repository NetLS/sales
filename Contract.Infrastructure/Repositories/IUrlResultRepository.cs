﻿namespace Contract.Infrastructure.Repositories
{
    using System.Collections.Generic;

    using Contract.Infrastructure.DataContracts.Sprint;

    public interface IUrlResultRepository
    {
        void Create(IEnumerable<UrlResultModel> resultModels);

        IEnumerable<UrlResultModel> Get(UrlResultFilterModel filter, out int count);
    }
}