﻿namespace Contract.Infrastructure.Repositories
{
    using System.Collections.Generic;

    using Contract.DataAccessLayer.Models;

    public interface IProxyRepository
    {
        IEnumerable<ProxyModel> GetAll(ProxyFilterModel filterModel);

        void CreateOrUpdate(IEnumerable<AddDeleteProxyModel> proxyModels);

        void CreateOrUpdate(AddDeleteProxyModel proxyModel);

        void Delete(IEnumerable<AddDeleteProxyModel> proxyModels);

        void Delete(AddDeleteProxyModel proxyModel);
    }
}