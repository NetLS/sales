﻿namespace Contract.Infrastructure.Repositories
{
    using System.Collections.Generic;

    using Contract.DataAccessLayer.Models;

    public interface ISiteRepository
    {
        IEnumerable<SiteModel> GetAll(SiteFilterModel filterModel);

        void Create(SiteModel siteModel);

        void Update(SiteModel siteModel);

        void Update(int id);

        void Delete(int id);

    }
}