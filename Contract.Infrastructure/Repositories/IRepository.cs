﻿namespace Contract.DataAccessLayer.Interfaces
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;

    public interface IRepository<T> where T : class 
    {
        IEnumerable<T> Get(params Expression<Func<T, object>>[] properties);

        T FindById(int id, params Expression<Func<T, object>>[] properties);

        IEnumerable<T> Get(Func<T, bool> predicate, params Expression<Func<T, object>>[] properties);

        void Add(T entity);

        void Delete(T entity);

        void Edit(T entity);
    }
}