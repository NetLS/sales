﻿namespace Contract.DataAccessLayer.Interfaces
{
    using System;

    using Contract.Infrastructure.Repositories;

    public interface IUnitOfWork : IDisposable
    {
        IProxyRepository ProxyRepository { get; }

        ISiteRepository SiteRepository { get; }

        void Save();
    }
}