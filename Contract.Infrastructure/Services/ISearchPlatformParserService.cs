﻿namespace Contract.Infrastructure.Services
{
    using System.Collections.Generic;
    using System.Net;

    using Contract.Infrastructure.DataContracts.Platform;

    using Sales.Models;

    public interface ISearchPlatformParserService
    {
        SearchPlatformMetaData Parse(BaseFilterModel filterModel, Proxy proxy, NetworkCredential credential = null);
    }
}