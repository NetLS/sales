﻿namespace Contract.Infrastructure.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq.Expressions;

    public interface IDbWebService<T>
    {
        IEnumerable<T> GetAll(params Expression<Func<T, object>>[] properties);

        IEnumerable<T> Get(Func<T, bool> predicate, params Expression<Func<T, object>>[] properties);

        T GetById(int id, params Expression<Func<T, object>>[] properties);

        bool Create(T item);

        bool Update(T item);

        bool Delete(int id);

    }
}