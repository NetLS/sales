﻿namespace Contract.Infrastructure.Services
{
    using System.Collections.Generic;

    using Contract.DataAccessLayer.Models;

    public interface ISiteService
    {
        IEnumerable<SiteModel> GetAll(SiteFilterModel filterModel);

        void Create(SiteModel siteModel);

        void Update(SiteModel siteModel);

        void Update(int id);

        void Delete(int id);
    }
}