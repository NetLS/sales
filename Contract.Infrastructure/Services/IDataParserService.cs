﻿namespace Contract.Infrastructure.Services
{
    using System.Collections.Generic;

    using Contract.DataAccessLayer.Models;

    using Sales.Models;

    public interface IDataParserService
    {
        List<Proxy> Parse(TypeContent content, string data, string pattern, string url);

        List<Proxy> ParseHtmlData(string data, string pattern, string url);

        List<Proxy> ParseHtmlDataWithSeparator(string data, string pattern, string url);

        List<Proxy> ParseJsonData(string data, string pattern, string url);
    }
}