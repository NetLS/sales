﻿namespace Contract.Infrastructure.Services
{
    using System.Collections.Generic;

    using Contract.Infrastructure.DataContracts.Sprint;

    public interface IUrlResultService
    {
        void Create(IEnumerable<UrlResultModel> resultModels);

        IEnumerable<UrlResultModel> Get(UrlResultFilterModel filter, out int count);
    }
}