﻿namespace Contract.Infrastructure.Factories
{
    using Contract.Infrastructure.DataContracts.Platform;
    using Contract.Infrastructure.Services;

    public interface IPlatformFactory
    {
        ISearchPlatformParserService CreatePlatformParserService(PlatformType type);

        BaseFilterModel ParseFilterModel(PlatformType type, string filters);
    }
}