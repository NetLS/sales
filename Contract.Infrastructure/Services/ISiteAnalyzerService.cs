﻿namespace Contract.Infrastructure.Services
{
    using System.Collections.Generic;

    using Contract.Infrastructure.DataContracts.SiteAnalyzer;

    public interface ISiteAnalyzerService
    {
        void AnalyzeAll(
            SiteMetaData data,
            bool isSetUrls = true,
            bool isResolveImplies = true,
            bool isResolveExcludes = true);

        List<AppFull> AnalyzeUrl(List<string> urlsList);

        List<AppFull> AnalyzeHtml(string html);

        List<AppFull> AnalyzeScript(List<string> scripts);

        List<AppFull> AnalyzeMeta(List<KeyValuePair<string, string>> meta);

        List<AppFull> AnalyzeHeader(List<KeyValuePair<string, string>> headers);

        List<AppFull> AnalyzeEnvVar(List<string> environmentVariables);
    }
}