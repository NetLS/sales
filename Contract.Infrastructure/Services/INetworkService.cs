﻿namespace Contract.Infrastructure.Services
{
    using System.Collections.Generic;
    using System.Net;
    using System.Threading.Tasks;

    using Contract.Infrastructure.DataContracts.Network;
    using Contract.Infrastructure.DataContracts.Network.Device;
    using Contract.Infrastructure.DataContracts.SiteAnalyzer;

    using Sales.Models;

    public interface INetworkService
    {
        Task<string> GetPageSource(string url, Proxy proxy = null);

        Task<string> GetPageSourceWithJS(string url, Proxy proxy = null);

        Task<SiteMetaData> GetSiteMetaDataAll(string url, Device device, Proxy proxy = null);

        Task<SiteMetaData> GetSiteMetaDataGeneral(string url, Device device, Proxy proxy = null);

        Task<CheckProxyResult> CheckProxy(Proxy proxy, int? timeout = null);

        Task<bool> IsProxyAvailable(Proxy proxy, string url = null, int? timeout = null);

        Task<ProxyAnonymity> GetProxyAnonymity(Proxy proxy, string url = null, int? timeout = null);

        Task<List<CrawlerData>> Crawler(List<CrawlerData> listUrl, int depth, Proxy proxy = null);

        Task<List<CrawlerData>> Crawler(string url, int depth, Proxy proxy = null);
    }
}