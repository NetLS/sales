﻿namespace Contract.Services.Database
{
    using System;
    using System.Collections.Generic;

    using Contract.DataAccessLayer.Interfaces;
    using Contract.DataAccessLayer.Models;
    using Contract.Infrastructure.DataContracts.Proxy;
    using Contract.Infrastructure.Services;

    using Microsoft.Extensions.Logging;

    public class ProxyService : IProxyService
    {
        private IUnitOfWork unitOfWork;
        private readonly ILogger logger;

        public ProxyService(IUnitOfWork unitOfWork, ILoggerFactory logger)
        {
            this.unitOfWork = unitOfWork;

            this.logger = logger.CreateLogger<NLog.ILogger>();
        }

        public IEnumerable<ProxyModel> GetAll(ProxyFilterModel filterModel)
        {
            IEnumerable<ProxyModel> proxyModels = null;

            try
            {
                proxyModels = this.unitOfWork.ProxyRepository.GetAll(filterModel);
            }
            catch (Exception e)
            {
                this.logger.LogError(LoggingEvents.PROXY_CRUD_ALL_EXCEPTION, e, $"Cannot get proxy with filter {filterModel.ToString()}.");
            }

            return proxyModels;
        }

        public void CreateOrUpdate(IEnumerable<AddDeleteProxyModel> proxyModels)
        {
            try
            {
                this.unitOfWork.ProxyRepository.CreateOrUpdate(proxyModels);
            }
            catch (Exception e)
            {
                this.logger.LogError(LoggingEvents.PROXY_CRUD_ALL_EXCEPTION, e, $"Cannot create proxy list {proxyModels.ToString()}.");
            }
        }

        public void CreateOrUpdate(AddDeleteProxyModel proxyModel)
        {
            try
            {
                this.unitOfWork.ProxyRepository.CreateOrUpdate(proxyModel);
            }
            catch (Exception e)
            {
                this.logger.LogError(LoggingEvents.PROXY_CRUD_ALL_EXCEPTION, e, $"Cannot create proxy {proxyModel.ToString()}.");
            }
        }

        public void Delete(IEnumerable<AddDeleteProxyModel> proxyModels)
        {
            try
            {
                this.unitOfWork.ProxyRepository.Delete(proxyModels);
            }
            catch (Exception e)
            {
                this.logger.LogError(LoggingEvents.PROXY_CRUD_ALL_EXCEPTION, e, $"Cannot delete proxy {proxyModels.ToString()}.");
            }
        }

        public void Delete(AddDeleteProxyModel proxyModel)
        {
            try
            {
                this.unitOfWork.ProxyRepository.Delete(proxyModel);
            }
            catch (Exception e)
            {
                this.logger.LogError(LoggingEvents.PROXY_CRUD_ALL_EXCEPTION, e, $"Cannot delete proxy {proxyModel.ToString()}.");
            }
        }
    }
}