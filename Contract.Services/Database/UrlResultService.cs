﻿namespace Contract.Services.Database
{
    using System;
    using System.Collections.Generic;

    using Contract.Infrastructure.DataContracts.Proxy;
    using Contract.Infrastructure.DataContracts.Sprint;
    using Contract.Infrastructure.Repositories;
    using Contract.Infrastructure.Services;

    using Microsoft.Extensions.Logging;

    public class UrlResultService : IUrlResultService
    {
        private IUrlResultRepository repository;

        private readonly ILogger logger;

        public UrlResultService(IUrlResultRepository repository, ILoggerFactory logger)
        {
            this.repository = repository;

            this.logger = logger.CreateLogger<NLog.ILogger>();
        }

        public void Create(IEnumerable<UrlResultModel> resultModels)
        {
            try
            {
                this.repository.Create(resultModels);
            }
            catch (Exception e)
            {
                this.logger.LogError(
                    LoggingEvents.GET_ALL_EXCEPTION,
                    e,
                    $"Cannot create urlResult list {resultModels.ToString()}.");
            }
        }

        public IEnumerable<UrlResultModel> Get(UrlResultFilterModel filter, out int count)
        {
            IEnumerable<UrlResultModel> urlResultModels = null;
            count = 0;
            try
            {
                urlResultModels = this.repository.Get(filter, out count);
            }
            catch (Exception e)
            {
                this.logger.LogWarning(
                    LoggingEvents.GET_ALL_EXCEPTION,
                    e,
                    $"Cannot get uerResult with filter {filter.ToString()}.");
            }

            return urlResultModels;
        }
    }
}
