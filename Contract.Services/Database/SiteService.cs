﻿namespace Contract.Services.Database
{
    using System;
    using System.Collections.Generic;

    using Contract.DataAccessLayer.Interfaces;
    using Contract.DataAccessLayer.Models;
    using Contract.DataAccessLayer.Repository;
    using Contract.Infrastructure.DataContracts.Proxy;
    using Contract.Infrastructure.Services;

    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.Logging;

    public class SiteService : ISiteService
    {
        private IUnitOfWork unitOfWork;
        private ILogger logger;

        public SiteService(IUnitOfWork unitOfWork, ILoggerFactory logger)
        {
            this.unitOfWork = unitOfWork;

            this.logger = logger.CreateLogger<NLog.ILogger>();
        }

        public IEnumerable<SiteModel> GetAll(SiteFilterModel filterModel)
        {
            IEnumerable<SiteModel> siteModels = null;

            try
            {
                siteModels = this.unitOfWork.SiteRepository.GetAll(filterModel);
            }
            catch (Exception e)
            {
                this.logger.LogError(LoggingEvents.PROXY_CRUD_ALL_EXCEPTION, e, $"Cannot get sites with filter {filterModel.ToString()}.");
            }

            return siteModels;
        }

        public void Create(SiteModel siteModel)
        {
            try
            {
                this.unitOfWork.SiteRepository.Create(siteModel);
            }
            catch (Exception e)
            {
                this.logger.LogError(LoggingEvents.PROXY_CRUD_ALL_EXCEPTION, e, $"Cannot create site {siteModel.ToString()}.");
            }
        }

        public void Update(SiteModel siteModel)
        {
            try
            {
                this.unitOfWork.SiteRepository.Update(siteModel);
            }
            catch (Exception e)
            {
                this.logger.LogError(LoggingEvents.PROXY_CRUD_ALL_EXCEPTION, e, $"Cannot update site {siteModel.ToString()}.");
            }
        }

        public void Update(int id)
        {
            try
            {
                this.unitOfWork.SiteRepository.Update(id);
            }
            catch (Exception e)
            {
                this.logger.LogError(LoggingEvents.PROXY_CRUD_ALL_EXCEPTION, e, $"Cannot update site  {id}.");
            }
        }

        public void Delete(int id)
        {
            try
            {
                this.unitOfWork.SiteRepository.Delete(id);
            }
            catch (Exception e)
            {
                this.logger.LogError(LoggingEvents.PROXY_CRUD_ALL_EXCEPTION, e, $"Cannot delete site with id {id}.");
            }
        }
    }
}