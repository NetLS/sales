﻿namespace Contract.Services.Database
{
    using System;
    using System.Collections.Generic;
    using System.Linq.Expressions;

    using Contract.DataAccessLayer.Interfaces;
    using Contract.Infrastructure.DataContracts.Proxy;
    using Contract.Infrastructure.Services;

    using Microsoft.Extensions.Logging;

    public class DbWebService<T> : IDbWebService<T> where T : class
    {
        private IRepository<T> repository;
        private readonly ILogger logger;

        public DbWebService(IRepository<T> repository, ILoggerFactory loggerFactory)
        {
            this.repository = repository;
            this.logger = loggerFactory.CreateLogger<NLog.ILogger>();
        }

        public IEnumerable<T> GetAll(params Expression<Func<T, object>>[] properties)
        {
            IEnumerable<T> enumerable = null;

            try
            {
                enumerable = this.repository.Get(properties);
            }
            catch (Exception e)
            {
                this.logger.LogError(LoggingEvents.PROXY_CRUD_ALL_EXCEPTION, e, $"Cannot get list of {typeof(T)}.");
            }

            return enumerable;
        }

        public IEnumerable<T> Get(Func<T, bool> predicate, params Expression<Func<T, object>>[] properties)
        {
            IEnumerable<T> enumerable = null;

            try
            {
                enumerable = this.repository.Get(predicate, properties);
            }
            catch (Exception e)
            {
                this.logger.LogError(LoggingEvents.PROXY_CRUD_ALL_EXCEPTION, e, $"Cannot get list of {typeof(T)} with predicate {predicate.ToString()}.");
            }

            return enumerable;
        }

        public T GetById(int id, params Expression<Func<T, object>>[] properties)
        {
            try
            {
                var item = this.repository.FindById(id, properties);
                return item;
            }
            catch (Exception e)
            {
                this.logger.LogError(LoggingEvents.PROXY_CRUD_ALL_EXCEPTION, e, $"Cannot get {typeof(T)} with id {id}.");
                return null;
            }
        }

        public bool Create(T item)
        {
            try
            {
                this.repository.Add(item);
                return true;
            }
            catch (Exception e)
            {
                this.logger.LogError(LoggingEvents.PROXY_CRUD_ALL_EXCEPTION, e, $"Cannot create {typeof(T)} {item.ToString()}.");
                return false;
            }
        }

        public bool Update(T item)
        {
            try
            {
                this.repository.Edit(item);
                return true;
            }
            catch (Exception e)
            {
                this.logger.LogError(LoggingEvents.PROXY_CRUD_ALL_EXCEPTION, e, $"Cannot update {typeof(T)} {item.ToString()}.");
                return false;
            }
        }

        public bool Delete(int id)
        {
            T item = default(T);
            try
            {
                item = this.GetById(id, null);
                this.repository.Delete(item);
                return true;
            }
            catch (Exception e)
            {
                this.logger.LogError(LoggingEvents.PROXY_CRUD_ALL_EXCEPTION, e, $"Cannot delete {typeof(T)} {item?.ToString()}.");
                return false;
            }
        }
    }
}