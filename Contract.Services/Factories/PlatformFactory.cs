﻿namespace Contract.Services.Factories
{
    using System;

    using Contract.Infrastructure.DataContracts.Platform;
    using Contract.Infrastructure.DataContracts.Platform.DataDotCom;
    using Contract.Infrastructure.DataContracts.Platform.Google;
    using Contract.Infrastructure.DataContracts.Proxy;
    using Contract.Infrastructure.Factories;
    using Contract.Infrastructure.Services;

    using Microsoft.Extensions.Logging;
    using Microsoft.Extensions.Options;

    using Newtonsoft.Json;

    using Sales;
    using Sales.Services;

    public class PlatformFactory : IPlatformFactory
    {
        private IOptions<DataDotComSettings> optionsDataDotCom;
        private IOptions<GoogleSettings> optionsGoogle;

        private ILoggerFactory loggerFactory;

        private readonly ILogger logger;

        public PlatformFactory(IOptions<DataDotComSettings> optionsDataDotCom, IOptions<GoogleSettings> optionsGoogle, ILoggerFactory loggerFactory)
        {
            this.optionsDataDotCom = optionsDataDotCom;
            this.optionsGoogle = optionsGoogle;
            this.loggerFactory = loggerFactory;

            this.logger = loggerFactory.CreateLogger<NLog.ILogger>();
        }

        public ISearchPlatformParserService CreatePlatformParserService(PlatformType type)
        {
            switch (type)
            {
                case PlatformType.DataDotCom:
                    {
                        return new DataDotComParserService(this.optionsDataDotCom, this.loggerFactory);
                    }
                case PlatformType.Google:
                    {
                        return new GoogleParserService(this.optionsGoogle, this.loggerFactory);
                    }

                default: return null;
            }
        }

        public BaseFilterModel ParseFilterModel(PlatformType type, string filters)
        {
            try
            {
                switch (type)
                {
                    case PlatformType.DataDotCom:
                        {
                            return JsonConvert.DeserializeObject<DataDotComFilterModel>(filters);
                        }
                    case PlatformType.Google:
                        {
                            return JsonConvert.DeserializeObject<GoogleFilterModel>(filters);
                        }

                    default: return null;
                }
            }
            catch (JsonException e)
            {
                this.logger.LogError(
                    LoggingEvents.GET_ALL_EXCEPTION,
                    $"Cannot parse filter with type {type}, filter => {filters}",
                    e);
                return null;
            }
        }
    }
}