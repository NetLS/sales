﻿namespace Sales.Services
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Text.RegularExpressions;

    using Contract.Infrastructure.DataContracts.Proxy;
    using Contract.Infrastructure.DataContracts.SiteAnalyzer;
    using Contract.Infrastructure.Services;
    using Contract.Services.Helpers;

    using HtmlAgilityPack;

    using Microsoft.Extensions.Logging;
    using Microsoft.Extensions.Options;

    using Newtonsoft.Json;

    public class SiteAnalyzerService : ISiteAnalyzerService
    {
        private Technologies technologies;

        private SiteAnalyzerSettings settings;

        private readonly ILogger logger;

        public SiteAnalyzerService(IOptions<SiteAnalyzerSettings> options, ILoggerFactory logger)
        {
            this.settings = options.Value;

            this.logger = logger.CreateLogger<NLog.ILogger>();

            try
            {
                var contentText = File.ReadAllText(this.settings.AppsPath);
                var settingsJson = new JsonSerializerSettings();
                settingsJson.Converters.Add(new SingleOrEnumerableJsonConverter<IEnumerable<string>>());
                this.technologies = JsonConvert.DeserializeObject<Technologies>(contentText, settingsJson);
            }
            catch (IOException e)
            {
                this.logger.LogError(
                    LoggingEvents.APPS_LOAD_FILE_ERROR,
                    e,
                    $"Cannot load apps.json form path {this.settings.AppsPath}.");
            }
            catch (JsonException e)
            {
                this.logger.LogError(
                    LoggingEvents.APPS_LOAD_PARSE_ERROR,
                    e,
                    $"Cannot parse apps.json, maybe wrong schema.");
            }
            catch (Exception e)
            {
                this.logger.LogError(
                    LoggingEvents.APPS_LOAD_ALL_EXCEPTION,
                    e,
                    $"Cannot load or parse apps.json {this.settings.AppsPath}");
            }
            finally
            {
                if (this.technologies == null)
                {
                    this.technologies = new Technologies()
                                            {
                                                Apps = new Dictionary<string, Apps>(),
                                                Categories = new Dictionary<string, Categories>()
                                            };
                }
            }
        }

        public void AnalyzeAll(SiteMetaData data, bool isSetUrls = true, bool isResolveImplies = true, bool isResolveExcludes = true)
        {
            var appFulls = new List<AppFull>();

            var urls = this.GetUrls(data.Html, data.Host);

            if (isSetUrls)
            {
                data.Urls = urls;
            }

            appFulls.AddRange(this.AnalyzeUrl(data.Urls));
            appFulls.AddRange(this.AnalyzeScript(this.GetScripts(data.Html)));
            appFulls.AddRange(this.AnalyzeHtml(data.Html));
            appFulls.AddRange(this.AnalyzeMeta(this.GetMetas(data.Html)));
            appFulls.AddRange(this.AnalyzeHeader(data.Headers));
            appFulls.AddRange(this.AnalyzeEnvVar(data.EnvironmentVariables));

            if (isResolveImplies)
            {
                this.ResolveImplies(ref appFulls);
            }

            if (isResolveExcludes)
            {
                this.ResolveExcludes(ref appFulls);
            }

            data.AppFulls = appFulls;
        }

        public List<AppFull> AnalyzeUrl(List<string> urlsList)
        {
            var list = new List<AppFull>();

            foreach (var item in this.technologies.Apps)
            {
                if (item.Value.Url != null)
                {
                    foreach (var url in urlsList)
                    {
                        var regex = new Regex(item.Value.Url, RegexOptions.IgnoreCase);
                        var match = regex.Match(url);
                        if (match.Success)
                        {
                            list.Add(new AppFull() { App = item.Value, Name = item.Key });
                        }
                    }
                }
            }

            return list;
        }

        public List<AppFull> AnalyzeHtml(string html)
        {
            var list = new List<AppFull>();

            foreach (var itemApp in this.technologies.Apps)
            {
                if (itemApp.Value.Html != null)
                {
                    foreach (var itemHtml in itemApp.Value.Html)
                    {
                        var regex = new Regex(itemHtml, RegexOptions.IgnoreCase);
                        var match = regex.Match(html);
                        if (match.Success)
                        {
                            var app = this.AnalyzeRegex(itemApp.Key, itemHtml);

                            app.App = itemApp.Value;
                            app.Name = itemApp.Key;
                            list.Add(app);
                        }
                    }
                }
            }

            return list;
        }

        public List<AppFull> AnalyzeScript(List<string> scripts)
        {
            var list = new List<AppFull>();

            foreach (var itemApp in this.technologies.Apps)
            {
                if (itemApp.Value.Script != null)
                {
                    foreach (var itemScript in itemApp.Value.Script)
                    {
                        foreach (var itemSourceScript in scripts)
                        {
                            var regex = new Regex(this.SplitRegex(itemScript)[0], RegexOptions.IgnoreCase);
                            var match = regex.Match(itemSourceScript);
                            if (match.Success)
                            {
                                var app = this.AnalyzeRegex(itemScript, itemSourceScript);

                                app.App = itemApp.Value;
                                app.Name = itemApp.Key;
                                list.Add(app);
                            }
                        }
                    }
                }
            }

            return list;
        }

        public List<AppFull> AnalyzeEnvVar(List<string> environmentVariables)
        {
            var list = new List<AppFull>();

            foreach (var itemApp in this.technologies.Apps)
            {
                if (itemApp.Value.Env != null)
                {
                    foreach (var itemScript in itemApp.Value.Env)
                    {
                        foreach (var environmentVariable in environmentVariables)
                        {
                            var regex = new Regex(this.SplitRegex(itemScript)[0], RegexOptions.IgnoreCase);
                            var match = regex.Match(environmentVariable);
                            if (match.Success)
                            {
                                var app = this.AnalyzeRegex(itemScript, environmentVariable);

                                app.App = itemApp.Value;
                                app.Name = itemApp.Key;
                                list.Add(app);
                            }
                        }
                    }
                }
            }

            return list;
        }

        public List<AppFull> AnalyzeMeta(List<KeyValuePair<string, string>> meta)
        {
            var list = new List<AppFull>();

            foreach (var itemApp in this.technologies.Apps)
            {
                if (itemApp.Value.Meta != null)
                {
                    foreach (var itemMeta in itemApp.Value.Meta)
                    {
                        foreach (var itemSourceMeta in meta)
                        {
                            if (itemSourceMeta.Key == itemMeta.Key)
                            {
                                var regex = new Regex(itemMeta.Value, RegexOptions.IgnoreCase);
                                var match = regex.Match(itemSourceMeta.Value);
                                if (match.Success)
                                {
                                    var app = this.AnalyzeRegex(itemMeta.Value, itemSourceMeta.Value);

                                    app.App = itemApp.Value;
                                    app.Name = itemApp.Key;
                                    list.Add(app);
                                }
                            }
                        }
                    }
                }
            }

            return list;
        }

        public List<AppFull> AnalyzeHeader(List<KeyValuePair<string, string>> headers)
        {
            var list = new List<AppFull>();

            foreach (var itemApp in this.technologies.Apps)
            {
                if (itemApp.Value.Headers != null)
                {
                    foreach (var itemHeader in itemApp.Value.Headers)
                    {
                        foreach (var itemSourceHeader in headers)
                        {
                            if (itemSourceHeader.Key == itemHeader.Key)
                            {

                                var regex = new Regex(this.SplitRegex(itemHeader.Value)[0], RegexOptions.IgnoreCase);
                                var match = regex.Match(itemSourceHeader.Value);
                                if (match.Success)
                                {
                                    var app = this.AnalyzeRegex(itemHeader.Value, itemSourceHeader.Value);

                                    app.App = itemApp.Value;
                                    app.Name = itemApp.Key;
                                    list.Add(app);
                                }
                            }
                        }
                    }
                }
            }

            return list;
        }

        private void ResolveExcludes(ref List<AppFull> appFulls)
        {
            // distinct 
            appFulls = appFulls.GroupBy(x => x.Name).Select(x =>
                {
                    var item = x.FirstOrDefault(app => app.Version != null) ?? x.FirstOrDefault();
                    return item;
                }).ToList();

            var excludeList = new List<string>();

            foreach (var itemApp in appFulls)
            {
                if (itemApp.App.Exclude != null)
                {
                    foreach (var item in itemApp.App.Exclude)
                    {
                        excludeList.Add(item);
                    }
                }
            }

            for (int i = 0; i < excludeList.Count; i++)
            {
                var item = appFulls.FirstOrDefault(full => full.Name == excludeList[i]);
                if (item != null)
                {
                    appFulls.Remove(item);
                }
            }
        }

        private void ResolveImplies(ref List<AppFull> appFulls)
        {
            var tempList = new List<AppFull>();

            foreach (var itemApp in appFulls)
            {
                if (itemApp.App.Implies != null)
                {
                    foreach (var item in itemApp.App.Implies)
                    {
                        if (technologies.Apps.ContainsKey(item))
                        {
                            Apps apps = null;
                            this.technologies.Apps.TryGetValue(item, out apps);
                            tempList.Add(new AppFull() { App = apps, Name = item });
                        }
                    }
                }
            }

            appFulls.AddRange(tempList);
        }

        private string[] SplitRegex(string regex)
        {
            var patterns = regex.Split(new string[] { this.settings.VersionSplitter }, StringSplitOptions.None);

            if (patterns.Length > 0)
            {
                return patterns;
            }

            return new string[] { regex };
        }

        private AppFull AnalyzeRegex(string item, string source)
        {
            var appFinal = new AppFull();

            var patterns = this.SplitRegex(item);
            item = patterns[0];
            if (patterns.Length >= 2)
            {
                appFinal.Version = patterns[1].Split(':')[1];
            }

            var regex = new Regex(item, RegexOptions.IgnoreCase);
            var match = regex.Match(source);
            if (match.Success)
            {
                var version = match.Groups[1].Value;
                var versions = new List<string>();
                if (match.Groups[1].Success)
                {
                    for (int i = 0; i < match.Groups[1].Captures.Count; i++)
                    {
                        var ternary = new Regex(string.Format(this.settings.TernaryFinder, i), RegexOptions.IgnoreCase).Matches(match.Groups[1].Value);
                        if (ternary != null && ternary.Count == 3)
                        {
                            version = version.Replace(
                                ternary[0].Value,
                                !string.IsNullOrEmpty(match.Groups[1].Captures[1].Value) ? ternary[1].Value : ternary[2].Value);
                        }

                        version = Regex.Replace(version, string.Format(this.settings.VersionReplace, i), match.Groups[1].Captures[i].Value ?? string.Empty, RegexOptions.IgnoreCase);
                    }

                    if (version != null && !versions.Contains(version))
                    {
                        versions.Add(version);
                    }

                    versions.Sort((s, s1) => s.Length - s1.Length);

                    var resolved = versions[0];

                    for (int i = 1; i < versions.Count; i++)
                    {
                        if (versions[i].IndexOf(resolved) == -1)
                        {
                            break;
                        }

                        resolved = versions[i];
                    }

                    appFinal.Version = resolved;
                }
                else
                {
                    appFinal.Version = null;
                }
            }

            return appFinal;
        }

        private List<string> GetScripts(string pageSource)
        {
            var listScripts = new List<string>();

            try
            {
                var doc = new HtmlDocument();
                doc.LoadHtml(pageSource);

                var list = doc.DocumentNode.SelectNodes("//script");
                if (list != null)
                {
                    foreach (var node in list)
                    {
                        var src = node.GetAttributeValue("src", string.Empty);

                        if (!string.IsNullOrEmpty(src))
                        {
                            listScripts.Add(src);
                        }
                    }
                }
            }
            catch
            {
                // ignored
            }

            return listScripts;
        }

        private List<KeyValuePair<string, string>> GetMetas(string pageSource)
        {
            var keyValuePairs = new List<KeyValuePair<string, string>>();

            try
            {
                var doc = new HtmlDocument();
                doc.LoadHtml(pageSource);

                var list = doc.DocumentNode.SelectNodes("//meta");
                if (list != null)
                {
                    foreach (var node in list)
                    {
                        var name = node.GetAttributeValue("name", string.Empty);
                        var content = node.GetAttributeValue("content", string.Empty);

                        if (!string.IsNullOrEmpty(name))
                        {
                            keyValuePairs.Add(new KeyValuePair<string, string>(name, content));
                        }
                    }
                }
            }
            catch
            {
                // ignored
            }

            return keyValuePairs;
        }

        private List<string> GetUrls(string pageSource, string host)
        {
            var listUrls = new List<string>() { host };

            try
            {
                var htmlDocument = new HtmlDocument();
                htmlDocument.LoadHtml(pageSource);

                var list = htmlDocument.DocumentNode.SelectNodes("//a[starts-with(@href,'/') or contains(@href, '" + host + "')]");
                if (list != null)
                {
                    foreach (var node in list)
                    {
                        var link = node.GetAttributeValue("href", string.Empty);

                        if (!string.IsNullOrEmpty(link))
                        {
                            var tempUrl = !link.Contains(host) ? host + link.Substring(1) : link;
                            if (!listUrls.Contains(tempUrl))
                            {
                                listUrls.Add(tempUrl);
                            }
                        }
                    }
                }
            }
            catch
            {
                // ignored
            }

            return listUrls;
        }
    }
}