﻿namespace Sales.Services
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Drawing;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Net.Http;
    using System.Net.Http.Headers;
    using System.Security.Authentication;
    using System.Text;
    using System.Text.RegularExpressions;
    using System.Threading;
    using System.Threading.Tasks;

    using Contract.Infrastructure.DataContracts.Network;
    using Contract.Infrastructure.DataContracts.Network.Device;
    using Contract.Infrastructure.DataContracts.Proxy;
    using Contract.Infrastructure.DataContracts.SiteAnalyzer;
    using Contract.Infrastructure.Services;

    using HtmlAgilityPack;

    using Microsoft.Extensions.Logging;
    using Microsoft.Extensions.Options;

    using Newtonsoft.Json;

    using OpenQA.Selenium;
    using OpenQA.Selenium.Chrome;
    using OpenQA.Selenium.PhantomJS;
    using OpenQA.Selenium.Remote;

    using Sales.Models;
    using Proxy = Models.Proxy;
    using Size = System.Drawing.Size;

    public class NetworkService : INetworkService
    {
        private NetworkSettings networkSettings;

        private readonly ILogger logger;

        public NetworkService(IOptions<NetworkSettings> options, ILoggerFactory logger)
        {
            this.networkSettings = options.Value;

            this.logger = logger.CreateLogger<NLog.ILogger>();
        }

        public async Task<string> GetPageSource(string url, Proxy proxy = null)
        {
            var clientHandler = new HttpClientHandler();

            // Init proxy
            if (proxy != null)
            {
                clientHandler.Proxy = new NetworkProxy(proxy);
                clientHandler.UseProxy = true;
            }
            
            clientHandler.SslProtocols = SslProtocols.Tls12 | SslProtocols.Tls11 | SslProtocols.Tls;

            using (var client = new HttpClient(clientHandler))
            {
                // Emulate browser headers
                client.DefaultRequestHeaders.TryAddWithoutValidation("User-Agent", Device.DesktopWin64Chrome.Agent);

                client.Timeout = TimeSpan.FromMilliseconds(this.networkSettings.Timeout);

                try
                {
                    var uri = new UriBuilder(url).Uri;
                    var result = await client.GetAsync(uri);
                    var html = await result.Content.ReadAsStringAsync();

                    if (result.IsSuccessStatusCode && !string.IsNullOrWhiteSpace(html))
                    {
                        return html;
                    }
                }
                catch
                {
                    // ignored
                }
                finally
                {
                    client.Dispose();
                }
            }

            return await Task.FromResult(default(string));
        }

        public async Task<string> GetPageSourceWithJS(string url, Proxy proxy = null)
        {
            var htmlSource = string.Empty;

            IWebDriver driver = null;

            try
            {
                var service = PhantomJSDriverService.CreateDefaultService(this.networkSettings.DriverPath);
                service.LoadImages = false;
                service.HideCommandPromptWindow = true;
                service.IgnoreSslErrors = true;

                // Init proxy
                if (proxy != null)
                {
                    service.Proxy = string.Format("http://{0}:{1}", proxy.Ip, proxy.Port);
                }

                // Emulate browser headers
                var options = new PhantomJSOptions();
                options.AddAdditionalCapability("phantomjs.page.settings.userAgent", Device.DesktopWin64Chrome.Agent);

                using (driver = new PhantomJSDriver(
                           service,
                           options,
                           TimeSpan.FromMilliseconds(this.networkSettings.Timeout)))
                {
                    driver.Manage().Timeouts().PageLoad = TimeSpan.FromMilliseconds(this.networkSettings.Timeout);
                    driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromMilliseconds(this.networkSettings.Timeout);
                    driver.Manage().Timeouts().AsynchronousJavaScript =
                        TimeSpan.FromMilliseconds(this.networkSettings.Timeout);

                    driver.Navigate().GoToUrl(url);
                    htmlSource = driver.PageSource;
                }
            }
            catch (Exception e)
            {
                this.logger.LogDebug(
                    LoggingEvents.GET_ALL_EXCEPTION,
                    e,
                    $"Cannot load web driver {url} and with proxy {proxy?.Ip}:{proxy?.Port}.");
            }
            finally
            {

                driver?.Quit();
                driver?.Dispose();
            }

            if (!string.IsNullOrWhiteSpace(htmlSource))
            {
                return htmlSource;
            }

            return await Task.FromResult(default(string));
        }

        public async Task<SiteMetaData> GetSiteMetaDataGeneral(string url, Device device, Proxy proxy = null)
        {
            var siteMetaData = new SiteMetaData() { Host = url };

            HttpResponseMessage result = null;

            var clientHandler = new HttpClientHandler();
            clientHandler.SslProtocols = SslProtocols.Tls12 | SslProtocols.Tls11 | SslProtocols.Tls;
            if (proxy != null)
            {
                clientHandler.Proxy = new NetworkProxy(proxy);
                clientHandler.UseProxy = true;
            }

            clientHandler.AllowAutoRedirect = true;

            using (var client = new HttpClient(clientHandler))
            {
                // Emulate browser headers
                client.DefaultRequestHeaders.TryAddWithoutValidation("User-Agent", device.Agent);

                client.Timeout = TimeSpan.FromMilliseconds(this.networkSettings.Timeout);
                try
                {
                    result = await client.GetAsync(new UriBuilder(url).Uri);

                    if (!result.IsSuccessStatusCode)
                    {
                        this.logger.LogDebug(
                            LoggingEvents.CANNOT_GET_ACCESS_TO_SITE,
                            $"Cannot load web page url={url} with in proxy {proxy?.Ip}:{proxy?.Port}, because http response {result.StatusCode}");

                        siteMetaData.Status = SiteStatus.NotAvailable;
                        return siteMetaData;
                    }
                }
                catch (Exception e)
                {
                    this.logger.LogDebug(
                        LoggingEvents.CANNOT_GET_ACCESS_TO_SITE,
                        e,
                        $"Cannot load web page url={url} with in proxy {proxy?.Ip}:{proxy?.Port}");

                    siteMetaData.Status = SiteStatus.NotAvailable;
                    return siteMetaData;
                }
            }

            var receiveStream = await result.Content.ReadAsStreamAsync();
            var readStream = new StreamReader(receiveStream, Encoding.UTF8);

            siteMetaData.Html = readStream.ReadToEnd();

            siteMetaData.Headers = this.ParseHeaders(result.Headers);
            siteMetaData.Status = SiteStatus.Available;

            return siteMetaData;
        }

        public async Task<SiteMetaData> GetSiteMetaDataAll(string url, Device device, Proxy proxy = null)
        {
            var siteMetaData = new SiteMetaData() { Host = url, SiteMobileMetaData = new SiteMobileMetaData() };
            IWebDriver driver = null;

            try
            {
                var service = PhantomJSDriverService.CreateDefaultService(this.networkSettings.DriverPath);
                service.LoadImages = true;
                service.HideCommandPromptWindow = true;
                service.IgnoreSslErrors = true;

                // Init proxy
                if (proxy != null)
                {
                    service.Proxy = string.Format("http://{0}:{1}", proxy.Ip, proxy.Port);
                }

                var options = new PhantomJSOptions();
                options.AddAdditionalCapability("phantomjs.page.settings.userAgent", device.Agent);
                options.AddAdditionalCapability("phantomjs.page.settings.resourceTimeout", this.networkSettings.Timeout);
                using (driver = new PhantomJSDriver(
                           service,
                           options,
                           TimeSpan.FromMilliseconds(this.networkSettings.Timeout)))
                    try
                    {
                        driver.Manage().Window.Size = new Size(device.Size.Width, device.Size.Height);

                        driver.Manage().Timeouts().PageLoad = TimeSpan.FromMilliseconds(this.networkSettings.Timeout);
                        driver.Manage().Timeouts().ImplicitWait =
                            TimeSpan.FromMilliseconds(this.networkSettings.Timeout);
                        driver.Manage().Timeouts().AsynchronousJavaScript =
                            TimeSpan.FromMilliseconds(this.networkSettings.Timeout);

                        // load page
                        driver.Navigate().GoToUrl(new UriBuilder(url).Uri);

                        // alert detection
                        try
                        {
                            driver.SwitchTo().Alert();
                            try
                            {
                                driver.SwitchTo().Alert().Accept();
                            }
                            catch(Exception e)
                            {

                            }
                            siteMetaData.SiteMobileMetaData.AlertStatus = AlertStatus.AlertDetected;
                        }
                        catch
                        {
                            siteMetaData.SiteMobileMetaData.AlertStatus = AlertStatus.Done;
                        }

                        IJavaScriptExecutor js = (IJavaScriptExecutor)driver;

                        // check response
                        var responseCode = (long)js.ExecuteScript(
                            @"var req = new XMLHttpRequest(); req.open('GET', document.location, false); req.send(null); var status = req.status;  return status;");
                        if (responseCode != 200)
                        {
                            throw new Exception($"Bad status code {responseCode}, page not load");
                        }

                        if (string.IsNullOrWhiteSpace(driver.PageSource))
                        {
                            throw new Exception($"Bad html source code, page not load");
                        }

                        siteMetaData.EnvironmentVariables = JsonConvert.DeserializeObject<List<string>>(
                            (string)js.ExecuteScript(this.networkSettings.EnvironmentVariableJs));
                        siteMetaData.Headers =
                            this.ParseHeaders((string)js.ExecuteScript(this.networkSettings.HeadersJs));
                        siteMetaData.Html = driver.PageSource;

                        siteMetaData.Status = SiteStatus.Available;
                        siteMetaData.SiteMobileMetaData.FontSizeStatus = FontSizeStatus.Done;
                        siteMetaData.SiteMobileMetaData.TouchScreenStatus = TouchScreenStatus.Done;

                        // check doc and window size
                        var scrollWidth = (long)js.ExecuteScript("return window.document.documentElement.scrollWidth");
                        var clientWidth = (long)js.ExecuteScript("return window.document.documentElement.clientWidth");
                        siteMetaData.SiteMobileMetaData.DocWidthStatus =
                            scrollWidth > clientWidth ? DocWidthStatus.DocTooWide : DocWidthStatus.Done;

                        // check viewPort
                        IWebElement actualViewPort = null;
                        var listViewPorts = driver.FindElements(By.CssSelector("meta[name=\"viewport\"]"));
                        if (listViewPorts.Count > 1)
                        {
                            actualViewPort = listViewPorts[listViewPorts.Count - 1];
                        }
                        else if (listViewPorts.Count == 0)
                        {
                            siteMetaData.SiteMobileMetaData.PortStatus = ViewPortStatus.NoViewPortDeclared;
                        }
                        else
                        {
                            actualViewPort = listViewPorts[0];
                        }

                        if (actualViewPort != null)
                        {
                            var content = actualViewPort.GetAttribute("content");
                            if (string.IsNullOrWhiteSpace(content))
                            {
                                siteMetaData.SiteMobileMetaData.PortStatus = ViewPortStatus.ContentMissed;
                            }
                            else
                            {
                                if (content.IndexOf("width=device-width") == -1)
                                {
                                    siteMetaData.SiteMobileMetaData.PortStatus = ViewPortStatus.HardCodedWidth;
                                }
                                else
                                {
                                    siteMetaData.SiteMobileMetaData.PortStatus = ViewPortStatus.ResponsiveWidth;
                                }
                            }
                        }

                        // check font size
                        var tags = new List<string>()
                                       {
                                           "p",
                                           "button",
                                           "input",
                                           "a",
                                           "h1",
                                           "h2",
                                           "h3",
                                           "h4",
                                           "h5",
                                           "h6",
                                           "li"
                                       };
                        var tagsTouch = new List<string>() { "button", "input", "a" };

                        Parallel.ForEach(
                            tags,
                            (string tag, ParallelLoopState pls) =>
                                {
                                    try
                                    {
                                        var isLarge = (bool)js.ExecuteScript(
                                            "var tags = document.getElementsByTagName('" + tag
                                            + "'); for (i = 0; i < tags.length; i++) { if(window.getComputedStyle(tags[i], null).getPropertyValue('font-size').replace('px','') < 12) return true; } return false");

                                        if (isLarge)
                                        {
                                            siteMetaData.SiteMobileMetaData.FontSizeStatus =
                                                FontSizeStatus.TooSmallSizeFonts;
                                            pls.Break();
                                        }
                                    }
                                    catch (Exception)
                                    {
                                        // ignored
                                    }
                                });
                        var listLargeElement = new List<bool>();
                        Parallel.ForEach(
                            tagsTouch,
                            (string tag) =>
                                {
                                    try
                                    {
                                        listLargeElement.Add(
                                            (bool)js.ExecuteScript(
                                                "var tags = document.getElementsByTagName('" + tag
                                                + "'); var normalSize = 0; var wrongSize = 0; for (i = 0; i < tags.length; i++) {  if(window.getComputedStyle(tags[i], null).getPropertyValue('width').replace('px','') < 48 || window.getComputedStyle(tags[i], null).getPropertyValue('height').replace('px','') < 48) { wrongSize++; }else{normalSize++ }} return normalSize>=wrongSize;"));
                                    }
                                    catch (Exception)
                                    {
                                        // ignored
                                    }
                                });
                        if (listLargeElement.Count < tagsTouch.Count / 2)
                        {
                            siteMetaData.SiteMobileMetaData.FontSizeStatus = FontSizeStatus.TooSmallSizeFonts;
                        }

                        // flash detection
                        if (driver.FindElements(By.TagName("script")).Any(
                                element => element.GetAttribute("src").Contains("swfobject.js")) || driver
                                .FindElements(By.TagName("embed"))
                                .Any(element => element.GetAttribute("src").Contains(".swf")))
                        {
                            siteMetaData.SiteMobileMetaData.FlashStatus = FlashStatus.FlashDetected;
                        }
                        else
                        {
                            siteMetaData.SiteMobileMetaData.FlashStatus = FlashStatus.Done;
                        }


                        // create screenShot
                        Screenshot screenShot = ((ITakesScreenshot)driver).GetScreenshot();

                        var bitmap = Image.FromStream(new MemoryStream(screenShot.AsByteArray)) as Bitmap;
                        if (bitmap != null)
                        {
                            var image = bitmap.Clone(
                                            new Rectangle(
                                                new Point(0, 0),
                                                new Size(device.Size.Width, device.Size.Height)),
                                            bitmap.PixelFormat) as Image;
                            using (var ms = new MemoryStream())
                            {
                                image.Save(ms, image.RawFormat);
                                siteMetaData.SiteMobileMetaData.ImageScreenShot = ms.ToArray();
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        siteMetaData.Status = SiteStatus.NotAvailable;
                        this.logger.LogDebug(
                            LoggingEvents.GET_ALL_EXCEPTION,
                            e,
                            $"Some error when execute {url} and with proxy {proxy?.Ip}:{proxy?.Port}.");
                    }
            }
            catch (Exception e)
            {
                siteMetaData.Status = SiteStatus.NotAvailable;
                this.logger.LogDebug(
                    LoggingEvents.GET_ALL_EXCEPTION,
                    e,
                    $"Cannot load web driver {url} and with proxy {proxy?.Ip}:{proxy?.Port}.");
            }

            return siteMetaData;
        }

        public async Task<CheckProxyResult> CheckProxy(Proxy proxy, int? timeout = null)
        {
            var result = new CheckProxyResult() { Proxy = proxy };

            result.ProxyAnonymity = await this.GetProxyAnonymity(proxy, this.networkSettings.UrlForTestProxy, timeout);

            if (result.ProxyAnonymity == ProxyAnonymity.Offline)
            {
                return result;
            }
            result.IsAvailable = true;
            result.IsHttpsAllowed = await this.IsProxyAvailable(proxy, this.networkSettings.UrlForTestHttps, timeout);

            return result;
        }

        public async Task<bool> IsProxyAvailable(Proxy proxy, string url = null, int? timeout = null)
        {
            var clientHandler = new HttpClientHandler();

            // Init proxy
            clientHandler.Proxy = new NetworkProxy(proxy);
            clientHandler.UseProxy = true;

            clientHandler.SslProtocols = SslProtocols.Tls12 | SslProtocols.Tls11 | SslProtocols.Tls;

            using (var client = new HttpClient(clientHandler))
            {
                // Emulate browser headers
                client.DefaultRequestHeaders.TryAddWithoutValidation("User-Agent", Device.DesktopWin64Chrome.Agent);

                client.Timeout = TimeSpan.FromMilliseconds(timeout ?? this.networkSettings.ProxyTimeout);

                for (int i = 0; i < this.networkSettings.CountCheckProxy; i++)
                {
                    try
                    {
                        var result = await client.GetAsync(new UriBuilder(url ?? this.networkSettings.UrlForTestProxy).Uri);
                        if (!string.IsNullOrEmpty(result.Content.ReadAsStringAsync().Result) && result.IsSuccessStatusCode)
                        {
                            return true;
                        }
                    }
                    catch
                    {
                        // ignored
                    }
                }
            }

            return false;
        }

        public async Task<ProxyAnonymity> GetProxyAnonymity(
             Proxy proxy, string url = null, int? timeout = null)
        {
            var clientHandler = new HttpClientHandler();

            // Init proxy
            clientHandler.Proxy = new NetworkProxy(proxy);
            clientHandler.UseProxy = true;

            clientHandler.SslProtocols = SslProtocols.Tls12 | SslProtocols.Tls11 | SslProtocols.Tls;

            string pageSource = null;

            using (var client = new HttpClient(clientHandler))
            {
                // Emulate browser headers
                client.DefaultRequestHeaders.TryAddWithoutValidation("User-Agent", Device.DesktopWin64Chrome.Agent);

                client.Timeout = TimeSpan.FromMilliseconds(timeout ?? this.networkSettings.ProxyTimeout);

                for (int i = 0; i < this.networkSettings.CountCheckProxy; i++)
                {
                    try
                    {
                        var result = await client.GetAsync(url ?? this.networkSettings.UrlForTestProxy);

                        pageSource = await result.Content.ReadAsStringAsync();

                        if (!string.IsNullOrEmpty(pageSource) && result.IsSuccessStatusCode)
                        {
                            break;
                        }
                    }
                    catch
                    {
                        // ignored
                    }
                }
            }

            if (string.IsNullOrWhiteSpace(pageSource))
            {
                return ProxyAnonymity.Offline;
            }

            if (pageSource.IndexOf("REQUEST_URI", StringComparison.Ordinal) == -1)
            {
                return ProxyAnonymity.Offline;
            }

            var values = new Dictionary<string, string>();
            foreach (Match match in Regex.Matches(pageSource, @"[A-Z_]+ = [^\n]+"))
            {
                string[] info = Regex.Split(match.Value, " = ");
                values.Add(info[0], info[1]);
            }

            var anonymity = ProxyAnonymity.High;

            foreach (var header in values)
            {
                bool revealing;
                if (revealingHeader.TryGetValue(header.Key.ToUpper(), out revealing))
                {
                    if (revealing)
                    {
                        anonymity = ProxyAnonymity.Low;
                        break;
                    }
                }
            }

            if (pageSource.IndexOf(this.networkSettings.LocalIp) != -1)
            {
                anonymity = ProxyAnonymity.None;
            }

            return anonymity;
        }

        public async Task<List<CrawlerData>> Crawler(List<CrawlerData> listUrl, int depth, Proxy proxy = null)
        {
            if (depth > 0)
            {
                var tempUrlList = new List<CrawlerData>();

                for (int i = 0; i < listUrl.Count; i++)
                {
                    if (!listUrl[i].IsVisited)
                    {
                        var pageSource = await this.GetPageSource(listUrl[i].Url.ToString(), proxy);
                        if (pageSource != null)
                        {
                            listUrl[i].IsVisited = true;
                            var htmlDocument = new HtmlDocument();
                            htmlDocument.LoadHtml(pageSource);

                            var list = htmlDocument.DocumentNode.SelectNodes("//a[starts-with(@href,'/') or contains(@href, '" + listUrl[i].Url.Host + "')]");
                            if (list != null)
                            {
                                foreach (var node in list)
                                {
                                    var link = node.GetAttributeValue("href", string.Empty);

                                    if (!string.IsNullOrEmpty(link))
                                    {
                                        try
                                        {
                                            var tempUrl = new UriBuilder(!link.Contains(listUrl[i].Url.Host) ? listUrl[i].Url.Host + link : link).Uri;
                                            if (listUrl[i].LinkList.All(data => !data.Url.Equals(tempUrl)))
                                            {
                                                listUrl[i].LinkList.Add(new CrawlerData() { Url = tempUrl });

                                                if (listUrl.All(data => !data.Url.Equals(tempUrl)) && tempUrlList.All(data => !data.Url.Equals(tempUrl)))
                                                {
                                                    tempUrlList.Add(new CrawlerData() { Url = tempUrl });
                                                }
                                            }
                                        }
                                        catch
                                        {
                                            // uri exception ignore 
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            listUrl.Remove(listUrl[i]);
                        }
                    }
                }

                listUrl.AddRange(tempUrlList);

                return await this.Crawler(listUrl, --depth, proxy);
            }

            return listUrl;
        }

        public async Task<List<CrawlerData>> Crawler(string url, int depth, Proxy proxy = null)
        {
            return await this.Crawler(new List<CrawlerData>() { new CrawlerData(url) }, depth, proxy);
        }

        private List<KeyValuePair<string, string>> ParseHeaders(string line)
        {
            var headers = line.Split('\n');
            var responseHeader = new List<KeyValuePair<string, string>>();
            foreach (var header in headers)
            {
                if (!string.IsNullOrWhiteSpace(header))
                {
                    var key = header.Split(':')[0];
                    var value = header.Split(':')[1].Substring(1);

                    responseHeader.Add(new KeyValuePair<string, string>(key, value));
                }
            }

            return responseHeader;
        }

        private List<KeyValuePair<string, string>> ParseHeaders(HttpResponseHeaders headers)
        {
            List<KeyValuePair<string, string>> responseHeader = new List<KeyValuePair<string, string>>();
            foreach (var item in headers)
            {
                responseHeader.Add(new KeyValuePair<string, string>(item.Key, string.Join(" ", item.Value.ToArray())));
            }

            return responseHeader;
        }

        private static readonly Dictionary<string, bool> revealingHeader =
            new Dictionary<string, bool>()
                {
                    { "HTTP_CACHE_CONTROL", true },
                    { "HTTP_CDN_SRC_IP", true },
                    { "HTTP_CLIENT_IP", true },
                    { "HTTP_REFERER", true },
                    { "HTTP_IF_NONE_MATCH", true },
                    { "HTTP_IF_MODIFIED_SINCE", true },
                    { "HTTP_MAX_FORWARDS", true },
                    { "HTTP_OCT_MAX_AGE", true },
                    { "HTTP_PROXY_AGENT", true },
                    { "HTTP_PROXY_CONNECTION", true },
                    { "HTTP_VIA", true },
                    { "VIA", true },
                    { "HTTP_X_ACCEPT_ENCODING_PRONTOWIFI", true },
                    { "HTTP_X_BLUECOAT_VIA", true },
                    { "HTTP_X_FORWARDED_FOR", true },
                    { "HTTP_X_FORWARD_FOR", true },
                    { "HTTP_X_FORWARDED_HOST", true },
                    { "HTTP_X_FORWARDED_SERVER", true },
                    { "HTTP_X_MATO_PARAM", true },
                    { "HTTP_X_NAI_ID", true },
                    { "HTTP_X_PROXY_ID", true },
                    { "HTTP_X_REAL_IP", true },
                    { "HTTP_X_VIA", true },
                    { "HTTP_XCNOOL_REMOTE_ADDR", true },
                    { "HTTP_XROXY_CONNECTION", true },
                    { "HTTP_XXPECT", true },

                    { "HTTP_ACCEPT", false },
                    { "HTTP_ACCEPT_ENCODING", false },
                    { "HTTP_ACCEPT_LANGUAGE", false },
                    { "HTTP_CONNECTION", false },
                    { "HTTP_HOST", false },
                    { "HTTP_USER_AGENT", false },
                    { "REMOTE_ADDR", false },
                    { "REMOTE_PORT", false },
                    { "REQUEST_METHOD", false },
                    { "REQUEST_TIME", false },
                    { "REQUEST_TIME_FLOAT", false },
                    { "REQUEST_URI", false },
                };

    }
}