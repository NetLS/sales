﻿namespace Sales.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text.RegularExpressions;
    using System.Xml.XPath;

    using Contract.DataAccessLayer.Models;
    using Contract.Infrastructure.DataContracts.Proxy;
    using Contract.Infrastructure.Services;

    using HtmlAgilityPack;

    using Microsoft.Extensions.Logging;

    using Newtonsoft.Json.Linq;

    using Sales.Models;

    public class DataParserService : IDataParserService
    {
        private delegate List<Proxy> ParserDelegate(string data, string pattern, string url);

        private Dictionary<TypeContent, ParserDelegate> actionsDelegates;

        private readonly ILogger logger;

        private string Ipv4Pattern =
                @"^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$";


        public DataParserService(ILoggerFactory logger)
        {
            this.logger = logger.CreateLogger<NLog.ILogger>();

            this.actionsDelegates =
                new Dictionary<TypeContent, ParserDelegate>()
                    {
                        { TypeContent.ConsistentData, this.ParseHtmlData },
                        { TypeContent.DataWithSeparator, this.ParseHtmlDataWithSeparator },
                        { TypeContent.JsonData, this.ParseJsonData },
                    };
        }

        public List<Proxy> Parse(TypeContent content, string data, string pattern, string url)
        {
            return this.actionsDelegates[content](data, pattern, url);
        }

        public List<Proxy> ParseHtmlData(string data, string pattern, string url)
        {
            var proxy = new List<Proxy>();

            var xmlDoc = new HtmlDocument();
            xmlDoc.LoadHtml(data);

            try
            {
                var listIpPort = xmlDoc.DocumentNode.SelectNodes(pattern);

                for (var i = 0; i < listIpPort.Count; i += 2)
                {
                    try
                    {
                        string ip = this.ClearIpOrPort(listIpPort[i].InnerText);
                        int port = int.Parse(this.ClearIpOrPort(listIpPort[i + 1].InnerText));

                        if (!new Regex(this.Ipv4Pattern).IsMatch(ip))
                        {
                            throw new ArgumentException();
                        }

                        proxy.Add(new Proxy() { Ip = ip, Port = port });
                    }
                    catch
                    {
                        // ignore parse errors
                    }
                }
            }
            catch (ArgumentOutOfRangeException e)
            {
                this.logger.LogWarning(
                    LoggingEvents.PARSE_HTML_EXCEPTION_NON_EVEN_NUMBER,
                    e,
                    $"Non even number of ips and ports, check html. URL {url}.");
            }
            catch (NullReferenceException e)
            {
                this.logger.LogWarning(
                    LoggingEvents.PARSE_HTML_EXCEPTION_DONT_MATCH_PATTERN,
                    e,
                    $"Empty list nodes, don't exists matches with pattern. URL {url}.");
            }
            catch (XPathException e)
            {
                this.logger.LogWarning(
                    LoggingEvents.PARSE_HTML_EXCEPTION_XPATH_INVALID,
                    e,
                    $"XPATH is not valid. URL {url} XPATH {pattern}.");
            }
            catch (Exception e)
            {
                this.logger.LogWarning(LoggingEvents.PARSE_ALL_EXCEPTION, e, $"Other exception. URL {url}.");
            }

            return proxy;
        }

        public List<Proxy> ParseHtmlDataWithSeparator(string data, string pattern, string url)
        {
            var proxy = new List<Proxy>();

            var xmlDoc = new HtmlDocument();
            xmlDoc.LoadHtml(data);

            try
            {
                var listIpPort = xmlDoc.DocumentNode.SelectNodes(pattern);

                foreach (HtmlNode t in listIpPort)
                {
                    try
                    {
                        var ipAndPort = t.InnerText.Split(':');

                        string ip = this.ClearIpOrPort(ipAndPort[0]);
                        int port = int.Parse(this.ClearIpOrPort(ipAndPort[1]));

                        if (!new Regex(this.Ipv4Pattern).IsMatch(ip))
                        {
                            throw new ArgumentException();
                        }

                        proxy.Add(new Proxy() { Ip = ip, Port = port });
                    }
                    catch
                    {
                        // ignore parse errors
                    }
                }
            }
            catch (NullReferenceException e)
            {
                this.logger.LogWarning(
                    LoggingEvents.PARSE_HTML_EXCEPTION_DONT_MATCH_PATTERN,
                    e,
                    $"Empty list nodes, don't exists matches with pattern. URL {url}.");
            }
            catch (XPathException e)
            {
                this.logger.LogWarning(
                    LoggingEvents.PARSE_HTML_EXCEPTION_XPATH_INVALID,
                    e,
                    $"XPATH is not valid. URL {url} XPATH {pattern}.");
            }
            catch (Exception e)
            {
                this.logger.LogWarning(LoggingEvents.PARSE_ALL_EXCEPTION, e, $"Other exception. URL {url}.");
            }

            return proxy;
        }

        public List<Proxy> ParseJsonData(string data, string pattern, string url)
        {
            var proxy = new List<Proxy>();

            try
            {
                var json = JToken.Parse(data);
                var listIpPort = json.SelectTokens(pattern).ToList();

                for (int i = 0; i < listIpPort.Count; i += 2)
                {
                    try
                    {
                        string ip = this.ClearIpOrPort(listIpPort[i].ToString());
                        int port = int.Parse(this.ClearIpOrPort(listIpPort[i + 1].ToString()));

                        if (!new Regex(this.Ipv4Pattern).IsMatch(ip))
                        {
                            throw new ArgumentException();
                        }

                        proxy.Add(new Proxy() { Ip = ip, Port = port });
                    }
                    catch
                    {
                        // ignore parse errors
                    }
                }
            }
            catch (NullReferenceException e)
            {
                this.logger.LogWarning(
                    LoggingEvents.PARSE_JSON_EXCEPTION_DONT_MATCH_PATTERN,
                    e,
                    $"Empty list nodes, don't exists matches with pattern. URL {url}.");
            }
            catch (ArgumentOutOfRangeException e)
            {
                this.logger.LogWarning(
                    LoggingEvents.PARSE_JSON_EXCEPTION_JSONPATH_INVALID,
                    e,
                    $"JSONPath is not valid. URL {url} JSONPath {pattern}.");
            }
            catch (Exception e)
            {
                this.logger.LogWarning(LoggingEvents.PARSE_ALL_EXCEPTION, e, $"Other exception. URL {url}.");
            }

            return proxy;
        }

        private string ClearIpOrPort(string str)
        {
            return Regex.Replace(str, @"[^(\d)|(.)]", string.Empty);
        }
    }
}