﻿namespace Sales.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net;
    using System.Security.Authentication;
    using System.Threading;

    using Contract.Infrastructure.DataContracts.Platform;
    using Contract.Infrastructure.DataContracts.Platform.DataDotCom;
    using Contract.Infrastructure.DataContracts.Proxy;
    using Contract.Infrastructure.Services;

    using Microsoft.Extensions.Logging;
    using Microsoft.Extensions.Options;

    using Newtonsoft.Json;

    using OpenQA.Selenium;
    using OpenQA.Selenium.Chrome;
    using OpenQA.Selenium.Support.UI;

    using Proxy = Sales.Models.Proxy;

    public class DataDotComParserService : ISearchPlatformParserService
    {
        private DataDotComSettings dotComSettings;

        private readonly ILogger logger;

        public DataDotComParserService(IOptions<DataDotComSettings> options, ILoggerFactory logger)
        {
            this.dotComSettings = options.Value;

            this.logger = logger.CreateLogger<NLog.ILogger>();
        }

        public SearchPlatformMetaData Parse(BaseFilterModel filter, Proxy proxy, NetworkCredential credential = null)
        {

            SearchPlatformMetaData platformMetaData = new SearchPlatformMetaData();

            try
            {
                if (!(filter is DataDotComFilterModel filterModel))
                {
                    throw new ArgumentException($"Cannot cast base filter to {nameof(DataDotComFilterModel)}");
                }

                var options = new ChromeOptions();

                options.AddArgument("--headless");
                options.AddArgument("--disable-gpu");
                options.AddArgument("--log-level=2");
                options.AddArgument("--start-maximized");

                if (proxy != null)
                {
                    options.Proxy =
                        new OpenQA.Selenium.Proxy()
                        {
                            HttpProxy = string.Format("{0}:{1}", proxy.Ip, proxy.Port)
                        };
                }

                using (IWebDriver driver = new ChromeDriver(this.dotComSettings.DriverPath, options, TimeSpan.FromMilliseconds(this.dotComSettings.Timeout)))
                {
                    driver.Manage().Timeouts().PageLoad = TimeSpan.FromMilliseconds(this.dotComSettings.Timeout);
                    driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromMilliseconds(this.dotComSettings.Timeout);
                    driver.Manage().Timeouts().AsynchronousJavaScript =
                        TimeSpan.FromMilliseconds(this.dotComSettings.Timeout);

                    if (!this.Login(driver))
                    {
                        throw new InvalidCredentialException("Login failed, cannot login with current credential");
                    }

                    this.Search(driver, filterModel);
                    
                    do
                    {
                        
                        var result = new PageScanResultModel { Urls = this.ParseUrl(driver) };
                        result.UrlPage = driver.Url;
                        platformMetaData.ScanResultModels.Add(result);
                    }
                    while (this.NextPage(driver));

                    platformMetaData.IsSuccesses = true;

                    driver.Close();
                    driver.Quit();
                    driver.Dispose();
                }
            }
            catch (ArgumentException e)
            {
                platformMetaData.IsSuccesses = false;
                this.logger.LogError(LoggingEvents.GET_ALL_EXCEPTION, e, $"Cannot cast base filter to concrete {this.dotComSettings.DriverPath} and with proxy {proxy.Ip}:{proxy.Port}.", e);
            }
            catch (WebDriverException e)
            {
                platformMetaData.IsSuccesses = false;
                this.logger.LogError(LoggingEvents.GET_ALL_EXCEPTION, e, $"Cannot load web driver {this.dotComSettings.DriverPath} and with proxy {proxy.Ip}:{proxy.Port}.", e);
            }
            catch (InvalidCredentialException e)
            {
                platformMetaData.IsSuccesses = false;
                this.logger.LogCritical(LoggingEvents.GET_ALL_EXCEPTION, e, $"Cannot login in system with login:{this.dotComSettings.Login} password:{this.dotComSettings.Password}", e);
            }
            catch (Exception e)
            {
                platformMetaData.IsSuccesses = false;
                this.logger.LogError(LoggingEvents.GET_ALL_EXCEPTION, e, "Some other error has taken place", e);
            }

            return platformMetaData;
        }

        private bool Login(IWebDriver driver)
        {
            driver.Navigate().GoToUrl(this.dotComSettings.LoginUrl);

            driver.FindElement(By.Id(this.dotComSettings.LoginSetUp)).SendKeys(this.dotComSettings.Login);
            driver.FindElement(By.Id(this.dotComSettings.PasswordSetUp)).SendKeys(this.dotComSettings.Password);

            driver.FindElement(By.Id(this.dotComSettings.CheckBoxSetUp)).Click();

            driver.FindElement(By.Id(this.dotComSettings.ButtonSetUp)).Submit();

            return driver.Url == this.dotComSettings.HomeUrl;
        }

        private void Search(IWebDriver driver, DataDotComFilterModel filterModel)
        {
            driver.Navigate().GoToUrl(this.dotComSettings.SearchUrl);
            IJavaScriptExecutor js = (IJavaScriptExecutor)driver;
            js.ExecuteScript(this.dotComSettings.ExpandAreasJS);
            js.ExecuteScript(this.dotComSettings.ExpandDropDownJS);

            foreach (var item in this.dotComSettings.LocationXPath)
            {
                js.ExecuteScript(string.Format(this.dotComSettings.ElementClickJS, item, "\'" + string.Join("\' , \'", filterModel.Location.ToArray()) + "\'"));
            }

            foreach (var item in this.dotComSettings.IndustryXPath)
            {
                js.ExecuteScript(string.Format(this.dotComSettings.ElementClickJS, item, "\'" + string.Join("\' , \'", filterModel.Industry.ToArray()) + "\'"));
            }

            driver.FindElement(By.XPath(this.dotComSettings.ButtonSearchXPath)).Click();
            Thread.Sleep(2000);
        }

        private IEnumerable<string> ParseUrl(IWebDriver driver)
        {
            IJavaScriptExecutor js = (IJavaScriptExecutor)driver;
            return JsonConvert.DeserializeObject<List<string>>(
                (string)js.ExecuteScript(this.dotComSettings.UrlXPath));
        }

        private bool NextPage(IWebDriver driver)
        {
            var nextButton = driver.FindElement(By.XPath(this.dotComSettings.NextPageXPath));
            if (nextButton.GetAttribute("class").Contains(this.dotComSettings.ClassNextPage))
            {
                nextButton.Click();
                Thread.Sleep(2000);
                return true;
            }

            return false;
        }
    }
}