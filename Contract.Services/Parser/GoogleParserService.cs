﻿namespace Sales.Services
{
    using System;
    using System.Collections.Generic;
    using System.Net;
    using System.Threading;

    using Contract.Infrastructure.DataContracts.Platform;
    using Contract.Infrastructure.DataContracts.Platform.Google;
    using Contract.Infrastructure.DataContracts.Proxy;
    using Contract.Infrastructure.Services;

    using Microsoft.Extensions.Logging;
    using Microsoft.Extensions.Options;

    using OpenQA.Selenium;
    using OpenQA.Selenium.Chrome;

    using Proxy = Sales.Models.Proxy;

    public class GoogleParserService : ISearchPlatformParserService
    {
        private GoogleSettings dotComSettings;

        private readonly ILogger logger;

        public GoogleParserService(IOptions<GoogleSettings> options, ILoggerFactory logger)
        {
            this.dotComSettings = options.Value;

            this.logger = logger.CreateLogger<NLog.ILogger>();
        }

        public SearchPlatformMetaData Parse(BaseFilterModel filter, Proxy proxy, NetworkCredential credential = null)
        {
            SearchPlatformMetaData platformMetaData = new SearchPlatformMetaData();

            try
            {
                if (!(filter is GoogleFilterModel filterModel))
                {
                    throw new ArgumentException($"Cannot cast base filter to {nameof(GoogleFilterModel)}");
                }

                var options = new ChromeOptions();

                options.AddArgument("--headless");
                options.AddArgument("--disable-gpu");
                options.AddArgument("--log-level=2");
                options.AddArgument("--start-maximized");

                if (proxy != null)
                {
                    options.Proxy =
                        new OpenQA.Selenium.Proxy()
                        {
                            HttpProxy = string.Format("{0}:{1}", proxy.Ip, proxy.Port)
                        };
                }

                using (IWebDriver driver = new ChromeDriver(this.dotComSettings.DriverPath, options, TimeSpan.FromMilliseconds(this.dotComSettings.Timeout)))
                {
                    driver.Manage().Timeouts().PageLoad = TimeSpan.FromMilliseconds(this.dotComSettings.Timeout);
                    driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromMilliseconds(this.dotComSettings.Timeout);
                    driver.Manage().Timeouts().AsynchronousJavaScript =
                        TimeSpan.FromMilliseconds(this.dotComSettings.Timeout);

                    this.Search(driver, filterModel);

                    do
                    {

                        var result = new PageScanResultModel { Urls = this.ParseUrl(driver) };
                        result.UrlPage = driver.Url;
                        platformMetaData.ScanResultModels.Add(result);
                    }
                    while (this.NextPage(driver));

                    platformMetaData.IsSuccesses = true;

                    driver.Close();
                    driver.Quit();
                    driver.Dispose();
                }
            }
            catch (ArgumentException e)
            {
                platformMetaData.IsSuccesses = false;
                this.logger.LogError(LoggingEvents.GET_ALL_EXCEPTION, e, $"Cannot cast base filter to concrete {this.dotComSettings.DriverPath} and with proxy {proxy.Ip}:{proxy.Port}.", e);
            }
            catch (WebDriverException e)
            {
                platformMetaData.IsSuccesses = false;
                this.logger.LogError(LoggingEvents.GET_ALL_EXCEPTION, e, $"Cannot load web driver {this.dotComSettings.DriverPath} and with proxy {proxy.Ip}:{proxy.Port}.", e);
            }
            catch (Exception e)
            {
                platformMetaData.IsSuccesses = false;
                this.logger.LogError(LoggingEvents.GET_ALL_EXCEPTION, e, "Some other error has taken place", e);
            }

            return platformMetaData;
        }

        private void Search(IWebDriver driver, GoogleFilterModel filterModel)
        {
            var searchUrl = string.Empty;
            
            foreach (var location in filterModel.Location)
            {
                searchUrl += location + " ";
            }
            foreach (var industry in filterModel.Industry)
            {
                searchUrl += "\"" + industry + "\" OR ";
            }

            driver.Navigate().GoToUrl(string.Format(this.dotComSettings.SearchUrl, searchUrl));
           
            Thread.Sleep(2000);
        }

        private bool NextPage(IWebDriver driver)
        {
            var nextButton = driver.FindElement(By.XPath(this.dotComSettings.NextPageXPath));
            try
            {
                driver.FindElement(By.XPath(this.dotComSettings.NextPageLabelXpath));
                nextButton.Click();
                Thread.Sleep(5000);
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        private IEnumerable<string> ParseUrl(IWebDriver driver)
        {
            var list = new List<string>();
            var items = driver.FindElements(By.XPath(this.dotComSettings.SiteNameLabelsXpath));
            foreach (var webElement in items)
            {
                webElement.Click();
                Thread.Sleep(3000);
                try
                {
                    var item = driver.FindElement(By.XPath(this.dotComSettings.UrlXPath));
                    list.Add(item.GetAttribute("innerText"));
                }
                catch (Exception e)
                {

                }
            }

            return list;
        }
    }
}