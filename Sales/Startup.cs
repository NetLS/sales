﻿namespace Sales
{
    using System.IO;
    using System.Threading.Tasks;

    using Contract.DataAccessLayer.Interfaces;
    using Contract.DataAccessLayer.Models;
    using Contract.DataAccessLayer.Repository;
    using Contract.Infrastructure.Services;
    using Contract.Services.Database;

    using Microsoft.AspNetCore.Builder;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Routing;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;
    using Microsoft.Extensions.Logging;

    using NLog;
    using NLog.Extensions.Logging;

    using ProxyScraper;

    using Sales.Models;
    using Sales.Services;

    public class Startup
    {
        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddEnvironmentVariables();

            this.Configuration = builder.Build();
        }

        public IConfigurationRoot Configuration { get; set; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddLogging();
            services.AddOptions();
            services.AddRouting();

            services.Configure<NetworkSettings>(
                options => this.Configuration.GetSection("NetworkSettings").Bind(options));

            services.Configure<ProxyScraperSettings>(
                options => this.Configuration.GetSection("ProxyScraperSettings").Bind(options));

            services.AddTransient<IDataParserService, DataParserService>();
            services.AddTransient<INetworkService, NetworkService>();

            services.AddTransient<IUnitOfWork>(
                provider => new EFUnitOfWork(this.Configuration.GetConnectionString("DefaultConnection")));

            services.AddTransient<IProxyService, ProxyService>();
            services.AddTransient<ISiteService, SiteService>();
            services.AddTransient<ILoggerFactory>(provider => new LoggerFactory().AddNLog());
            
            var config = new AutoMapper.MapperConfiguration(
                cfg =>
                    {
                        cfg.CreateMap<Proxy, ProxyModel>()
                            .ForMember(model => model.ProxyIp, expression => expression.MapFrom(proxy => proxy.Ip))
                            .ForMember(model => model.ProxyPort, expression => expression.MapFrom(proxy => proxy.Port));
                        cfg.CreateMap<ProxyModel, Proxy>()
                            .ForMember(proxy => proxy.Ip, expression => expression.MapFrom(model => model.ProxyIp))
                            .ForMember(proxy => proxy.Port, expression => expression.MapFrom(model => model.ProxyPort));

                        cfg.CreateMap<AddDeleteProxyModel, ProxyModel>()
                            .ForMember(model => model.ProxyIp, expression => expression.MapFrom(proxy => proxy.Ip))
                            .ForMember(model => model.ProxyPort, expression => expression.MapFrom(proxy => proxy.Port));
                        cfg.CreateMap<ProxyModel, AddDeleteProxyModel>()
                            .ForMember(proxy => proxy.Ip, expression => expression.MapFrom(model => model.ProxyIp))
                            .ForMember(proxy => proxy.Port, expression => expression.MapFrom(model => model.ProxyPort));


                        cfg.CreateMap<Proxy, AddDeleteProxyModel>();
                        cfg.CreateMap<AddDeleteProxyModel, Proxy>();
                    });

            var mapper = config.CreateMapper();
            services.AddSingleton(mapper);

            services.AddScoped<Scraper>();
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddNLog();
#if RELEASE
            loggerFactory.ConfigureNLog("nlog.config");
#elif (DEBUG)
            loggerFactory.ConfigureNLog("nlog.Development.config");
#endif

            LogManager.Configuration.Variables["connectionString"] =
                this.Configuration.GetConnectionString("DefaultConnection");

            var routeBuilder = new RouteBuilder(app);
            routeBuilder.MapGet(
                "Scraper/scrapeNew",
                delegate(HttpContext context)
                    {
                        Task.Run(() => app.ApplicationServices.GetService<Scraper>().Scrape());
                        context.Response.StatusCode = 200;
                        return Task.FromResult(context);
                    });

            routeBuilder.MapGet(
                "Scraper/checkExist",
                delegate(HttpContext context)
                    {
                        Task.Run(() => app.ApplicationServices.GetService<Scraper>().CheckExistingProxy());
                        context.Response.StatusCode = 200;
                        return Task.FromResult(context);
                    });

            app.UseRouter(routeBuilder.Build());
        }
    }
}