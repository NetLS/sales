﻿namespace ProxyScraper
{
    using System;
    using System.Collections.Concurrent;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;

    using AutoMapper;

    using Contract.DataAccessLayer.Models;
    using Contract.Infrastructure.DataContracts.Network;
    using Contract.Infrastructure.DataContracts.Proxy;
    using Contract.Infrastructure.Services;

    using Microsoft.Extensions.Logging;
    using Microsoft.Extensions.Options;

    using Sales;
    using Sales.Models;

    using Proxy = Sales.Models.Proxy;

    public class Scraper
    {
        private IProxyService proxyService;

        private ISiteService siteService;

        private INetworkService networkService;

        private IDataParserService dataParser;

        private ILogger logger;

        private IMapper mapper;

        private ProxyScraperSettings settings;

        private SemaphoreSlim semaphore;


        public Scraper(
            IProxyService proxyService,
            ISiteService siteService,
            INetworkService networkService,
            IDataParserService dataParser,
            ILoggerFactory logger,
            IMapper mapper,
            IOptions<ProxyScraperSettings> options)
        {
            this.settings = options.Value;
            this.proxyService = proxyService;
            this.siteService = siteService;
            this.networkService = networkService;
            this.dataParser = dataParser;

            this.logger = logger.CreateLogger<NLog.ILogger>();

            this.mapper = mapper;

            this.semaphore = new SemaphoreSlim(this.settings.CountScrapeSiteThread);
        }


        public async Task Scrape()
        {
            // Select sites for scrape proxy
            var sites = this.siteService.GetAll(new SiteFilterModel());//.Where(model => model.NextCheck < DateTime.Now);

            // Get proxy from DB
            var proxiesFromDb = new ConcurrentBag<ProxyModel>(
                this.proxyService.GetAll(
                        new ProxyFilterModel()
                        {
                            IsHttpAllowed = true,
                            ProxyType =
                                    new List<ProxyAnonymity>()
                                        {
                                            ProxyAnonymity.High,
                                            ProxyAnonymity.Low
                                        },
                            limit = this.settings.CountProxyForScrape
                        })
                    .ToList());

            // Check list proxy for working
            ConcurrentQueue<ProxyModel> proxiesForScrape = new ConcurrentQueue<ProxyModel>();
            var checkProxyForScrapeTasks = proxiesFromDb.Select(
                model => Task.Run(
                    async () =>
                        {
                            var proxy = this.mapper.Map<Proxy>(model);
                            var result = await this.networkService.CheckProxy(proxy);

                            if (result.IsAvailable)
                            {
                                proxiesForScrape.Enqueue(model);
                            }
                        }));

            // Wait for checked proxy
            await Task.WhenAll(checkProxyForScrapeTasks);

            var siteProxyScrape = sites.Select(
                currentSite => Task.Run(
                    () =>
                        {
                            try
                            {


                                // Update site check
                                this.siteService.Update(currentSite.SiteId);

                                // Create pagination list
                                var listUrlPagination = new ConcurrentBag<string> { currentSite.Url };

                                if (currentSite.MoveStep != null && currentSite.MoveFinish != null
                                    && !string.IsNullOrEmpty(currentSite.MoveTemplate))
                                {
                                    for (int i = currentSite.MoveStep.Value;
                                         i < currentSite.MoveFinish.Value;
                                         i += currentSite.MoveStep.Value)
                                    {
                                        listUrlPagination.Add(currentSite.Url + string.Format(currentSite.MoveTemplate, i));
                                    }
                                }

                                // Create list with available proxy for scrape concrete site
                                ConcurrentQueue<Proxy> proxiesForConcreteSite = new ConcurrentQueue<Proxy>();
                                var count =
                                    proxiesForScrape.Count > listUrlPagination.Count
                                    * this.settings.CoefficientCountForScrapePagination
                                        ? listUrlPagination.Count * this.settings.CoefficientCountForScrapePagination
                                        : proxiesForScrape.Count;
                                for (int i = 0; i < count;)
                                {
                                    if (proxiesForScrape.TryDequeue(out ProxyModel model))
                                    {
                                        if (model.SiteId != currentSite.SiteId)
                                        {
                                            proxiesForConcreteSite.Enqueue(this.mapper.Map<Proxy>(model));
                                            i++;
                                        }
                                    }

                                    proxiesForScrape.Enqueue(model);
                                }

                                // Get source and parse
                                var checkedProxy = new ConcurrentBag<AddDeleteProxyModel>();
                                var scrapedProxy = new ConcurrentBag<Proxy>();

                                Parallel.ForEach(
                                    listUrlPagination,
                                    (url) =>
                                        {
                                            var readyProxy = this.Parse(url, currentSite, proxiesForConcreteSite).Result;
                                            foreach (var proxy in readyProxy)
                                            {
                                                scrapedProxy.Add(proxy);
                                            }
                                        });

                                Parallel.ForEach(
                                    scrapedProxy,
                                    (proxy) =>
                                        {
                                        // check proxy with timeout from settings
                                        var result = this.networkService.CheckProxy(proxy, this.settings.MaxTimeout)
                                                .Result;

                                            if (result.IsAvailable && result.ProxyAnonymity != ProxyAnonymity.Offline)
                                            {
                                                var mapProxy = this.mapper.Map<AddDeleteProxyModel>(proxy);
                                                mapProxy.IsHttpsAllowed = result.IsHttpsAllowed;
                                                mapProxy.SiteId = currentSite.SiteId;
                                                mapProxy.TypeAnonymity = (int)result.ProxyAnonymity;

                                                checkedProxy.Add(mapProxy);
                                            }
                                        });

                                if (checkedProxy.Count > 0)
                                {
                                    this.proxyService.CreateOrUpdate(checkedProxy);
                                }
                            }
                            catch (Exception e)
                            {
                                this.logger.LogError(LoggingEvents.GET_ALL_EXCEPTION, e, "Some error taken place");
                            }
                        }));

            await Task.WhenAll(siteProxyScrape);
        }

        public async Task CheckExistingProxy()
        {
            int limit = this.settings.DbSelectStep;
            int offset = 0;

            ConcurrentBag<ProxyModel> proxiesFromDb = null;
            do
            {
                // Get proxy from DB
                proxiesFromDb = new ConcurrentBag<ProxyModel>(
                    this.proxyService.GetAll(
                            new ProxyFilterModel()
                            {
                                limit = limit,
                                offset = offset
                            })
                        .ToList());

                // Check list proxy for working
                ConcurrentQueue<ProxyModel> proxies = new ConcurrentQueue<ProxyModel>();
                var checkProxyTasks = proxiesFromDb
                    .Select(
                        model => Task.Run(
                            async () =>
                                {
                                    var proxy = this.mapper.Map<Proxy>(model);
                                    var result = await this.networkService.CheckProxy(proxy);

                                    if (result.IsAvailable)
                                    {
                                        proxies.Enqueue(model);
                                    }
                                }));

                // Wait for checked proxy
                await Task.WhenAll(checkProxyTasks);

                if (proxies.Count > 0)
                {
                    this.proxyService.CreateOrUpdate(this.mapper.Map<List<AddDeleteProxyModel>>(proxies.ToList()));
                }

                // Next list proxy
                offset += limit;
            }
            while (proxiesFromDb.Any());
        }

        private async Task<ConcurrentBag<Proxy>> Parse(string url, SiteModel model, ConcurrentQueue<Proxy> proxies)
        {
            await this.semaphore.WaitAsync();

            var parsedData = new ConcurrentBag<Proxy>();

            // Index of loop indicate count of attempts to load page source else possible infinite loop
            for (int i = 0; i < this.settings.CountTryToScrapeSite; i++)
            {
                // Get proxy from queue
                proxies.TryDequeue(out Proxy proxy);

                // With proxy try to load page source one of two ways: if js need use console browser, else httpWebRequest
                var result = model.IsJSNeed
                                    ? await this.networkService.GetPageSourceWithJS(url, proxy)
                                    : await this.networkService.GetPageSource(url, proxy);

                // Return proxy to end of queue
                proxies.Enqueue(proxy);

                if (result != null)
                {
                    // Parse if content is available
                    parsedData = new ConcurrentBag<Proxy>(this.dataParser.Parse(model.Content, result, model.XPath, url));

                    // If parse success(array have elements) then break form loop, else continue
                    if (parsedData.Any())
                    {
                        break;
                    }
                }
            }

            this.semaphore.Release();

            return parsedData;
        }
    }
}