﻿INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('1&1', '1and1.svg', 'http://1and1.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('1C-Bitrix', '1C-Bitrix.png', 'http://www.1c-bitrix.ru') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('2z Project', '2z Project.png', 'http://2zproject-cms.ru') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('3DM', '3DM.png', 'http://www.3ware.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('3dCart', '3dCart.png', 'http://www.3dcart.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('3ware', '3ware.png', 'http://www.3ware.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('AD EBiS', 'ebis.png', 'http://www.ebis.ne.jp') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('AMPcms', 'AMPcms.png', 'http://www.ampcms.org') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('AOLserver', 'AOLserver.png', 'http://aolserver.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('AT Internet Analyzer', 'AT Internet.png', 'http://atinternet.com/en') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('AT Internet XiTi', 'AT Internet.png', 'http://atinternet.com/en') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('ATEN', 'ATEN.png', 'http://www.aten.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('AWStats', 'AWStats.png', 'http://awstats.sourceforge.net') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Accessible Portal', 'Accessible Portal.png', 'http://www.accessibleportal.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Act-On', 'ActOn.png', 'http://act-on.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Prebid', 'Prebid.png', 'http://prebid.org') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('AdInfinity', 'AdInfinity.png', 'http://adinfinity.com.au') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('AdRiver', 'AdRiver.png', 'http://adriver.ru') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('AdRoll', 'AdRoll.svg', 'http://adroll.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Adcash', 'Adcash.svg', 'http://adcash.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('AddShoppers', 'AddShoppers.png', 'http://www.addshoppers.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('AddThis', 'AddThis.svg', 'http://www.addthis.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('AddToAny', 'AddToAny.png', 'http://www.addtoany.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Adminer', 'adminer.png', 'http://www.adminer.org') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Adnegah', 'adnegah.png', 'https://Adnegah.net') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Adobe ColdFusion', 'Adobe ColdFusion.svg', 'http://adobe.com/products/coldfusion-family.html') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Adobe Experience Manager', 'Adobe Experience Manager.svg', 'http://www.adobe.com/au/marketing-cloud/enterprise-content-management.html') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Adobe GoLive', 'Adobe GoLive.png', 'http://www.adobe.com/products/golive') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Adobe Muse', 'Adobe Muse.svg', 'http://muse.adobe.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Adobe RoboHelp', 'Adobe RoboHelp.svg', 'http://adobe.com/products/robohelp.html') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Advanced Web Stats', 'Advanced Web Stats.png', 'http://www.advancedwebstats.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Advert Stream', 'Advert Stream.png', 'http://www.advertstream.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Adzerk', 'Adzerk.png', 'http://adzerk.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Aegea', 'Aegea.png', 'http://blogengine.ru') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('AfterBuy', 'after-buy.png', 'http://www.afterbuy.de') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Aircall', 'aircall.png', 'http://aircall.io') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Airee', 'Airee.png', 'http://xn--80aqc2a.xn--p1ai') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Akamai', 'Akamai.png', 'http://akamai.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Akka HTTP', 'akka-http.png', 'http://akka.io') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Algolia Realtime Search', 'Algolia Realtime Search.svg', 'http://www.algolia.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Allegro RomPager', 'Allegro RomPager.png', 'http://allegrosoft.com/embedded-web-server-s2') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('AlloyUI', 'AlloyUI.png', 'http://www.alloyui.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Amaya', 'Amaya.png', 'http://www.w3.org/Amaya') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Amazon Cloudfront', 'Amazon-Cloudfront.svg', 'http://aws.amazon.com/cloudfront/') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Amazon EC2', 'aws-ec2.svg', 'http://aws.amazon.com/ec2/') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Amazon S3', 'aws-s3.svg', 'http://aws.amazon.com/s3/') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Ametys', 'Ametys.png', 'http://ametys.org') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Amiro.CMS', 'Amiro.CMS.png', 'http://amirocms.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Angular Material', 'Angular.svg', 'http://material.angularjs.org') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('AngularJS', 'AngularJS.svg', 'http://angularjs.org') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Apache', 'Apache.svg', 'http://apache.org') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Apache HBase', 'Apache HBase.png', 'http://hbase.apache.org') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Apache Hadoop', 'Apache Hadoop.svg', 'http://hadoop.apache.org') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Apache JSPWiki', 'Apache JSPWiki.png', 'http://jspwiki.org') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Apache Tomcat', 'Apache Tomcat.svg', 'http://tomcat.apache.org') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Apache Traffic Server', 'Apache Traffic Server.png', 'http://trafficserver.apache.org/') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Apache Wicket', 'Apache Wicket.svg', 'http://wicket.apache.org') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('ApexPages', 'ApexPages.png', 'https://developer.salesforce.com/docs/atlas.en-us.apexcode.meta/apexcode/apex_intro.htm') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Apostrophe CMS', 'apostrophecms.svg', 'http://apostrophecms.org') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('AppNexus', 'AppNexus.svg', 'http://appnexus.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Arastta', 'Arastta.svg', 'http://arastta.org') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Arc Forum', 'Arc Forum.png', 'http://arclanguage.org') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('ArcGIS API for JavaScript', 'arcgis_icon.png', 'https://developers.arcgis.com/javascript/') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Artifactory', 'Artifactory.svg', 'http://jfrog.com/open-source/#os-arti') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Artifactory Web Server', 'Artifactory.svg', 'http://jfrog.com/open-source/#os-arti') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('ArvanCloud', 'ArvanCloud.png', 'http://www.ArvanCloud.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('AsciiDoc', 'AsciiDoc.png', 'http://www.methods.co.nz/asciidoc') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Asymptix PHP Framework', 'Asymptix PHP Framework.png', 'http://github.com/Asymptix/Framework') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Atlassian Bitbucket', 'Atlassian Bitbucket.svg', 'http://www.atlassian.com/software/bitbucket/overview/') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Atlassian Confluence', 'Atlassian Confluence.svg', 'http://www.atlassian.com/software/confluence/overview/team-collaboration-software') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Atlassian FishEye', 'Atlassian FishEye.svg', 'http://www.atlassian.com/software/fisheye/overview/') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Atlassian Jira', 'Atlassian Jira.svg', 'http://www.atlassian.com/software/jira/overview/') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Atlassian Jira Issue Collector', 'Atlassian Jira.svg', 'http://www.atlassian.com/software/jira/overview/') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Aurelia', 'Aurelia.svg', 'http://aurelia.io') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Avangate', 'Avangate.svg', 'http://avangate.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('BEM', 'BEM.png', 'http://en.bem.info') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('BIGACE', 'BIGACE.png', 'http://bigace.de') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Bablic', 'bablic.png', 'https://www.bablic.com/') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Backbone.js', 'Backbone.js.png', 'http://backbonejs.org') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Backdrop', 'Backdrop.png', 'http://backdropcms.org') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Banshee', 'Banshee.png', 'http://www.banshee-php.org') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('BaseHTTP', 'BaseHTTP.png', 'http://docs.python.org/2/library/basehttpserver.html') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('BigDump', '', 'http://www.ozerov.de/bigdump.php') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Bigcommerce', 'Bigcommerce.png', 'http://www.bigcommerce.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Bigware', 'Bigware.png', 'http://bigware.de') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('BittAds', 'BittAds.png', 'http://bittads.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Blesta', 'Blesta.png', 'http://www.blesta.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Blip.tv', 'Blip.tv.png', 'http://blip.tv') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Blogger', 'Blogger.png', 'http://www.blogger.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Bluefish', 'Bluefish.png', 'http://sourceforge.net/projects/bluefish') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Boa', '', 'http://www.boa.org') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Boba.js', '', 'http://boba.space150.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Bold Chat', 'BoldChat.png', 'https://www.boldchat.com/') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Bolt', 'Bolt.png', 'http://bolt.cm') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Bonfire', 'Bonfire.png', 'http://cibonfire.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Bootstrap Table', 'Bootstrap Table.svg', 'http://bootstrap-table.wenzhixin.net.cn/') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Bounce Exchange', 'Bounce Exchange.svg', 'http://www.bounceexchange.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Brother', 'Brother.png', 'http://www.brother.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('BrowserCMS', 'BrowserCMS.png', 'http://browsercms.org') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Bubble', 'bubble.png', 'http://bubble.is') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('BugSense', 'BugSense.png', 'http://bugsense.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('BugSnag', 'BugSnag.png', 'http://bugsnag.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Bugzilla', 'Bugzilla.png', 'http://www.bugzilla.org') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Burning Board', 'Burning Board.png', 'http://www.woltlab.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Business Catalyst', 'Business Catalyst.png', 'http://businesscatalyst.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('BuySellAds', 'BuySellAds.png', 'http://buysellads.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('C++', 'C++.png', 'http://isocpp.org') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('CFML', 'CFML.png', 'http://adobe.com/products/coldfusion-family.html') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('CKEditor', 'CKEditor.png', 'http://ckeditor.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('CMS Made Simple', 'CMS Made Simple.png', 'http://cmsmadesimple.org') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('CMSimple', '', 'http://www.cmsimple.org/en') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('CO2Stats', 'CO2Stats.png', 'http://co2stats.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('CPG Dragonfly', 'CPG Dragonfly.png', 'http://dragonflycms.org') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('CS Cart', 'CS Cart.png', 'http://www.cs-cart.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('CacheFly', 'CacheFly.png', 'http://www.cachefly.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Caddy', 'caddy.svg', 'http://caddyserver.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('CakePHP', 'CakePHP.png', 'http://cakephp.org') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Canon', 'Canon.png', 'http://www.canon.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Canon HTTP Server', 'Canon.png', 'http://www.canon.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Captch Me', 'Captch Me.svg', 'http://captchme.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Carbon Ads', 'Carbon Ads.png', 'http://carbonads.net') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Cargo', 'Cargo.png', 'http://cargocollective.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Catberry.js', 'Catberry.js.png', 'http://catberry.org') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Catwalk', 'Catwalk.png', 'http://www.canon.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('CentOS', 'CentOS.png', 'http://centos.org') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('CenteHTTPd', 'CenteHTTPd.png', 'http://cente.jp/cente/app/HTTPdc.html') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Chameleon', 'Chameleon.png', 'http://chameleon-system.de') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Chamilo', 'Chamilo.png', 'http://www.chamilo.org') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Chartbeat', 'Chartbeat.png', 'http://chartbeat.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Cherokee', 'Cherokee.png', 'http://www.cherokee-project.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('CherryPy', 'CherryPy.png', 'http://www.cherrypy.org') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Chitika', 'Chitika.png', 'http://chitika.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Ckan', 'Ckan.png', 'http://ckan.org/') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('ClickHeat', 'ClickHeat.png', 'http://www.labsmedia.com/clickheat/index.html') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('ClickTale', 'ClickTale.png', 'http://www.clicktale.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Clicky', 'Clicky.png', 'http://getclicky.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Clientexec', 'Clientexec.png', 'http://www.clientexec.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Clipboard.js', 'Clipboard.js.svg', 'https://clipboardjs.com/') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('CloudCart', 'cloudcart.svg', 'http://cloudcart.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('CloudFlare', 'CloudFlare.svg', 'http://www.cloudflare.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Cloudera', 'Cloudera.png', 'http://www.cloudera.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('CodeIgniter', 'CodeIgniter.png', 'http://codeigniter.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('CodeMirror', 'CodeMirror.png', 'http://codemirror.net') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Comandia', 'Comandia.svg', 'http://comandia.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Commerce Server', 'Commerce Server.png', 'http://commerceserver.net') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('CompaqHTTPServer', 'HP.svg', 'http://www.hp.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Concrete5', 'Concrete5.png', 'http://concrete5.org') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Connect', 'Connect.png', 'http://www.senchalabs.org/connect') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Contao', 'Contao.png', 'http://contao.org') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Contenido', 'Contenido.png', 'http://contenido.org/en') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Contens', 'Contens.png', 'http://www.contens.com/en/pub/index.cfm') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('ContentBox', 'ContentBox.png', 'http://www.gocontentbox.org') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('ConversionLab', 'ConversionLab.png', 'http://www.trackset.it/conversionlab') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Coppermine', 'Coppermine.png', 'http://coppermine-gallery.net') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Cosmoshop', 'Cosmoshop.png', 'http://cosmoshop.de') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Cotonti', 'Cotonti.png', 'http://www.cotonti.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('CouchDB', 'CouchDB.png', 'http://couchdb.apache.org') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Cowboy', 'Cowboy.png', 'http://ninenines.eu') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('CppCMS', 'CppCMS.png', 'http://cppcms.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Craft CMS', 'Craft CMS.png', 'https://craftcms.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Crazy Egg', 'Crazy Egg.png', 'http://crazyegg.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Criteo', 'Criteo.svg', 'http://criteo.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Cross Pixel', 'Cross Pixel.png', 'http://datadesk.crsspxl.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('CubeCart', 'CubeCart.png', 'http://www.cubecart.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Cufon', 'Cufon.png', 'http://cufon.shoqolate.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('D3', 'D3.png', 'http://d3js.org') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('DHTMLX', 'DHTMLX.png', 'http://dhtmlx.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('DM Polopoly', 'DM Polopoly.png', 'http://www.atex.com/products/dm-polopoly') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('DNN', 'DNN.png', 'http://dnnsoftware.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('DTG', 'DTG.png', 'http://www.dtg.nl') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Dancer', 'Dancer.png', 'http://perldancer.org') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Danneo CMS', 'Danneo CMS.png', 'http://danneo.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Darwin', 'Darwin.png', 'http://opensource.apple.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('DataLife Engine', 'DataLife Engine.png', 'http://dle-news.ru') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('DataTables', 'DataTables.png', 'http://datatables.net') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('David Webbox', 'David Webbox.png', 'http://www.tobit.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Debian', 'Debian.png', 'http://debian.org') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Decorum', '', 'http://example.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('DedeCMS', 'DedeCMS.png', 'http://dedecms.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Dell', 'Dell.png', 'http://dell.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Deployd', 'Deployd.png', 'http://deployd.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('DirectAdmin', 'DirectAdmin.png', 'http://www.directadmin.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Discourse', 'Discourse.png', 'http://discourse.org') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Discuz! X', 'Discuz X.png', 'http://discuz.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Disqus', 'Disqus.svg', 'http://disqus.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Django', 'Django.png', 'http://djangoproject.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Django CMS', 'Django CMS.png', 'http://django-cms.org') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Dojo', 'Dojo.png', 'http://dojotoolkit.org') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Dokeos', 'Dokeos.png', 'http://dokeos.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('DokuWiki', 'DokuWiki.png', 'http://www.dokuwiki.org') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Dotclear', 'Dotclear.png', 'http://dotclear.org') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('DoubleClick Ad Exchange (AdX)', 'DoubleClick.svg', 'http://www.doubleclickbygoogle.com/solutions/digital-marketing/ad-exchange/') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('DoubleClick Campaign Manager (DCM)', 'DoubleClick.svg', 'http://www.doubleclickbygoogle.com/solutions/digital-marketing/campaign-manager/') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('DoubleClick Floodlight', 'DoubleClick.svg', 'http://support.google.com/ds/answer/6029713?hl=en') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('DoubleClick for Publishers (DFP)', 'DoubleClick.svg', 'http://www.google.com/dfp') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('DovetailWRP', 'DovetailWRP.png', 'http://www.dovetailinternet.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Doxygen', 'Doxygen.png', 'http://www.stack.nl/~dimitri/doxygen/') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('DreamWeaver', 'DreamWeaver.png', 'http://www.adobe.com/products/dreamweaver') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Drupal', 'Drupal.png', 'http://drupal.org') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Drupal Commerce', 'Drupal Commerce.png', 'http://drupalcommerce.org') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Dynamicweb', 'Dynamicweb.png', 'http://www.dynamicweb.dk') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Dynatrace', 'Dynatrace.png', 'http://dynatrace.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('E-Commerce Paraguay', 'eCommercePy.png', 'http://e-commerceparaguay.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('E-Merchant', 'E-Merchant.png', 'http://e-merchant.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('EC-CUBE', 'ec-cube.png', 'http://www.ec-cube.net') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('ELOG', 'ELOG.png', 'http://midas.psi.ch/elog') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('ELOG HTTP', 'ELOG.png', 'http://midas.psi.ch/elog') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('EPages', 'epages.png', 'http://www.epages.com/') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('EPiServer', 'EPiServer.png', 'http://episerver.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('EPrints', 'EPrints.png', 'http://www.eprints.org') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('ESERV-10', 'ESERV-10.png', 'http://www.violasystems.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('EWS-NIC4', 'EWS-NIC4.png', 'http://dell.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('EdgeCast', 'EdgeCast.png', 'http://www.edgecast.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Elcodi', 'Elcodi.png', 'http://elcodi.io') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Eleanor CMS', 'Eleanor CMS.png', 'http://eleanor-cms.ru') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Eloqua', 'Oracle.png', 'http://eloqua.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('EmbedThis Appweb', 'Embedthis.png', 'http://embedthis.com/appweb') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Embedthis-http', 'Embedthis.png', 'http://github.com/embedthis/http') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Ember.js', 'Ember.js.png', 'http://emberjs.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Enyo', 'Enyo.png', 'http://enyojs.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Epoch', '', 'http://fastly.github.io/epoch') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Epom', 'Epom.png', 'http://epom.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Erlang', 'Erlang.png', 'http://www.erlang.org') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Etherpad', 'etherpad.png', 'https://etherpad.org') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Exagon Concept', 'ExagonConcept.svg', 'http://www.exagon-concept.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Exhibit', 'Exhibit.png', 'http://simile-widgets.org/exhibit/') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Express', 'Express.png', 'http://expressjs.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('ExpressionEngine', 'ExpressionEngine.png', 'http://expressionengine.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('ExtJS', 'ExtJS.png', 'http://www.extjs.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('FAST ESP', 'FAST ESP.png', 'http://microsoft.com/enterprisesearch') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('FAST Search for SharePoint', 'FAST Search for SharePoint.png', 'http://sharepoint.microsoft.com/en-us/product/capabilities/search/Pages/Fast-Search.aspx') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('FWP', 'FWP.png', 'http://fwpshop.org') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Facebook', 'Facebook.svg', 'http://facebook.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Fact Finder', 'Fact Finder.png', 'http://fact-finder.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('FancyBox', 'FancyBox.png', 'http://fancyapps.com/fancybox') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Fastly', 'Fastly.svg', 'https://www.fastly.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Fat-Free Framework', 'Fat-Free Framework.png', 'http://fatfreeframework.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Fedora', 'Fedora.png', 'http://fedoraproject.org') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Firebase', 'Firebase.png', 'http://firebase.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Fireblade', 'Fireblade.png', 'http://fireblade.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('FlashCom', '', 'http://example.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Flask', 'Flask.png', 'http://flask.pocoo.org') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('FlexCMP', 'FlexCMP.png', 'http://www.flexcmp.com/cms/home') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('FlexSlider', 'FlexSlider.png', 'https://woocommerce.com/flexslider/') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('FluxBB', 'FluxBB.png', 'http://fluxbb.org') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Flyspray', 'Flyspray.png', 'http://flyspray.org') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Font Awesome', 'Font Awesome.png', 'http://fontawesome.io') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Fork CMS', 'ForkCMS.png', 'http://www.fork-cms.com/') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Fortune3', 'Fortune3.png', 'http://fortune3.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Foswiki', 'foswiki.png', 'http://foswiki.org') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('FreeBSD', 'FreeBSD.png', 'http://freebsd.org') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Freespee', 'Freespee.svg', 'https://www.freespee.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('FreeTextBox', 'FreeTextBox.png', 'http://freetextbox.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Froala Editor', 'Froala.svg', 'http://froala.com/wysiwyg-editor') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('FrontPage', 'FrontPage.png', 'http://office.microsoft.com/frontpage') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Fusion Ads', 'Fusion Ads.png', 'http://fusionads.net') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('G-WAN', 'G-WAN.png', 'http://gwan.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('GX WebManager', 'GX WebManager.png', 'http://www.gxsoftware.com/en/products/web-content-management.htm') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Gallery', 'Gallery.png', 'http://gallery.menalto.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Gambio', 'Gambio.png', 'http://gambio.de') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Gauges', 'Gauges.png', 'http://get.gaug.es') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Gentoo', 'Gentoo.png', 'http://www.gentoo.org') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Gerrit', 'gerrit.svg', 'http://www.gerritcodereview.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Get Satisfaction', 'Get Satisfaction.png', 'http://getsatisfaction.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('GetSimple CMS', 'GetSimple CMS.png', 'http://get-simple.info') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Ghost', 'Ghost.png', 'http://ghost.org') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('GitBook', 'GitBook.png', 'http://gitbook.io') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('GitLab', 'GitLab.svg', 'http://about.gitlab.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('GitLab CI', 'GitLab CI.png', 'http://about.gitlab.com/gitlab-ci') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('GitPHP', '', 'http://gitphp.org') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Gitiles', '', 'http://gerrit.googlesource.com/gitiles/') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('GlassFish', 'GlassFish.png', 'http://glassfish.java.net') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Glyphicons', 'Glyphicons.png', 'http://glyphicons.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Go', 'Go.svg', 'https://golang.org') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('GoAhead', 'GoAhead.png', 'http://embedthis.com/products/goahead/index.html') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('GoStats', 'GoStats.png', 'http://gostats.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Gogs', 'gogs.png', 'http://gogs.io') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Google AdSense', 'Google AdSense.svg', 'http://google.com/adsense') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Google Analytics', 'Google Analytics.svg', 'http://google.com/analytics') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Google App Engine', 'Google App Engine.png', 'http://code.google.com/appengine') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Google Charts', 'Google Charts.png', 'http://developers.google.com/chart/') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Google Code Prettify', 'Google Code Prettify.png', 'http://code.google.com/p/google-code-prettify') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Google Font API', 'Google Font API.png', 'http://google.com/fonts') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Google Maps', 'Google Maps.png', 'http://maps.google.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Google PageSpeed', 'Google PageSpeed.png', 'http://developers.google.com/speed/pagespeed/mod') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Google Plus', 'Google Plus.svg', 'http://plus.google.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Google Search Appliance', 'Google Search Appliance.png', 'https://enterprise.google.com/search') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Google Sites', 'Google Sites.png', 'http://sites.google.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Google Tag Manager', 'Google Tag Manager.png', 'http://www.google.com/tagmanager') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Google Wallet', 'Google Wallet.png', 'http://wallet.google.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Google Web Server', 'Google Web Server.png', 'http://en.wikipedia.org/wiki/Google_Web_Server') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Google Web Toolkit', 'Google Web Toolkit.png', 'http://developers.google.com/web-toolkit') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Graffiti CMS', 'Graffiti CMS.png', 'http://graffiticms.codeplex.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Grandstream', 'Grandstream.png', 'http://www.grandstream.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Grav', 'Grav.png', 'http://getgrav.org') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Gravatar', 'Gravatar.png', 'http://gravatar.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Gravity Forms', 'gravityforms.svg', 'http://gravityforms.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Gravity Insights', 'Gravity Insights.png', 'http://insights.gravity.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Green Valley CMS', 'Green Valley CMS.png', 'http://www.greenvalley.nl/Public/Producten/Content_Management/CMS') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('HERE', 'HERE.png', 'http://developer.here.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('HHVM', 'HHVM.png', 'http://hhvm.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('HP', 'HP.svg', 'http://hp.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('HP ChaiServer', 'HP.svg', 'http://hp.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('HP Compact Server', 'HP.svg', 'http://hp.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('HP ProCurve', 'HP.svg', 'http://hp.com/networking') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('HP System Management', 'HP.svg', 'http://hp.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('HP iLO', 'HP.svg', 'http://hp.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('HTTP Kit', '', 'http://http-kit.org') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('HTTP-Server', '', 'http://example.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('HTTP/2', 'http2.png', 'http://http2.github.io') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Haddock', '', 'http://www.haskell.org/haddock/') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Hammer.js', 'Hammer.js.png', 'http://hammerjs.github.io') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Handlebars', 'Handlebars.png', 'http://handlebarsjs.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Happy ICS Server', '', 'http://example.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Haravan', 'Haravan.png', 'https://www.haravan.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Haskell', 'Haskell.png', 'http://wiki.haskell.org/Haskell') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('HeadJS', 'HeadJS.png', 'http://headjs.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Heap', 'Heap.png', 'http://heapanalytics.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Hello Bar', 'Hello Bar.png', 'http://hellobar.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Hiawatha', 'Hiawatha.png', 'http://hiawatha-webserver.org') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Highcharts', 'Highcharts.png', 'http://highcharts.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Highlight.js', 'Highlight.js.png', 'https://highlightjs.org/') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Highstock', 'Highcharts.png', 'http://highcharts.com/products/highstock') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Hippo', 'Hippo.png', 'http://onehippo.org') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Hogan.js', 'Hogan.js.png', 'http://twitter.github.com/hogan.js') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Homeland', 'Homeland.png', 'https://gethomeland.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Hotaru CMS', 'Hotaru CMS.png', 'http://hotarucms.org') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Hotjar', 'Hotjar.png', 'https://www.hotjar.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('HubSpot', 'HubSpot.png', 'http://hubspot.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Hugo', 'Hugo.png', 'http://gohugo.io') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Hybris', 'Hybris.png', 'http://hybris.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('IBM Coremetrics', 'IBM.svg', 'http://ibm.com/software/marketing-solutions/coremetrics') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('IBM HTTP Server', 'IBM.svg', 'http://ibm.com/software/webservers/httpservers') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('IBM Tivoli Storage Manager', 'IBM.svg', 'http://ibm.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('IBM WebSphere Commerce', 'IBM.svg', 'http://ibm.com/software/genservers/commerceproductline') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('IBM WebSphere Portal', 'IBM.svg', 'http://ibm.com/software/websphere/portal') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('IIS', 'IIS.png', 'http://www.iis.net') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('INFOnline', 'INFOnline.png', 'http://infonline.de') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('IPB', 'IPB.png', 'http://www.invisionpower.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Immutable.js', 'Immutable.js.png', 'http://facebook.github.io/immutable-js/') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('imperia CMS', 'imperiaCMS.svg', 'https://www.pirobase-imperia.com/de/produkte/produktuebersicht/imperia-cms') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('ImpressCMS', 'ImpressCMS.png', 'http://www.impresscms.org') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('ImpressPages', 'ImpressPages.png', 'http://impresspages.org') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('InProces', 'InProces.png', 'http://www.brein.nl/oplossing/product/website') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Incapsula', 'Incapsula.png', 'http://www.incapsula.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Indexhibit', '', 'http://www.indexhibit.org') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Indico', 'Indico.png', 'http://indico-software.org') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Indy', '', 'http://indyproject.org') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('InfernoJS', 'InfernoJS.png', 'https://infernojs.org/') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Infusionsoft', 'infusionsoft.svg', 'http://infusionsoft.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('InstantCMS', 'InstantCMS.png', 'http://www.instantcms.ru') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Intel Active Management Technology', 'Intel Active Management Technology.png', 'http://intel.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('IntenseDebate', 'IntenseDebate.png', 'http://intensedebate.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Intercom', 'Intercom.png', 'http://intercom.io') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Intershop', 'Intershop.png', 'http://intershop.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Invenio', 'Invenio.png', 'http://invenio-software.org') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Ionicons', 'Ionicons.png', 'http://ionicons.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('JAlbum', 'JAlbum.png', 'http://jalbum.net/en') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('JBoss Application Server', 'JBoss Application Server.png', 'http://jboss.org/jbossas.html') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('JBoss Web', 'JBoss Web.png', 'http://jboss.org/jbossweb') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('JC-HTTPD', 'JC-HTTPD.png', 'http://canon.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('JS Charts', 'JS Charts.png', 'http://www.jscharts.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('JTL Shop', 'JTL Shop.png', 'http://www.jtl-software.de/produkte/jtl-shop3') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Jalios', 'Jalios.png', 'http://www.jalios.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Java', 'Java.png', 'http://java.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Java Servlet', 'Java.png', 'http://www.oracle.com/technetwork/java/index-jsp-135475.html') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('JavaScript Infovis Toolkit', 'JavaScript Infovis Toolkit.png', 'http://thejit.org') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('JavaServer Faces', 'JavaServer Faces.png', 'http://javaserverfaces.java.net') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('JavaServer Pages', 'Java.png', 'http://www.oracle.com/technetwork/java/javaee/jsp/index.html') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Jekyll', 'Jekyll.png', 'http://jekyllrb.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Jenkins', 'Jenkins.png', 'http://jenkins-ci.org') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Jetty', 'Jetty.png', 'http://www.eclipse.org/jetty') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Jimdo', 'jimdo.png', 'http://www.jimdo.com/') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Jirafe', 'Jirafe.png', 'http://jirafe.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Jive', 'Jive.png', 'http://www.jivesoftware.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Jo', 'Jo.png', 'http://joapp.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('JobberBase', 'JobberBase.png', 'http://jobberbase.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Joomla', 'Joomla.png', 'http://joomla.org') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('K2', 'K2.png', 'http://getk2.org') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('KISSmetrics', 'KISSmetrics.png', 'http://www.kissmetrics.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('KS_HTTP', 'KS_HTTP.png', 'http://www.canon.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Kampyle', 'Kampyle.png', 'http://www.kampyle.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Kamva', 'Kamva.svg', 'http://kamva.ir') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Kendo UI', 'Kendo UI.png', 'http://www.kendoui.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Kentico CMS', 'Kentico CMS.png', 'http://www.kentico.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('KeyCDN', 'KeyCDN.png', 'http://www.keycdn.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Kibana', 'kibana.svg', 'http://www.elastic.co/products/kibana') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('KineticJS', 'KineticJS.png', 'http://kineticjs.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Klarna Checkout', 'Klarna.svg', 'http://klarna.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Knockout.js', 'Knockout.js.png', 'http://knockoutjs.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Koa', 'Koa.png', 'http://koajs.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Koala Framework', 'Koala Framework.png', 'http://koala-framework.org') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Koego', 'Koego.png', 'http://www.koego.com/en') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Kohana', 'Kohana.png', 'http://kohanaframework.org') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Koken', 'Koken.png', 'http://koken.me') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Kolibri CMS', '', 'http://alias.io') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Komodo CMS', 'Komodo CMS.png', 'http://www.komodocms.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Kontaktify', 'Kontaktify.png', 'https://www.kontaktify.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Koobi', 'Koobi.png', 'http://dream4.de/cms') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Kooboo CMS', 'Kooboo CMS.png', 'http://kooboo.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Kotisivukone', 'Kotisivukone.png', 'http://www.kotisivukone.fi') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('LEPTON', 'LEPTON.png', 'http://www.lepton-cms.org') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('LabVIEW', 'LabVIEW.png', 'http://ni.com/labview') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Laravel', 'Laravel.png', 'http://laravel.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Lazy.js', '', 'http://danieltao.com/lazy.js') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Leaflet', 'Leaflet.png', 'http://leafletjs.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Less', 'Less.png', 'http://lesscss.org') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Liferay', 'Liferay.png', 'http://www.liferay.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Lift', 'Lift.png', 'http://liftweb.net') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('LightMon Engine', 'LightMon Engine.png', 'http://lightmon.ru') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Lightbox', 'Lightbox.png', 'http://lokeshdhakar.com/projects/lightbox2/') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Lightspeed eCom', 'Lightspeed.svg', 'http://www.lightspeedhq.com/products/ecommerce/') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Lighty', 'Lighty.png', 'http://gitlab.com/lighty/framework') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('LimeSurvey', 'LimeSurvey.png', 'http://limesurvey.org/') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('LinkSmart', 'LinkSmart.png', 'http://linksmart.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Linkedin', 'Linkedin.svg', 'http://linkedin.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('List.js', 'List.js.png', 'http://www.listjs.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('LiteSpeed', 'LiteSpeed.png', 'http://litespeedtech.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Lithium', 'Lithium.png', 'http://www.lithium.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('LiveAgent', 'LiveAgent.png', 'http://www.ladesk.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('LiveChat', 'LiveChat.png', 'http://livechatinc.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('LiveJournal', 'LiveJournal.png', 'http://www.livejournal.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('LivePerson', 'LivePerson.png', 'https://www.liveperson.com/') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('LiveStreet CMS', 'LiveStreet CMS.png', 'http://livestreetcms.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Livefyre', 'Livefyre.png', 'http://livefyre.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Liveinternet', 'Liveinternet.png', 'http://liveinternet.ru/rating/') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Lo-dash', 'Lo-dash.png', 'http://www.lodash.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Locomotive', 'Locomotive.png', 'http://www.locomotivecms.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Logitech Media Server', 'Logitech Media Server.png', 'http://www.mysqueezebox.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Lotus Domino', 'Lotus Domino.png', 'http://www-01.ibm.com/software/lotus/products/domino') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Lua', 'Lua.png', 'http://www.lua.org') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Lucene', 'Lucene.png', 'http://lucene.apache.org/core/') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Luigi’s Box', 'Luigisbox.svg', 'https://www.luigisbox.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('M.R. Inc BoxyOS', 'M.R. Inc.png', 'http://mrincworld.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('M.R. Inc SiteFrame', 'M.R. Inc.png', 'http://mrincworld.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('M.R. Inc Webserver', 'M.R. Inc.png', 'http://mrincworld.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('MHonArc', 'mhonarc.png', 'http://www.mhonarc.at') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('MOBOTIX', 'MOBOTIX.png', 'http://mobotix.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('MODX', 'MODX.png', 'http://modx.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('MadAdsMedia', 'MadAdsMedia.png', 'http://madadsmedia.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Magento', 'Magento.png', 'http://www.magentocommerce.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Mailchimp', 'mailchimp.svg', 'http://mailchimp.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Mambo', 'Mambo.png', 'http://mambo-foundation.org') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('MantisBT', 'MantisBT.png', 'http://www.mantisbt.org') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('ManyContacts', 'ManyContacts.png', 'http://www.manycontacts.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Marionette.js', 'Marionette.js.svg', 'http://marionettejs.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Marketo', 'Marketo.png', 'http://www.marketo.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Materialize CSS', 'Materialize CSS.png', 'http://materializecss.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('MathJax', 'MathJax.png', 'http://mathjax.org') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Mattermost', 'mattermost.png', 'http://about.mattermost.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('MaxCDN', 'MaxCDN.png', 'http://www.maxcdn.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('MaxSite CMS', 'MaxSite CMS.png', 'http://max-3000.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Mean.io', 'Mean.io.png', 'http://mean.io') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('MediaElement.js', 'MediaElement.js.png', 'http://mediaelementjs.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('MediaTomb', 'MediaTomb.png', 'http://mediatomb.cc') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('MediaWiki', 'MediaWiki.png', 'http://www.mediawiki.org') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Medium', 'Medium.svg', 'https://medium.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Meebo', 'Meebo.png', 'http://www.meebo.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Melis CMS V2', 'meliscmsv2.png', 'http://www.melistechnology.com/') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Meteor', 'Meteor.png', 'http://meteor.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Methode', 'Methode.png', 'http://www.eidosmedia.com/solutions') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Microsoft ASP.NET', 'Microsoft ASP.NET.png', 'http://www.asp.net') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Microsoft HTTPAPI', 'Microsoft.svg', 'http://microsoft.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Microsoft SharePoint', 'Microsoft SharePoint.png', 'http://sharepoint.microsoft.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Mietshop', 'mietshop.png', 'http://www.mietshop.de/') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Milligram', 'Milligram.png', 'http://milligram.github.io') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('MiniBB', 'MiniBB.png', 'http://www.minibb.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('MiniServ', '', 'http://sourceforge.net/projects/miniserv') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Mint', 'Mint.png', 'http://haveamint.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Mixpanel', 'Mixpanel.png', 'http://mixpanel.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Mobify', 'Mobify.png', 'http://www.mobify.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('MochiKit', 'MochiKit.png', 'http://mochikit.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('MochiWeb', '', 'http://github.com/mochi/mochiweb') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Modernizr', 'Modernizr.png', 'http://www.modernizr.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Modified', 'modified.png', 'http://www.modified-shop.org/') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Moguta.CMS', 'Moguta.CMS.png', 'http://moguta.ru') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('MoinMoin', 'MoinMoin.png', 'http://moinmo.in') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Mojolicious', 'Mojolicious.png', 'http://mojolicio.us') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Mollom', 'Mollom.png', 'http://mollom.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Moment Timezone', 'Moment.js.png', 'http://momentjs.com/timezone/') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Moment.js', 'Moment.js.png', 'http://momentjs.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Mondo Media', 'Mondo Media.png', 'http://mondo-media.de') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('MongoDB', 'MongoDB.png', 'http://www.mongodb.org') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Mongrel', 'Mongrel.png', 'http://mongrel2.org') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Monkey HTTP Server', 'Monkey HTTP Server.png', 'http://monkey-project.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Mono', 'Mono.png', 'http://mono-project.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Mono.net', 'Mono.net.png', 'http://www.mono.net') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('MooTools', 'MooTools.png', 'http://mootools.net') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Moodle', 'Moodle.png', 'http://moodle.org') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Moon', 'moon.png', 'http://moonjs.ga/') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Motion-httpd', '', 'http://lavrsen.dk/foswiki/bin/view/Motion') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('MotoCMS', 'MotoCMS.svg', 'http://motocms.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Movable Type', 'Movable Type.png', 'http://movabletype.org') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Moxa', 'Moxa.png', 'http://moxa.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Mozard Suite', 'Mozard Suite.png', 'http://mozard.nl') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Mura CMS', 'Mura CMS.png', 'http://www.getmura.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Mustache', 'Mustache.png', 'http://mustache.github.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('MyBB', 'MyBB.png', 'http://www.mybb.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('MyBlogLog', 'MyBlogLog.png', 'http://www.mybloglog.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('MySQL', 'MySQL.svg', 'http://mysql.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Mynetcap', 'Mynetcap.png', 'http://www.netcap-creation.fr') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('NOIX', 'NOIX.png', 'http://www.noix.com.br/tecnologias/joomla') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('NVD3', 'NVD3.png', 'http://nvd3.org') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Navegg', 'Navegg.png', 'https://www.navegg.com/') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Nedstat', 'Nedstat.png', 'http://www.nedstat.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Neos CMS', 'Neos.svg', 'http://neos.io') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Neos Flow', 'Neos.svg', 'http://flow.neos.io') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Nepso', 'Nepso.png', 'http://nepso.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Netlify', 'Netlify.svg', 'https://www.netlify.com/') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Netmonitor', 'Netmonitor.png', 'http://netmonitor.fi/en') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Neto', 'Neto.svg', 'http://www.neto.com.au') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Netsuite', 'Netsuite.png', 'http://netsuite.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Nette Framework', 'Nette Framework.png', 'http://nette.org') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('New Relic', 'New Relic.png', 'http://newrelic.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Nginx', 'Nginx.svg', 'http://nginx.org/en') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Node.js', 'node.js.png', 'http://nodejs.org') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('NodeBB', 'NodeBB.png', 'https://nodebb.org') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('OWL Carousel', 'OWL Carousel.png', 'https://owlcarousel2.github.io/OwlCarousel2/') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('OXID eShop', 'OXID eShop.png', 'http://oxid-esales.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('October CMS', 'October CMS.png', 'http://octobercms.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Octopress', 'octopress.png', 'http://octopress.org') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Odoo', 'Odoo.png', 'http://odoo.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Olark', 'Olark.png', 'https://www.olark.com/') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('OmniTouch 8660 My Teamwork', 'OmniTouch 8660 My Teamwork.png', 'http://enterprise.alcatel-lucent.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('OneAPM', 'OneAPM.png', 'http://www.oneapm.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('OneStat', 'OneStat.png', 'http://www.onestat.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Open AdStream', 'Open AdStream.png', 'http://xaxis.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Open Classifieds', 'Open Classifieds.png', 'http://open-classifieds.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Open Journal Systems', 'Open Journal Systems.png', 'http://pkp.sfu.ca/ojs') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Open Web Analytics', 'Open Web Analytics.png', 'http://openwebanalytics.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Open eShop', 'Open eShop.png', 'http://open-eshop.com/') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('OpenCart', 'OpenCart.png', 'http://www.opencart.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('OpenCms', 'OpenCms.png', 'http://www.opencms.org') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('OpenGSE', 'OpenGSE.png', 'http://code.google.com/p/opengse') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('OpenGrok', 'OpenGrok.png', 'http://hub.opensolaris.org/bin/view/Project+opengrok/WebHome') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('OpenLayers', 'OpenLayers.png', 'http://openlayers.org') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('OpenNemas', 'OpenNemas.png', 'http://www.opennemas.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('OpenResty', 'OpenResty.png', 'http://openresty.org') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('OpenSSL', 'OpenSSL.png', 'http://openssl.org') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('OpenText Web Solutions', 'OpenText Web Solutions.png', 'http://websolutions.opentext.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('OpenX', 'OpenX.png', 'http://openx.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Ophal', 'Ophal.png', 'http://ophal.org') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Optimizely', 'Optimizely.png', 'http://optimizely.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Oracle Application Server', 'Oracle.png', 'http://www.oracle.com/technetwork/middleware/ias/overview/index.html') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Oracle Commerce', 'Oracle.png', 'http://www.oracle.com/applications/customer-experience/commerce/products/commerce-platform/index.html') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Oracle Commerce Cloud', 'Oracle.png', 'http://cloud.oracle.com/commerce-cloud') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Oracle Dynamic Monitoring Service', 'Oracle.png', 'http://oracle.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Oracle HTTP Server', 'Oracle.png', 'http://oracle.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Oracle Recommendations On Demand', 'Oracle.png', 'http://www.oracle.com/us/products/applications/commerce/recommendations-on-demand/index.html') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Oracle Web Cache', 'Oracle.png', 'http://oracle.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Orchard CMS', 'Orchard CMS.png', 'http://orchardproject.net') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Outbrain', 'Outbrain.png', 'http://outbrain.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Outlook Web App', 'Outlook Web App.png', 'http://help.outlook.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('PANSITE', 'PANSITE.png', 'http://panvision.de/Produkte/Content_Management/index.asp') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('PDF.js', 'PDF.js.svg', 'http://mozilla.github.io/pdf.js/') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('PHP', 'PHP.svg', 'http://php.net') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('PHP-Fusion', 'PHP-Fusion.png', 'http://www.php-fusion.co.uk') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('PHP-Nuke', 'PHP-Nuke.png', 'http://phpnuke.org') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Pagekit', 'Pagekit.png', 'http://pagekit.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Pardot', 'Pardot.png', 'http://pardot.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Parse.ly', 'Parse.ly.png', 'http://parse.ly') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Paths.js', '', 'http://github.com/andreaferretti/paths-js') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('PayPal', 'PayPal.png', 'http://paypal.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('PencilBlue', 'PencilBlue.png', 'http://pencilblue.org') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Penguin', 'Penguin.svg', 'http://penguin.docs.bqws.io') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Percussion', 'Percussion.png', 'http://percussion.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('PerfSONAR-PS', 'PerfSONAR-PS.png', 'http://psps.perfsonar.net') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Perl', 'Perl.png', 'http://perl.org') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Petrojs', 'Petrojs.png', 'http://petrojs.thepetronics.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Phabricator', 'Phabricator.png', 'http://phacility.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Phaser', 'Phaser.png', 'http://phaser.io') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Phusion Passenger', 'Phusion Passenger.png', 'http://phusionpassenger.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Piano Solo', 'Piano Solo.png', 'http://www.pianomedia.com/products') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Pimcore', 'pimcore.svg', 'http://pimcore.org') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Pinterest', 'Pinterest.svg', 'http://pinterest.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('pirobase CMS', 'pirobaseCMS.svg', 'https://www.pirobase-imperia.com/de/produkte/produktuebersicht/pirobase-cms') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Piwik', 'Piwik.png', 'http://piwik.org') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Planet', 'Planet.png', 'http://planetplanet.org') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Play', 'Play.svg', 'https://www.playframework.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Plentymarkets', 'Plentymarkets.png', 'http://plentymarkets.eu') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Plesk', 'Plesk.png', 'http://plesk.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Pligg', 'Pligg.png', 'http://pligg.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Plone', 'Plone.png', 'http://plone.org') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Plotly', 'Plotly.png', 'http://plot.ly/javascript/') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Plura', 'Plura.png', 'http://www.pluraprocessing.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Po.st', 'Po.st.png', 'http://www.po.st/') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Polymer', 'Polymer.png', 'http://polymer-project.org') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Posterous', 'Posterous.png', 'http://posterous.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('PostgreSQL', 'PostgreSQL.png', 'http://www.postgresql.org/') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Powergap', 'Powergap.png', 'http://powergap.de') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Prefix-Free', 'Prefix-Free.png', 'http://leaverou.github.io/prefixfree/') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('PrestaShop', 'PrestaShop.png', 'http://www.prestashop.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Project Wonderful', 'Project Wonderful.png', 'http://projectwonderful.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Prospector', 'Prospector.png', 'http://prospector.io') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Prototype', 'Prototype.png', 'http://www.prototypejs.org') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Protovis', '', 'http://mbostock.github.com/protovis') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Proximis Omnichannel', 'Proximis Omnichannel.png', 'https://www.proximis.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Proximis Web to Store', 'Proximis Omnichannel.png', 'https://www.proximis.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('PubMatic', 'PubMatic.png', 'http://www.pubmatic.com/') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Public CMS', 'Public CMS.png', 'http://www.publiccms.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Pure CSS', 'Pure CSS.png', 'http://purecss.io') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Pygments', 'pygments.png', 'http://pygments.org') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('PyroCMS', 'PyroCMS.png', 'http://pyrocms.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Python', 'Python.png', 'http://python.org') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Quantcast', 'Quantcast.png', 'http://www.quantcast.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Question2Answer', 'question2answer.png', 'http://www.question2answer.org') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Quick.CMS', 'Quick.CMS.png', 'http://opensolution.org') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Quick.Cart', 'Quick.Cart.png', 'http://opensolution.org') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Quill', 'Quill.png', 'http://quilljs.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('RAID HTTPServer', '', 'http://example.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('RBS Change', 'RBS Change.png', 'http://www.rbschange.fr') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('RCMS', 'RCMS.png', 'http://www.rcms.fi') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('RD Station', 'RD Station.png', 'http://rdstation.com.br') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('RDoc', 'RDoc.png', 'http://github.com/RDoc/RDoc') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('RackCache', 'RackCache.png', 'http://github.com/rtomayko/rack-cache') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('RainLoop', 'RainLoop.png', 'http://rainloop.net') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Rakuten DBCore', 'Rakuten DBCore.png', 'http://ecservice.rakuten.com.br') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Ramda', 'Ramda.png', 'http://ramdajs.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Raphael', 'Raphael.png', 'http://raphaeljs.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Rapid Logic', '', 'http://example.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Raspbian', 'Raspbian.svg', 'https://www.raspbian.org/') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Rdf', '', 'https://www.w3.org/RDF/') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('React', 'React.png', 'http://facebook.github.io/react') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Red Hat', 'Red Hat.png', 'http://redhat.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Reddit', 'Reddit.png', 'http://code.reddit.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Redmine', 'Redmine.png', 'http://www.redmine.org') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Reinvigorate', 'Reinvigorate.png', 'http://www.reinvigorate.net') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('RequireJS', 'RequireJS.png', 'http://requirejs.org') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Resin', 'Resin.png', 'http://caucho.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Reveal.js', 'Reveal.js.png', 'http://lab.hakim.se/reveal-js') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Revel', 'Revel.png', 'http://revel.github.io') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Rickshaw', '', 'http://code.shutterstock.com/rickshaw/') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('RightJS', 'RightJS.png', 'http://rightjs.org') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Riot', 'Riot.png', 'http://muut.com/riotjs') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('RiteCMS', 'RiteCMS.png', 'http://ritecms.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Roadiz CMS', 'Roadiz CMS.png', 'http://www.roadiz.io') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Robin', 'Robin.png', 'http://www.robinhq.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('RockRMS', 'RockRMS.svg', 'http://www.rockrms.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('RoundCube', 'RoundCube.png', 'http://roundcube.net') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Rubicon Project', 'Rubicon Project.png', 'http://rubiconproject.com/') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Ruby', 'Ruby.png', 'http://ruby-lang.org') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Ruby on Rails', 'Ruby on Rails.png', 'http://rubyonrails.org') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Ruxit', 'Ruxit.png', 'http://ruxit.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('RxJS', 'RxJS.png', 'http://reactivex.io') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('S.Builder', 'S.Builder.png', 'http://www.sbuilder.ru') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('SAP', 'SAP.png', 'http://sap.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('SDL Tridion', 'SDL Tridion.png', 'http://www.sdl.com/products/tridion') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('SIMsite', 'SIMsite.png', 'http://simgroep.nl/internet/portfolio-contentbeheer_41623/') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('SMF', 'SMF.png', 'http://www.simplemachines.org') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('SOBI 2', 'SOBI 2.png', 'http://www.sigsiu.net/sobi2.html') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('SPDY', 'SPDY.png', 'http://chromium.org/spdy') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('SPIP', 'SPIP.png', 'http://www.spip.net') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('SQL Buddy', 'SQL Buddy.png', 'http://www.sqlbuddy.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('SQLite', 'SQLite.png', 'http://www.sqlite.org') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('SUSE', 'SUSE.png', 'http://suse.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('SWFObject', 'SWFObject.png', 'http://github.com/swfobject/swfobject') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Saia PCD', 'Saia PCD.png', 'http://saia-pcd.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Sails.js', 'Sails.js.svg', 'http://sailsjs.org') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Salesforce', 'Salesforce.svg', 'https://www.salesforce.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Salesforce Commerce Cloud', 'Salesforce.svg', 'http://demandware.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Sarka-SPIP', 'Sarka-SPIP.png', 'http://sarka-spip.net') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Sazito', 'Sazito.svg', 'http://sazito.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Scala', 'Scala.png', 'http://www.scala-lang.org') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Schneider', 'Schneider.png', 'http://schneider-electric.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Schneider Web Server', 'Schneider.png', 'http://schneider-electric.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Scholica', 'Scholica.svg', 'http://scholica.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Scientific Linux', 'Scientific Linux.png', 'http://scientificlinux.org') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Segment', 'Segment.png', 'http://segment.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Select2', 'Select2.png', 'http://select2.github.io') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Semantic-ui', 'Semantic-ui.png', 'http://semantic-ui.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Sencha Touch', 'Sencha Touch.png', 'http://sencha.com/products/touch') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Sentinel Keys Server', 'Sentinel.png', 'http://www.safenet-inc.com/software-monetization/sentinel-rms') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Sentinel License Monitor', 'Sentinel.png', 'http://www.safenet-inc.com/software-monetization/sentinel-rms/') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Sentinel Protection Server', 'Sentinel.png', 'http://www.safenet-inc.com/software-monetization/sentinel-rms/') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Serendipity', 'Serendipity.png', 'http://s9y.org') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Shadow', 'Shadow.png', 'http://shadow-technologies.co.uk') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Shapecss', 'Shapecss.svg', 'https://shapecss.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('ShareThis', 'ShareThis.png', 'http://sharethis.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('ShellInABox', 'ShellInABox.png', 'http://shellinabox.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('ShinyStat', 'ShinyStat.png', 'http://shinystat.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Shopalize', 'Shopalize.png', 'http://shopalize.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Shopatron', 'Shopatron.png', 'http://ecommerce.shopatron.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Shopery', 'Shopery.svg', 'http://shopery.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Shopify', 'Shopify.svg', 'http://shopify.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Shoptet', 'Shoptet.svg', 'http://www.shoptet.cz') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Shopware', 'Shopware.png', 'http://shopware.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Silva', 'Silva.png', 'http://silvacms.org') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('SilverStripe', 'SilverStripe.svg', 'http://www.silverstripe.org') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('SimpleHTTP', '', 'http://example.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Site Meter', 'Site Meter.png', 'http://www.sitemeter.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('SiteCatalyst', 'SiteCatalyst.png', 'http://www.adobe.com/solutions/digital-marketing.html') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('SiteEdit', 'SiteEdit.png', 'http://www.siteedit.ru') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Sitecore', 'Sitecore.png', 'http://sitecore.net') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Sitefinity', 'Sitefinity.svg', 'http://www.sitefinity.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Sivuviidakko', 'Sivuviidakko.png', 'http://sivuviidakko.fi') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Sizmek', 'Sizmek.png', 'http://sizmek.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Slimbox', 'Slimbox.png', 'http://www.digitalia.be/software/slimbox') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Slimbox 2', 'Slimbox 2.png', 'http://www.digitalia.be/software/slimbox2') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Smart Ad Server', 'Smart Ad Server.png', 'http://smartadserver.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('SmartSite', 'SmartSite.png', 'http://www.seneca.nl/pub/Smartsite/Smartsite-Smartsite-iXperion') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Smartstore', 'Smartstore.png', 'http://smartstore.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Snap', 'Snap.png', 'http://snapframework.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Snap.svg', 'Snap.svg.png', 'http://snapsvg.io') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Snoobi', 'Snoobi.png', 'http://www.snoobi.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('SobiPro', 'SobiPro.png', 'http://sigsiu.net/sobipro.html') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Socket.io', 'Socket.io.png', 'http://socket.io') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Solodev', 'Solodev.png', 'http://www.solodev.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Solr', 'Solr.png', 'http://lucene.apache.org/solr/') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Solusquare OmniCommerce Cloud', 'Solusquare.png', 'https://www.solusquare.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Solve Media', 'Solve Media.png', 'http://solvemedia.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('SonarQubes', 'sonar.png', 'https://www.sonarqube.org/') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('SoundManager', 'SoundManager.png', 'http://www.schillmania.com/projects/soundmanager2') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Sparql', '', 'https://www.w3.org/TR/sparql11-overview/') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Sphinx', 'Sphinx.png', 'http://sphinx.pocoo.org') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('SpiderControl iniNet', 'SpiderControl iniNet.png', 'http://spidercontrol.net/ininet') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('SpinCMS', 'SpinCMS.png', 'http://www.spin.cw') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Splunk', 'Splunk.png', 'http://splunk.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Splunkd', 'Splunk.png', 'http://splunk.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Spree', 'Spree.png', 'http://spreecommerce.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Squarespace', 'Squarespace.png', 'http://www.squarespace.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('SquirrelMail', 'SquirrelMail.png', 'http://squirrelmail.org') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Squiz Matrix', 'Squiz Matrix.png', 'http://squiz.net') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Stackla', 'Stackla.png', 'http://stackla.com/') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Stackla Social Hub', 'Stackla.png', 'http://stackla.com/') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Stamplay', 'Stamplay.png', 'http://stamplay.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Starlet', 'Starlet.png', 'http://metacpan.org/pod/Starlet') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('StatCounter', 'StatCounter.png', 'http://www.statcounter.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Store Systems', 'Store Systems.png', 'http://store-systems.de') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Storyblok', 'storyblok.png', 'https://www.storyblok.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Strapdown.js', 'strapdown.js.png', 'http://strapdownjs.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Strato', 'strato.png', 'http://shop.strato.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Stripe', 'Stripe.png', 'http://stripe.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('SublimeVideo', 'SublimeVideo.png', 'http://sublimevideo.net') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Subrion', 'Subrion.png', 'http://subrion.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Sulu', 'Sulu.svg', 'http://sulu.io') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('SumoMe', 'SumoMe.png', 'http://sumome.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('SunOS', 'Oracle.png', 'http://oracle.com/solaris') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Supersized', 'Supersized.png', 'http://buildinternet.com/project/supersized') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('SweetAlert', 'SweetAlert.png', 'http://t4t5.github.io/sweetalert/') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('SweetAlert2', 'SweetAlert2.png', 'https://limonte.github.io/sweetalert2') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Swiftlet', 'Swiftlet.png', 'http://swiftlet.org') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Swiftype', 'swiftype.png', 'http://swiftype.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Symfony', 'Symfony.png', 'http://symfony.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Synology DiskStation', 'Synology DiskStation.png', 'http://synology.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('SyntaxHighlighter', 'SyntaxHighlighter.png', 'http://github.com/syntaxhighlighter') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('TWiki', 'TWiki.png', 'http://twiki.org') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('TYPO3 CMS', 'TYPO3.svg', 'http://www.typo3.org') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Taiga', 'Taiga.png', 'http://taiga.io') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Tawk.to', 'TawkTo.png', 'http://tawk.to') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Tealeaf', 'Tealeaf.png', 'http://www.tealeaf.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Tealium', 'Tealium.png', 'http://tealium.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('TeamCity', 'TeamCity.png', 'http://jetbrains.com/teamcity') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Telescope', 'Telescope.png', 'http://telescopeapp.org') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Tengine', 'Tengine.png', 'http://tengine.taobao.org') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Textpattern CMS', 'Textpattern CMS.png', 'http://textpattern.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Thelia', 'Thelia.png', 'http://thelia.net') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('ThinkPHP', 'ThinkPHP.png', 'http://www.thinkphp.cn') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('TiddlyWiki', 'TiddlyWiki.png', 'http://tiddlywiki.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Tiki Wiki CMS Groupware', 'Tiki Wiki CMS Groupware.png', 'http://tiki.org') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Timeplot', 'Timeplot.png', 'http://www.simile-widgets.org/timeplot/') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('TinyMCE', 'TinyMCE.png', 'http://tinymce.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Titan', 'Titan.png', 'http://titan360.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('TomatoCart', 'TomatoCart.png', 'http://tomatocart.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('TornadoServer', 'TornadoServer.png', 'http://tornadoweb.org') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('TotalCode', 'TotalCode.png', 'http://www.totalcode.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Trac', 'Trac.png', 'http://trac.edgewall.org') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('TrackJs', 'TrackJs.png', 'http://trackjs.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Translucide', 'translucide.svg', 'http://www.translucide.net') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Tumblr', 'Tumblr.png', 'http://www.tumblr.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('TweenMax', 'TweenMax.png', 'http://greensock.com/tweenmax') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Twilight CMS', 'Twilight CMS.png', 'http://www.twilightcms.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('TwistPHP', 'TwistPHP.png', 'http://twistphp.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('TwistedWeb', 'TwistedWeb.png', 'http://twistedmatrix.com/trac/wiki/TwistedWeb') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Twitter', 'Twitter.svg', 'http://twitter.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Twitter Bootstrap', 'Twitter Bootstrap.png', 'http://getbootstrap.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Twitter Emoji (Twemoji)', '', 'http://twitter.github.io/twemoji/') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Twitter Flight', 'Twitter Flight.png', 'http://flightjs.github.io/') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Twitter typeahead.js', 'Twitter typeahead.js.png', 'http://twitter.github.io/typeahead.js') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('TypePad', 'TypePad.png', 'http://www.typepad.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Typecho', 'typecho.svg', 'http://typecho.org/') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Typekit', 'Typekit.png', 'http://typekit.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('uCoz', 'uCoz.svg', 'https://ucoz.ru') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('UIKit', 'UIKit.png', 'https://getuikit.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('UNIX', 'UNIX.png', 'http://unix.org') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Ubercart', 'Ubercart.png', 'http://www.ubercart.org') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Ubuntu', 'Ubuntu.png', 'http://www.ubuntu.com/server') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('UltraCart', 'UltraCart.png', 'http://ultracart.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Umbraco', 'Umbraco.png', 'http://umbraco.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('UMI.CMS', 'UMI.CMS.png', 'https://www.umi-cms.ru') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Unbounce', 'Unbounce.png', 'http://unbounce.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Underscore.js', 'Underscore.js.png', 'http://underscorejs.org') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Usabilla', 'Usabilla.svg', 'http://usabilla.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('UserEngage', 'UserEngage.png', 'https://userengage.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('UserLike', 'UserLike.svg', 'http://userlike.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('UserRules', 'UserRules.png', 'http://www.userrules.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('UserVoice', 'UserVoice.png', 'http://uservoice.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Ushahidi', 'Ushahidi.png', 'http://www.ushahidi.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('VIVVO', 'VIVVO.png', 'http://vivvo.net') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('VP-ASP', 'VP-ASP.png', 'http://www.vpasp.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('VTEX Enterprise', 'VTEX Enterprise.png', 'http://vtex.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('VTEX Integrated Store', 'VTEX Integrated Store.png', 'http://lojaintegrada.com.br') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Vanilla', 'Vanilla.png', 'http://vanillaforums.org') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Varnish', 'Varnish.svg', 'http://www.varnish-cache.org') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Venda', 'Venda.png', 'http://venda.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Veoxa', 'Veoxa.png', 'http://veoxa.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('VideoJS', 'VideoJS.png', 'http://videojs.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('VigLink', 'VigLink.png', 'http://viglink.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Vignette', 'Vignette.png', 'http://www.vignette.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Vimeo', 'Vimeo.png', 'http://vimeo.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Vinala', 'Vinala.png', 'https://github.com/vinala/vinala') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Virata EmWeb', '', 'http://example.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('VirtueMart', 'VirtueMart.png', 'http://virtuemart.net') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Virtuoso', '', 'https://virtuoso.openlinksw.com/') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Visual WebGUI', 'Visual WebGUI.png', 'http://www.gizmox.com/products/visual-web-gui/') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('VisualPath', 'VisualPath.png', 'http://www.trackset.com/web-analytics-software/visualpath') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Volusion', 'Volusion.png', 'http://volusion.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Vox', 'Vox.png', 'http://www.vox.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Vue.js', 'Vue.js.png', 'http://vuejs.org') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('W3 Total Cache', 'W3 Total Cache.png', 'http://www.w3-edge.com/wordpress-plugins/w3-total-cache') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('W3Counter', 'W3Counter.png', 'http://www.w3counter.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('WEBXPAY', 'WEBXPAY.png', 'https://webxpay.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('WHMCS', 'WHMCS.png', 'http://www.whmcs.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('WP Rocket', 'WP Rocket.png', 'http://wp-rocket.me') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Warp', 'Warp.png', 'http://www.stackage.org/package/warp') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Web Optimizer', 'Web Optimizer.png', 'http://www.web-optimizer.us') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Web2py', 'Web2py.png', 'http://web2py.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('WebGUI', 'WebGUI.png', 'http://www.webgui.org') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('WebPublisher', 'WebPublisher.png', 'http://scannet.dk') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Webix', 'Webix.png', 'http://webix.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Webs', 'Webs.png', 'http://webs.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('WebsPlanet', 'WebsPlanet.png', 'http://websplanet.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Websale', 'Websale.png', 'http://websale.de') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('WebsiteBaker', 'WebsiteBaker.png', 'http://websitebaker2.org/en/home.php') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Webtrekk', 'Webtrekk.png', 'http://www.webtrekk.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Webtrends', 'Webtrends.png', 'http://worldwide.webtrends.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Weebly', 'Weebly.png', 'http://www.weebly.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Wikispaces', 'Wikispaces.png', 'http://www.wikispaces.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('WikkaWiki', 'WikkaWiki.png', 'http://wikkawiki.org') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Windows CE', 'Microsoft.svg', 'http://microsoft.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Windows Server', 'Microsoft.svg', 'http://microsoft.com/windowsserver') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Wink', 'Wink.png', 'http://winktoolkit.org') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Winstone Servlet Container', '', 'http://winstone.sourceforge.net') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Wix', 'Wix.png', 'http://wix.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Wolf CMS', 'Wolf CMS.png', 'http://www.wolfcms.org') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Woltlab Community Framework', 'Woltlab Community Framework.png', 'http://www.woltlab.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('WooCommerce', 'WooCommerce.png', 'http://www.woothemes.com/woocommerce') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Woopra', 'Woopra.png', 'http://www.woopra.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('WordPress', 'WordPress.svg', 'http://wordpress.org') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('WordPress Super Cache', 'wp_super_cache.png', 'http://z9.io/wp-super-cache/') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Wowza Media Server', 'Wowza Media Server.png', 'http://www.wowza.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('X-Cart', 'X-Cart.png', 'http://x-cart.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('XAMPP', 'XAMPP.png', 'http://www.apachefriends.org/en/xampp.html') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('XMB', 'XMB.png', 'http://www.xmbforum.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('XOOPS', 'XOOPS.png', 'http://xoops.org') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('XRegExp', 'XRegExp.png', 'http://xregexp.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Xajax', 'Xajax.png', 'http://xajax-project.org') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Xanario', 'Xanario.png', 'http://xanario.de') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('XenForo', 'XenForo.png', 'http://xenforo.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Xitami', 'Xitami.png', 'http://xitami.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Xonic', 'xonic.png', 'http://www.xonic-solutions.de') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('XpressEngine', 'XpressEngine.png', 'http://www.xpressengine.com/') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('YUI', 'YUI.png', 'http://yuilibrary.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('YUI Doc', 'yahoo.png', 'http://developer.yahoo.com/yui/yuidoc') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('YaBB', 'YaBB.png', 'http://www.yabbforum.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Yahoo Advertising', 'yahoo.png', 'http://advertising.yahoo.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Yahoo! Ecommerce', 'yahoo.png', 'http://smallbusiness.yahoo.com/ecommerce') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Yahoo! Web Analytics', 'yahoo.png', 'http://web.analytics.yahoo.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Yandex.Direct', 'Yandex.Direct.png', 'http://partner.yandex.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Yandex.Metrika', 'Yandex.Metrika.png', 'http://metrika.yandex.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Yaws', 'Yaws.png', 'http://yaws.hyber.org') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Yieldlab', 'Yieldlab.png', 'http://yieldlab.de') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Yii', 'Yii.png', 'http://yiiframework.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Yoast SEO', 'Yoast SEO.png', 'http://yoast.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('YouTrack', 'YouTrack.png', 'http://www.jetbrains.com/youtrack/') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('YouTube', 'YouTube.png', 'http://www.youtube.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('ZK', 'ZK.png', 'http://zkoss.org') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('ZURB Foundation', 'ZURB Foundation.png', 'http://foundation.zurb.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Zabbix', 'Zabbix.png', 'http://zabbix.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Zanox', 'Zanox.png', 'http://zanox.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Zen Cart', 'Zen Cart.png', 'http://www.zen-cart.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Zend', 'Zend.png', 'http://zend.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Zendesk Chat', 'Zendesk Chat.png', 'http://zopim.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Zepto', 'Zepto.png', 'http://zeptojs.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Zeuscart', 'Zeuscart.png', 'http://zeuscart.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Zinnia', 'Zinnia.png', 'http://django-blog-zinnia.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Zope', 'Zope.png', 'http://zope.org') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('a-blog cms', 'a-blog cms.svg', 'http://www.a-blogcms.jp') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('actionhero.js', 'actionhero.js.png', 'http://www.actionherojs.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('amCharts', 'amCharts.png', 'http://amcharts.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('basket.js', 'basket.js.png', 'http://addyosmani.github.io/basket.js/') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('cPanel', 'cPanel.png', 'http://www.cpanel.net') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('cgit', 'cgit.png', 'http://git.zx2c4.com/cgit') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('comScore', 'comScore.png', 'http://comscore.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('debut', 'debut.png', 'http://www.brother.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('dwhttpd', '', 'http://example.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('e107', 'e107.png', 'http://e107.org') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('eDevice SmartStack', 'eDevice SmartStack.png', 'http://edevice.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('eHTTP', '', 'http://example.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('eSyndiCat', 'eSyndiCat.png', 'http://esyndicat.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('eZ Publish', 'eZ Publish.png', 'http://ez.no') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('enduro.js', 'enduro.js.svg', 'http://endurojs.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('git', 'git.png', 'http://git-scm.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('gitlist', '', 'http://gitlist.org') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('gitweb', 'git.png', 'http://git-scm.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('govCMS', 'govCMS.svg', 'https://www.govcms.gov.au') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('gunicorn', 'gunicorn.png', 'http://gunicorn.org') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('hapi.js', 'hapi.js.png', 'http://hapijs.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('iCongo', 'Hybris.png', 'http://hybris.com/icongo') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('iPresta', 'iPresta.png', 'http://ipresta.ir') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('iWeb', 'iWeb.png', 'http://apple.com/ilife/iweb') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('ikiwiki', 'ikiwiki.png', 'http://ikiwiki.info') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('io4 CMS', 'io4 CMS.png', 'http://notenbomer.nl/Producten/Content_management/io4_|_cms') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('Jetshop', 'Jetshop.png', 'http://jetshop.se') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('jQTouch', 'jQTouch.png', 'http://jqtouch.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('jQuery', 'jQuery.svg', 'http://jquery.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('jQuery Mobile', 'jQuery Mobile.svg', 'http://jquerymobile.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('jQuery Sparklines', '', 'http://omnipotent.net/jquery.sparkline/') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('jQuery UI', 'jQuery UI.svg', 'http://jqueryui.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('jqPlot', 'jqPlot.png', 'http://www.jqplot.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('libwww-perl-daemon', 'libwww-perl-daemon.png', 'http://metacpan.org/pod/HTTP::Daemon') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('lighttpd', 'lighttpd.png', 'http://www.lighttpd.net') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('math.js', 'math.js.png', 'http://mathjs.org') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('mini_httpd', 'mini_httpd.png', 'http://acme.com/software/mini_httpd') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('mod_auth_pam', 'Apache.svg', 'http://pam.sourceforge.net/mod_auth_pam') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('mod_dav', 'Apache.svg', 'http://webdav.org/mod_dav') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('mod_fastcgi', 'Apache.svg', 'http://www.fastcgi.com/mod_fastcgi/docs/mod_fastcgi.html') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('mod_jk', 'Apache.svg', 'http://tomcat.apache.org/tomcat-3.3-doc/mod_jk-howto.html') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('mod_perl', 'mod_perl.png', 'http://perl.apache.org') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('mod_python', 'mod_python.png', 'http://www.modpython.org') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('mod_rack', 'Phusion Passenger.png', 'http://phusionpassenger.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('mod_rails', 'Phusion Passenger.png', 'http://phusionpassenger.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('mod_ssl', 'mod_ssl.png', 'http://modssl.org') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('mod_wsgi', 'mod_wsgi.png', 'http://code.google.com/p/modwsgi') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('nopCommerce', 'nopCommerce.png', 'http://www.nopcommerce.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('openEngine', 'openEngine.png', 'http://openengine.de/html/pages/de/') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('osCSS', 'osCSS.png', 'http://www.oscss.org') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('osCommerce', 'osCommerce.png', 'http://www.oscommerce.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('osTicket', 'osTicket.png', 'http://osticket.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('otrs', 'otrs.png', 'https://www.otrs.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('ownCloud', 'ownCloud.png', 'http://owncloud.org') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('papaya CMS', 'papaya CMS.png', 'http://papaya-cms.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('phpAlbum', 'phpAlbum.png', 'http://phpalbum.net') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('phpBB', 'phpBB.png', 'http://phpbb.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('phpCMS', 'phpCMS.png', 'http://phpcms.de') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('phpDocumentor', 'phpDocumentor.png', 'http://www.phpdoc.org') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('phpMyAdmin', 'phpMyAdmin.png', 'http://www.phpmyadmin.net') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('phpPgAdmin', 'phpPgAdmin.png', 'http://phppgadmin.sourceforge.net') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('phpSQLiteCMS', 'phpSQLiteCMS.png', 'http://phpsqlitecms.net') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('phpliteadmin', 'phpliteadmin.png', 'http://www.phpliteadmin.org/') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('phpwind', 'phpwind.png', 'http://www.phpwind.net') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('prettyPhoto', 'prettyPhoto.png', 'http://no-margin-for-errors.com/projects/prettyphoto-jquery-lightbox-clone/') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('punBB', 'punBB.png', 'http://punbb.informer.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('reCAPTCHA', 'reCAPTCHA.png', 'https://www.google.com/recaptcha/') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('sIFR', 'sIFR.png', 'http://www.mikeindustries.com/blog/sifr') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('sNews', 'sNews.png', 'http://snewscms.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('script.aculo.us', 'script.aculo.us.png', 'http://script.aculo.us') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('shine.js', '', 'http://bigspaceship.github.io/shine.js/') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('swift.engine', 'swift.engine.png', 'http://mittec.ru/default') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('three.js', 'three.js.png', 'http://threejs.org') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('thttpd', 'thttpd.png', 'http://acme.com/software/thttpd') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('total.js', 'total.js.png', 'http://totaljs.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('uCore', 'uCore.png', 'http://ucore.io') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('uKnowva', 'uKnowva.png', 'http://uknowva.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('vBulletin', 'vBulletin.png', 'http://www.vbulletin.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('vibecommerce', 'vibecommerce.png', 'http://vibecommerce.com.br') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('viennaCMS', '', 'http://www.viennacms.nl') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('webEdition', 'webEdition.png', 'http://webedition.de/en') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('webpack', 'webpack.svg', 'http://webpack.github.io') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('wpCache', 'wpCache.png', 'http://wpcache.co') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('xCharts', '', 'http://tenxer.github.io/xcharts/') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('xtCommerce', 'xtCommerce.png', 'http://www.xt-commerce.com') 
 GO 
 INSERT INTO [dbo].[Technology]([TechnologyName],[Icon],[Website]) VALUES ('xui', 'xui.png', 'http://xuijs.com') 
 GO 
 