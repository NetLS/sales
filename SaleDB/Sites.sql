﻿GO
SET IDENTITY_INSERT [dbo].[Site] ON 

GO
INSERT [dbo].[Site] ([SiteId], [Url], [XPath], [FirstProxyIp], [FirstProxyPort], [NextCheck], [Interval], [MoveTemplate], [MoveStep], [MoveFinish], [TypeContent], [Active], [IsJSNeed]) VALUES (1, N'http://proxydb.net/?protocol=http&protocol=https', N'/html/body/div[2]/table/tbody/tr/td[1]/a/text()', NULL, NULL, CAST(N'2017-12-02T23:05:42.303' AS DateTime), 90000, N'&offset={0}', 20, 300, 1, 1, 1)
GO
INSERT [dbo].[Site] ([SiteId], [Url], [XPath], [FirstProxyIp], [FirstProxyPort], [NextCheck], [Interval], [MoveTemplate], [MoveStep], [MoveFinish], [TypeContent], [Active], [IsJSNeed]) VALUES (2, N'https://hidemy.name/en/proxy-list/?type=hs', N'//*[@id=''content-section'']/section[1]/div/table/tbody/tr/td[position()<=2]/text()', NULL, NULL, CAST(N'2017-10-05T15:05:42.310' AS DateTime), 90000, N'&start={0}', 64, 610, 0, 1, 0)
GO
INSERT [dbo].[Site] ([SiteId], [Url], [XPath], [FirstProxyIp], [FirstProxyPort], [NextCheck], [Interval], [MoveTemplate], [MoveStep], [MoveFinish], [TypeContent], [Active], [IsJSNeed]) VALUES (3, N'https://list.proxylistplus.com/SSL-List-', N'//table[2]/tbody//td[position()<=3]/text()', NULL, NULL, CAST(N'2017-12-29T00:05:42.313' AS DateTime), 90000, N'{0}', 1, 3, 0, 1, 1)
GO
INSERT [dbo].[Site] ([SiteId], [Url], [XPath], [FirstProxyIp], [FirstProxyPort], [NextCheck], [Interval], [MoveTemplate], [MoveStep], [MoveFinish], [TypeContent], [Active], [IsJSNeed]) VALUES (4, N'https://free-proxy-list.net/', N'//*[@id=''proxylisttable'']/tbody/tr//td[position()<=2]/text()', NULL, NULL, CAST(N'2017-10-05T15:05:42.317' AS DateTime), 90000, NULL, NULL, NULL, 0, 1, 0)
GO
INSERT [dbo].[Site] ([SiteId], [Url], [XPath], [FirstProxyIp], [FirstProxyPort], [NextCheck], [Interval], [MoveTemplate], [MoveStep], [MoveFinish], [TypeContent], [Active], [IsJSNeed]) VALUES (5, N'http://www.httptunnel.ge/ProxyListForFree.aspx', N'//*[@id=''ctl00_ContentPlaceHolder1_GridViewNEW'']/tbody/tr/td[1]', NULL, NULL, CAST(N'2017-11-25T16:05:42.317' AS DateTime), 90000, NULL, NULL, NULL, 1, 1, 1)
GO
INSERT [dbo].[Site] ([SiteId], [Url], [XPath], [FirstProxyIp], [FirstProxyPort], [NextCheck], [Interval], [MoveTemplate], [MoveStep], [MoveFinish], [TypeContent], [Active], [IsJSNeed]) VALUES (6, N'http://foxtools.ru/Proxy', N'//*[@id=''theProxyList'']/tbody/tr/td[position()<=3]/text()', NULL, NULL, CAST(N'2017-09-26T06:05:42.317' AS DateTime), 90000, N'?page={0}', 1, 12, 0, 1, 0)
GO
INSERT [dbo].[Site] ([SiteId], [Url], [XPath], [FirstProxyIp], [FirstProxyPort], [NextCheck], [Interval], [MoveTemplate], [MoveStep], [MoveFinish], [TypeContent], [Active], [IsJSNeed]) VALUES (7, N'https://premproxy.com/ru/proxy/', N'//*[@id=''proxylist'']//tbody/tr/td[1]/text()', NULL, NULL, CAST(N'2017-12-02T23:05:42.320' AS DateTime), 90000, N'0{0}.htm', 1, 10, 1, 1, 0)
GO
INSERT [dbo].[Site] ([SiteId], [Url], [XPath], [FirstProxyIp], [FirstProxyPort], [NextCheck], [Interval], [MoveTemplate], [MoveStep], [MoveFinish], [TypeContent], [Active], [IsJSNeed]) VALUES (8, N'https://hidemy.life/ru/proxy-list-servers', N'//*[@id=''table-list'']/table/tbody/tr/td[2]', NULL, NULL, CAST(N'2017-11-21T12:05:42.323' AS DateTime), 90000, NULL, NULL, NULL, 1, 1, 1)
GO
INSERT [dbo].[Site] ([SiteId], [Url], [XPath], [FirstProxyIp], [FirstProxyPort], [NextCheck], [Interval], [MoveTemplate], [MoveStep], [MoveFinish], [TypeContent], [Active], [IsJSNeed]) VALUES (9, N'https://www.hide-my-ip.com/ru/proxylist.shtml', N'//*[@id=''sort-list'']/tbody/tr/td[position()<=2]/text()', NULL, NULL, CAST(N'2017-10-13T20:46:40.000' AS DateTime), 90000, NULL, NULL, NULL, 0, 1, 1)
GO
INSERT [dbo].[Site] ([SiteId], [Url], [XPath], [FirstProxyIp], [FirstProxyPort], [NextCheck], [Interval], [MoveTemplate], [MoveStep], [MoveFinish], [TypeContent], [Active], [IsJSNeed]) VALUES (10, N'http://freeproxylists.net/', N'/html/body/div[1]/div[2]/table/tbody/tr[position()>1]/(td[position()=1]/a|td[position()=2])', NULL, NULL, CAST(N'2017-09-29T20:00:00.000' AS DateTime), 90000, N'?page={0}', 1, 28, 0, 1, 0)
GO
INSERT [dbo].[Site] ([SiteId], [Url], [XPath], [FirstProxyIp], [FirstProxyPort], [NextCheck], [Interval], [MoveTemplate], [MoveStep], [MoveFinish], [TypeContent], [Active], [IsJSNeed]) VALUES (11, N'http://spys.one/en/free-proxy-list/', N'/html/body/table[2]/tbody/tr[4]/td/table/tbody/tr/td[1]/font[2]/text()', NULL, NULL, CAST(N'2017-09-20T11:00:00.000' AS DateTime), 90000, N'{0}/', 1, 2, 0, 1, 1)
GO
INSERT [dbo].[Site] ([SiteId], [Url], [XPath], [FirstProxyIp], [FirstProxyPort], [NextCheck], [Interval], [MoveTemplate], [MoveStep], [MoveFinish], [TypeContent], [Active], [IsJSNeed]) VALUES (12, N'https://proxy-list.org/english/index.php', N'//*[@id="proxy-table"]/div[2]/div/ul/li[1]/text()', NULL, NULL, CAST(N'2017-09-18T09:00:00.000' AS DateTime), 90000, N'?p={0}', 1, 10, 1, 1, 1)
GO
INSERT [dbo].[Site] ([SiteId], [Url], [XPath], [FirstProxyIp], [FirstProxyPort], [NextCheck], [Interval], [MoveTemplate], [MoveStep], [MoveFinish], [TypeContent], [Active], [IsJSNeed]) VALUES (13, N'http://list.proxylistplus.com/Fresh-HTTP-Proxy-List-', N'//table[2]/tbody//td[position()<=3]/text()', NULL, NULL, CAST(N'2017-09-16T07:00:00.000' AS DateTime), 90000, N'{0}', 1, 7, 0, 1, 1)
GO
INSERT [dbo].[Site] ([SiteId], [Url], [XPath], [FirstProxyIp], [FirstProxyPort], [NextCheck], [Interval], [MoveTemplate], [MoveStep], [MoveFinish], [TypeContent], [Active], [IsJSNeed]) VALUES (14, N'https://kss.pp.ua/spisok-rabochikh-proksi-serverov/', N'//div/div/div/article/div/p/text()', NULL, NULL, CAST(N'2017-09-23T14:00:00.000' AS DateTime), 90000, NULL, NULL, NULL, 1, 1, 0)
GO
INSERT [dbo].[Site] ([SiteId], [Url], [XPath], [FirstProxyIp], [FirstProxyPort], [NextCheck], [Interval], [MoveTemplate], [MoveStep], [MoveFinish], [TypeContent], [Active], [IsJSNeed]) VALUES (15, N'https://www.proxydocker.com/ru', N'/html/body/div[2]/div/div[1]/div/div[5]/table/tbody/tr/td[1]//text()', NULL, NULL, CAST(N'2017-09-23T14:00:00.000' AS DateTime), 90000, N'?page={0}', 1, 381, 1, 1, 0)
GO
INSERT [dbo].[Site] ([SiteId], [Url], [XPath], [FirstProxyIp], [FirstProxyPort], [NextCheck], [Interval], [MoveTemplate], [MoveStep], [MoveFinish], [TypeContent], [Active], [IsJSNeed]) VALUES (16, N'http://ru.proxyservers.pro/proxy/list/protocol/http%2Chttps/order/updated/order_dir/desc/', N'//*[@id="content-content"]/div/div[1]/table/tbody/tr/td[position()=2 or position()=3]//text()', NULL, NULL, CAST(N'2017-09-20T11:00:00.000' AS DateTime), 90000, N'page/{0}', 1, 18, 0, 1, 1)
GO
INSERT [dbo].[Site] ([SiteId], [Url], [XPath], [FirstProxyIp], [FirstProxyPort], [NextCheck], [Interval], [MoveTemplate], [MoveStep], [MoveFinish], [TypeContent], [Active], [IsJSNeed]) VALUES (17, N'http://fineproxy.org/eng/fresh-proxies/', N'//*[@id="post-1"]/div/p/text()', NULL, NULL, CAST(N'2017-11-09T04:00:00.000' AS DateTime), 90000, NULL, NULL, NULL, 1, 1, 0)
GO
SET IDENTITY_INSERT [dbo].[Site] OFF
GO