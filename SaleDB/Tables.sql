﻿CREATE TABLE [dbo].[Location] (
    LocationId      int IDENTITY(1, 1)   NOT NULL,
    CountryName     nvarchar(100)        NOT NULL,
    CountryCode     nvarchar(2)          NOT NULL,

    CONSTRAINT PK_Location PRIMARY KEY (LocationId)
);

CREATE TABLE [dbo].[IpRang] (
    IpRangId        int IDENTITY(1, 1)  NOT NULL,
    LocationId      int                 NOT NULL,
    IpFrom          binary(4)           NULL, 
    IpTo            binary(4)           NULL,
    
    CONSTRAINT PK_IpRang PRIMARY KEY (IpRangId),
    CONSTRAINT FK_LocationIpRang FOREIGN KEY (LocationId) REFERENCES [dbo].[Location](LocationId)
);

CREATE TABLE [dbo].[Site] (
    SiteId          int IDENTITY(1, 1) NOT NULL,
    [Url]           nvarchar(max)      NOT NULL,
    XPath           nvarchar(max)      NOT NULL,
    FirstProxyIp    binary(4)          NULL,
    FirstProxyPort  int                NULL,
    NextCheck       datetime           NOT NULL,
    Interval        int                NOT NULL,
    MoveTemplate    nvarchar(200)      NULL,
    MoveStep        int                NULL,
    MoveFinish      int                NULL,
    TypeContent     int                NOT NULL, -- HTML, XML, JSON maybe need table to store more info
    Active          bit                NOT NULL,
	IsJSNeed        bit                NULL

    CONSTRAINT PK_Site PRIMARY KEY (SiteId)
);

CREATE TABLE [dbo].[Proxy] (
    SiteId          int               NOT NULL,
    ProxyIp         binary(4)         NOT NULL,
    ProxyPort       int               NOT NULL,
    [Location]      int               NULL,
    Created         datetime          NOT NULL,
    IsHttpsAllowed  bit               NOT NULL, -- Http and https allowed
	TypeAnonymity	tinyint			  NOT NULL -- 0 - none, 1 - anonimus, 2 - high
    
    CONSTRAINT PK_Proxy PRIMARY KEY (ProxyIp, ProxyPort),
    CONSTRAINT FK_ProxyLocation FOREIGN KEY ([Location]) REFERENCES [dbo].[Location](LocationId),
    CONSTRAINT FK_ProxySite FOREIGN KEY (SiteId) REFERENCES [dbo].[Site](SiteId)
);

GO  

CREATE TABLE [dbo].[Log](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Application] [nvarchar](50) NOT NULL,
	[Logged] [datetime] NOT NULL,
	[Level] [nvarchar](50) NOT NULL,
	[Message] [nvarchar](max) NOT NULL,
	[Logger] [nvarchar](250) NULL,
	[Callsite] [nvarchar](max) NULL,
	[Exception] [nvarchar](max) NULL,
 CONSTRAINT PK_Log PRIMARY KEY (Id)
)

GO

CREATE TABLE [dbo].[Sprint] (
    SprintId    int IDENTITY(1, 1)  NOT NULL,
    [Name]      nvarchar(max)       NOT NULL,
    [From]      datetime            NULL,
    [To]        datetime            NULL

    CONSTRAINT PK_Sprint PRIMARY KEY (SprintId)
);

CREATE TABLE [dbo].[Tasks] (
    TaskId      int IDENTITY(1, 1)  NOT NULL,
    SprintId    int                 NULL,
    TaskName    nvarchar(max)       NOT NULL,
    TargetId	int				    NOT NULL,
    FiltersJson nvarchar(max)       NULL,
	[Status]    int                 NULL

    CONSTRAINT PK_Task PRIMARY KEY (TaskId),
    CONSTRAINT FK_SprintSample FOREIGN KEY (SprintId) REFERENCES [dbo].[Sprint](SprintId)
);

CREATE TABLE [dbo].[Technology](
	TechnologyId	int IDENTITY(1,1) NOT NULL,
	TechnologyName	nvarchar(100) NULL,
	Icon			nvarchar(100) NULL,
	Website			nvarchar(200) NULL

	CONSTRAINT PK_Technology PRIMARY KEY (TechnologyId),
);

CREATE TABLE [dbo].[TechnologyFilter](
	TechnologyFilterId int IDENTITY(1,1) NOT NULL,
	TaskId  		   int				 NOT NULL,
	TechnologyId	   int				 NOT NULL,
	VersionJson		   nvarchar(max)     NULL

	CONSTRAINT PK_TechnologyFilter PRIMARY KEY (TechnologyFilterId),
	CONSTRAINT FK_TechnologyFilter_Filter FOREIGN KEY (TaskId) REFERENCES [dbo].[Tasks](TaskId),
	CONSTRAINT FK_TechnologyFilter_Technology FOREIGN KEY (TechnologyId) REFERENCES [dbo].[Technology](TechnologyId)
);

CREATE TABLE [dbo].[Paginator] (
    PaginatorId int IDENTITY (1,1) NOT NULL,
    TaskId    int         NOT NULL,
    PaginatorUrl nvarchar(max) NULL

    CONSTRAINT PK_Paginator PRIMARY KEY (PaginatorId),
    CONSTRAINT FK_FilterPaginator FOREIGN KEY (TaskId) REFERENCES [dbo].[Tasks](TaskId)
);

CREATE TABLE [dbo].[Url] (
    UrlId int IDENTITY (1,1) NOT NULL,
    UrlLink nvarchar(max)    NULL,
    LastCheck datetime NULL

    CONSTRAINT PK_Url PRIMARY KEY (UrlId)
);

CREATE TABLE [dbo].[SiteData] (
    SiteDataId int IDENTITY (1,1) NOT NULL,
    UrlId int    NULL,
    [Check] datetime NULL

    CONSTRAINT PK_SiteData PRIMARY KEY (SiteDataId),
    CONSTRAINT FK_SiteDataUrl FOREIGN KEY (UrlId) REFERENCES [dbo].[Url](UrlId),
);

CREATE TABLE [dbo].[UrlResult] (
    UrlResultId int IDENTITY (1,1) NOT NULL,
    TaskId    int          NULL,
    PaginatorId    int        NULL,
    SiteDataId    int          NULL,
    UrlLink nvarchar(max)    NULL,
    [Status] int NULL, --  0 - Pending, 1- success, 2 fail
	UrlId int    NULL,
	IsMatch bit NULL,
	CountUrl int    NULL

    CONSTRAINT PK_UrlResult PRIMARY KEY (UrlResultId),
    CONSTRAINT FK_PaginatorUrlResult FOREIGN KEY (PaginatorId) REFERENCES [dbo].[Paginator](PaginatorId),
    CONSTRAINT FK_TaskUrlResult FOREIGN KEY (TaskId) REFERENCES [dbo].[Tasks](TaskId),
    CONSTRAINT FK_TaskSiteData FOREIGN KEY (SiteDataId) REFERENCES [dbo].[SiteData](SiteDataId)
);

CREATE TABLE [dbo].[TechnologySiteData](
	TechnologySiteDataId int IDENTITY(1,1) NOT NULL,
	SiteDataId  		   int				  NULL,
	TechnologyId	   int				  NULL,
	[Version] 		   nvarchar(max)     NULL

	CONSTRAINT PK_TechnologySiteData PRIMARY KEY (TechnologySiteDataId),
	CONSTRAINT FK_TechnologySiteData_SiteData FOREIGN KEY (SiteDataId) REFERENCES [dbo].[SiteData](SiteDataId),
	CONSTRAINT FK_TechnologySiteData_Technology FOREIGN KEY (TechnologyId) REFERENCES [dbo].[Technology](TechnologyId)
);

CREATE TABLE SiteResponsive(
    SiteResponsiveId int IDENTITY(1, 1)     NOT NULL,
    SiteDataId       int                    NULL,
    Alert		tinyint                NULL,
    DocWidth   tinyint                NULL,
    Flash      tinyint                NULL,
    FontSize   tinyint                NULL,
    TouchScreen tinyint               NULL,
    ViewPort   tinyint                NULL,
    ScreenShot      varchar(500)            NULL,
    CONSTRAINT PK_SiteSiteResponsive PRIMARY KEY (SiteResponsiveId),
    CONSTRAINT FK_SiteDataSiteResponsive FOREIGN KEY (SiteDataId) REFERENCES [dbo].[SiteData](SiteDataId)
)