﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE TYPE dbo.ProxyList AS TABLE (
    ip      nvarchar(15)    NOT NULL,
    port    int             NOT NULL,
    siteId  int             NOT NULL,
	isHttpsAllowed bit		NULL,
	typeAnonymity	tinyint NULL
)

GO

CREATE TYPE dbo.ProxyFilterSite AS TABLE (
    siteId      int         NULL
)

GO

CREATE TYPE dbo.ProxyFilterLocate AS TABLE (
    locateId    int         NULL
)

CREATE TYPE dbo.ProxyFilterTypeAnonymity AS TABLE (
    typeAnonymity    int         NULL
)

GO

CREATE TYPE dbo.UrlResultList AS TABLE (
    urlResultId int          NULL,
    taskId    int            NULL,
    paginatorId    int       NULL,
    siteDataId    int          NULL,
    urlLink nvarchar(max)    NULL,
    [status] int NULL,
    urlId int    NULL,
    isMatch bit NULL,
    countUrl int    NULL
)

GO

CREATE TYPE dbo.SiteDataList AS TABLE (
    technologySiteDataId int          NULL,
    siteId    int                    NULL,
    technologyId    int               NULL,
    [version] nvarchar(max)          NULL
)

GO

CREATE TYPE dbo.SiteResponsiveList AS TABLE (
    siteDataId       int                    NULL,
    alert      tinyint                NULL,
    docWidth   tinyint                NULL,
    flash      tinyint                NULL,
    fontSize   tinyint                NULL,
    touchScreen tinyint               NULL,
    viewPort   tinyint                NULL,
    screenShot       nvarchar(200)            NULL
)

GO

CREATE TYPE dbo.TechFilter AS TABLE (
    technologyId      int         NULL,
    [version]         nvarchar(max) null
)

GO

CREATE TYPE dbo.UrlFilter AS TABLE (
    [url]         nvarchar(max) null
)

GO

CREATE TYPE dbo.TaskFilter AS TABLE (
    taskId         int	 null
)

GO

CREATE PROCEDURE dbo.IPv4ToBinary 
    @ip VARCHAR(15)
AS
BEGIN
    DECLARE @bin BINARY(4)

    SELECT @bin = CAST(CAST(PARSENAME(@ip, 4) AS INTEGER) AS BINARY(1))
                + CAST(CAST(PARSENAME(@ip, 3) AS INTEGER) AS BINARY(1))
                + CAST(CAST(PARSENAME(@ip, 2) AS INTEGER) AS BINARY(1))
                + CAST(CAST(PARSENAME(@ip, 1) AS INTEGER) AS BINARY(1))

    RETURN @bin
END

GO

CREATE PROCEDURE [dbo].[BinaryToIPv4]
    @ip BINARY(4),
    @str VARCHAR(15) OUT
AS
BEGIN
    SELECT @str = CAST(CAST(SUBSTRING(@ip, 1, 1) AS INTEGER) AS VARCHAR(3)) + '.'
                + CAST(CAST(SUBSTRING(@ip, 2, 1) AS INTEGER) AS VARCHAR(3)) + '.'
                + CAST(CAST(SUBSTRING(@ip, 3, 1) AS INTEGER) AS VARCHAR(3)) + '.'
                + CAST(CAST(SUBSTRING(@ip, 4, 1) AS INTEGER) AS VARCHAR(3))
END;

GO

CREATE PROCEDURE dbo.GetLocationByBinaryIp
    @ip BINARY(4)
AS 
BEGIN
    DECLARE @locId int 

    SELECT TOP(1) @locId = [Location].LocationId     
    FROM    [Location] INNER JOIN [IpRang] ON [IpRang].LocationId = [Location].LocationId
    WHERE   @ip >= [IpRang].IpFrom AND @ip <= [IpRang].IpTo;

    RETURN (ISNULL(@locId,0))
END;

GO

CREATE PROCEDURE [dbo].[CreateOrUpdateProxy]
    @ip				nvarchar(15),
    @port			int,
    @siteId			int,
	@isHttpsAllowed bit,
	@typeAnonymity	tinyint
AS
BEGIN
    DECLARE @ipBinary binary(4)
    DECLARE @locId int 

    EXEC @ipBinary = dbo.IPv4ToBinary @ip

    EXEC @locId = dbo.GetLocationByBinaryIp @ipBinary
	IF @locId = 0
	BEGIN
		SET @locId = null
	END
    
    IF NOT EXISTS (SELECT * FROM dbo.Proxy WHERE Proxy.ProxyIp = @ipBinary AND Proxy.ProxyPort = @port)
    BEGIN
        INSERT INTO dbo.Proxy(SiteId, ProxyIp, ProxyPort, [Location], Created, IsHttpsAllowed, TypeAnonymity) VALUES (@siteId, @ipBinary, @port, @locId, CURRENT_TIMESTAMP, @isHttpsAllowed, @typeAnonymity)
    END
    ELSE
    BEGIN
        UPDATE dbo.Proxy SET [Location]=@locId, Created=CURRENT_TIMESTAMP, IsHttpsAllowed=@isHttpsAllowed, TypeAnonymity=@typeAnonymity WHERE Proxy.ProxyIp = @ipBinary AND Proxy.ProxyPort = @port
    END
END;

GO

CREATE PROCEDURE [dbo].[CreateOrUpdateProxyList]
    @proxyList dbo.ProxyList READONLY
AS
BEGIN
    DECLARE @siteIdCursor int
    DECLARE @proxyIpCursor nvarchar(15)
    DECLARE @proxyPortCursor int
	DECLARE @isHttpsAllowedCursor bit
	DECLARE @typeAnonymityCursor tinyint

    DECLARE dataCursor CURSOR FORWARD_ONLY FOR (SELECT siteId, ip, port, isHttpsAllowed, typeAnonymity FROM @proxyList)

    OPEN dataCursor
    FETCH NEXT FROM dataCursor INTO @siteIdCursor, @proxyIpCursor, @proxyPortCursor, @isHttpsAllowedCursor, @typeAnonymityCursor

    WHILE @@FETCH_STATUS = 0 BEGIN
        EXEC dbo.CreateOrUpdateProxy @proxyIpCursor, @proxyPortCursor, @siteIdCursor, @isHttpsAllowedCursor, @typeAnonymityCursor
        FETCH NEXT FROM dataCursor INTO @siteIdCursor, @proxyIpCursor, @proxyPortCursor, @isHttpsAllowedCursor, @typeAnonymityCursor
    END;
    
    CLOSE dataCursor;
    DEALLOCATE dataCursor;
END

GO

CREATE PROCEDURE dbo.DeleteProxy
    @ip     nvarchar(15),
    @port   int,
    @siteId int
AS
BEGIN
    DECLARE @ipBinary binary(4)
    DECLARE @locId int 
    DECLARE @countProxy int

    EXEC @ipBinary = dbo.IPv4ToBinary @ip
    
    IF EXISTS (SELECT * FROM dbo.Proxy WHERE Proxy.SiteId = @siteId AND Proxy.ProxyIp = @ipBinary AND Proxy.ProxyPort = @port)
    BEGIN
        DELETE FROM dbo.Proxy WHERE Proxy.SiteId = @siteId AND Proxy.ProxyIp = @ipBinary AND Proxy.ProxyPort = @port
    END
END;

GO

CREATE PROCEDURE dbo.DeleteProxyList
    @proxyList dbo.ProxyList READONLY
AS
BEGIN
    DECLARE @siteIdCursor int
    DECLARE @proxyIpCursor nvarchar(15)
    DECLARE @proxyPortCursor int

    DECLARE dataCursor CURSOR FORWARD_ONLY FOR (SELECT siteId, ip, port FROM @proxyList)

    OPEN dataCursor
    FETCH NEXT FROM dataCursor INTO @siteIdCursor, @proxyIpCursor, @proxyPortCursor

    WHILE @@FETCH_STATUS = 0 BEGIN
        EXEC dbo.DeleteProxy @siteIdCursor, @proxyIpCursor, @proxyPortCursor
        FETCH NEXT FROM dataCursor INTO @siteIdCursor, @proxyIpCursor, @proxyPortCursor
    END;
    
    CLOSE dataCursor;
    DEALLOCATE dataCursor;
END

GO

CREATE PROCEDURE [dbo].[GetProxies]
     @siteIds   dbo.ProxyFilterSite READONLY ,
     @locateIds dbo.ProxyFilterLocate  READONLY,
	 @typeAnonymity dbo.ProxyFilterTypeAnonymity  READONLY,
	 @isHttpsAllowed	bit = null,
     @limit     int = 10,
     @offset    int = 0
AS
BEGIN

	IF @limit=0 BEGIN
        SELECT @limit = COUNT(*) FROM dbo.Proxy
    END
	
    DECLARE @siteIdCursor int
    DECLARE @proxyIpCursor binary(4)
    DECLARE @proxyPortCursor int
    DECLARE @locationCursor int
    DECLARE @createdCursor datetime
	DECLARE @isHttpsAllowedCursor bit
	DECLARE @typeAnonymityCursor tinyint
     
    DECLARE @table TABLE(SiteId int, ProxyIp nvarchar(15), ProxyPort int, [Location] int, Created datetime, IsHttpsAllowed  bit, TypeAnonymity	tinyint)

    DECLARE dataCursor CURSOR FORWARD_ONLY FOR
        SELECT   Proxy.SiteId, Proxy.ProxyIp, Proxy.ProxyPort, Proxy.[Location], Proxy.Created, IsHttpsAllowed, TypeAnonymity
        FROM    dbo.Proxy
        WHERE (IsHttpsAllowed = @isHttpsAllowed OR @isHttpsAllowed IS NULL) AND ((TypeAnonymity in (Select typeAnonymity FROM @typeAnonymity)) or (SELECT COUNT(*) FROM @typeAnonymity) = 0  )
           AND ((Proxy.SiteId IN (SELECT siteId FROM @siteIds) OR (SELECT COUNT(*) FROM @siteIds) = 0) AND ((Proxy.[Location] IN (SELECT locateId FROM @locateIds) OR (SELECT COUNT(*) FROM @locateIds) = 0)))
              ORDER BY Created Offset @offset ROW 
              FETCH NEXT @limit ROWS ONLY
    OPEN dataCursor
    FETCH NEXT FROM dataCursor INTO @siteIdCursor, @proxyIpCursor, @proxyPortCursor, @locationCursor, @createdCursor, @isHttpsAllowedCursor, @typeAnonymityCursor
    
    WHILE @@FETCH_STATUS = 0 BEGIN
        DECLARE @proxyIpChars varchar(15)
        EXEC  dbo.BinaryToIPv4 @proxyIpCursor, @proxyIpChars OUT
        INSERT @table(SiteId, ProxyIp, ProxyPort, [Location], Created, IsHttpsAllowed, TypeAnonymity) VALUES(@siteIdCursor, @proxyIpChars, @proxyPortCursor, @locationCursor, @createdCursor, @isHttpsAllowedCursor, @typeAnonymityCursor)
        FETCH NEXT FROM dataCursor INTO @siteIdCursor, @proxyIpCursor, @proxyPortCursor, @locationCursor, @createdCursor, @isHttpsAllowedCursor, @typeAnonymityCursor
    END;
    
    CLOSE dataCursor;
    DEALLOCATE dataCursor;

    SELECT SiteId, ProxyIp, ProxyPort, [Location], Created, IsHttpsAllowed, TypeAnonymity FROM @table
END;

GO

CREATE PROCEDURE dbo.CreateSite
    @Url            nvarchar(max),
    @XPath          nvarchar(max),
    @FirstProxyIp   nvarchar(15)       NULL,
    @FirstProxyPort int                NULL,
    @NextCheck      datetime,
    @Interval       int,
    @MoveTemplate   nvarchar(200)      NULL,
    @MoveStep       int                NULL,
    @MoveFinish     int                NULL,
    @TypeContent    int, 
    @Active         bit
AS
BEGIN
    DECLARE @ipBinary binary(4)
    EXEC @ipBinary = dbo.IPv4ToBinary @FirstProxyIp

    INSERT INTO dbo.[Site]([Url], XPath, FirstProxyIp, FirstProxyPort, NextCheck, Interval, MoveTemplate, MoveStep, MoveFinish, TypeContent, Active) 
    VALUES (@Url, @XPath, @ipBinary, @FirstProxyPort, @NextCheck, @Interval, @MoveTemplate, @MoveStep, @MoveFinish, @TypeContent, @Active)
END;

GO

CREATE PROCEDURE dbo.UpdateSite
    @SiteId         int,
    @Url            nvarchar(max),
    @XPath          nvarchar(max),
    @FirstProxyIp   nvarchar(15)       NULL,
    @FirstProxyPort int                NULL,
    @NextCheck      datetime,
    @Interval       int,
    @MoveTemplate   nvarchar(200)      NULL,
    @MoveStep       int                NULL,
    @MoveFinish     int                NULL,
    @TypeContent    int, 
    @Active         bit
AS
BEGIN
    DECLARE @ipBinary binary(4)
    EXEC @ipBinary = dbo.IPv4ToBinary @FirstProxyIp

    UPDATE dbo.[Site]
    SET [Url] = @Url, XPath = @XPath, FirstProxyIp = @ipBinary, FirstProxyPort = @FirstProxyPort, NextCheck = @NextCheck, Interval = @Interval, MoveTemplate = @MoveTemplate, MoveStep = @MoveStep, MoveFinish = @MoveFinish, TypeContent = @TypeContent, Active = @Active
    WHERE SiteId = @SiteId
END;

GO

CREATE PROCEDURE dbo.DeleteSite
    @SiteId         int
AS
BEGIN
    DELETE FROM dbo.[Site] WHERE SiteId = @SiteId
END;
GO

CREATE PROCEDURE [dbo].[GetSites]
     @limit     int = 0,
     @offset    int = 0
AS
BEGIN
    DECLARE @siteIdCursor int
    DECLARE @urlCursor nvarchar(max)
    DECLARE @xpathCursor nvarchar(max)
    DECLARE @firstProxyIpCursor binary(4)
    DECLARE @fitstProxyPortCursor int
    DECLARE @nextCheckCursor datetime
    DECLARE @intervalCursor int
    DECLARE @moveTemplateCursor nvarchar(200)
    DECLARE @moveStepCursor int
    DECLARE @moveFinishCursor int
    DECLARE @typeContentCursor int
    DECLARE @activeCursor bit
    DECLARE @isJSNeed bit
    
    DECLARE @proxyIpChars nvarchar(15)

    DECLARE @table TABLE(SiteId int, [Url] nvarchar(max), XPath nvarchar(max), FirstProxyIp nvarchar(15), FirstProxyPort int, NextCheck datetime,
                            Interval int, MoveTemplate nvarchar(200), MoveStep int, MoveFinish int, TypeContent int, Active bit, IsJSNeed bit)

    DECLARE dataCursor CURSOR FORWARD_ONLY FOR
        SELECT  SiteList.SiteId, SiteList.[Url], SiteList.XPath, SiteList.FirstProxyIp, SiteList.FirstProxyPort, SiteList.NextCheck, SiteList.Interval, SiteList.MoveTemplate, SiteList.MoveStep, SiteList.MoveFinish, SiteList.TypeContent, SiteList.Active, SiteList.IsJSNeed
        FROM    ( 
                    SELECT *, ROW_NUMBER() OVER (ORDER BY SiteId) as row FROM dbo.[Site]
                ) SiteList
        WHERE   ( @limit = 0 OR (SiteList.[row] > @offset AND SiteList.[row] <= @limit)) 
    OPEN dataCursor
    FETCH NEXT FROM dataCursor INTO @siteIdCursor, @urlCursor, @xpathCursor, @firstProxyIpCursor, @fitstProxyPortCursor, @nextCheckCursor, @intervalCursor, @moveTemplateCursor, @moveStepCursor, @moveFinishCursor, @typeContentCursor, @activeCursor, @isJSNeed
    
    WHILE @@FETCH_STATUS = 0 BEGIN
        EXEC  dbo.BinaryToIPv4 @firstProxyIpCursor, @proxyIpChars OUT
        INSERT @table VALUES(@siteIdCursor, @urlCursor, @xpathCursor, @proxyIpChars, @fitstProxyPortCursor, @nextCheckCursor, @intervalCursor, @moveTemplateCursor, @moveStepCursor, @moveFinishCursor, @typeContentCursor, @activeCursor, @isJSNeed)
        FETCH NEXT FROM dataCursor INTO @siteIdCursor, @urlCursor, @xpathCursor, @firstProxyIpCursor, @fitstProxyPortCursor, @nextCheckCursor, @intervalCursor, @moveTemplateCursor, @moveStepCursor, @moveFinishCursor, @typeContentCursor, @activeCursor, @isJSNeed
    END;
    
    CLOSE dataCursor;
    DEALLOCATE dataCursor;

    SELECT SiteId, [Url], XPath, FirstProxyIp, FirstProxyPort, NextCheck, Interval, MoveTemplate, MoveStep, MoveFinish, TypeContent, Active, IsJSNeed FROM @table
END;

GO

CREATE PROCEDURE dbo.UpdateSiteCheck
    @siteId int
AS
BEGIN
    UPDATE dbo.[Site] SET NextCheck = DATEADD(SECOND, Interval, CURRENT_TIMESTAMP) WHERE SiteId = @siteId
END;
GO

CREATE PROCEDURE [dbo].[CreateUrlResultList]
    @urlResultList dbo.UrlResultList READONLY,
    @siteDataList dbo.SiteDataList READONLY,
    @siteResponsiveList dbo.SiteResponsiveList READONLY
AS
BEGIN
    DECLARE @urlResultId int
    DECLARE @taskId int
    DECLARE @paginatorId int
	DECLARE @siteDataIdResult int
    DECLARE @urlLinkResult nvarchar(max)
    DECLARE @status int
	DECLARE @urlIdResult int
    DECLARE @isMatch bit
    DECLARE @countUrl int

    DECLARE dataCursor CURSOR FORWARD_ONLY FOR (SELECT urlResultId, taskId, paginatorId, siteDataId, urlLink, [status], urlId, isMatch, countUrl FROM @urlResultList)

    OPEN dataCursor
    FETCH NEXT FROM dataCursor INTO @urlResultId, @taskId, @paginatorId, @siteDataIdResult, @urlLinkResult, @status, @urlIdResult, @isMatch, @countUrl

    WHILE @@FETCH_STATUS = 0 BEGIN

        DECLARE @urlId int
        SELECT @urlId = UrlId FROM  [dbo].[Url] WHERE UrlLink = @urlLinkResult
        IF  @urlId IS NULL
        BEGIN 
            INSERT INTO [dbo].[Url]([UrlLink],[LastCheck])VALUES(@urlLinkResult, CURRENT_TIMESTAMP)
            SELECT @urlId = SCOPE_IDENTITY()
        END

        INSERT INTO [dbo].[SiteData]([UrlId],[Check])VALUES(@urlId, CURRENT_TIMESTAMP)
        DECLARE @siteDataId int
        SELECT @siteDataId = SCOPE_IDENTITY()

        INSERT INTO [dbo].[UrlResult]([TaskId],[PaginatorId],[SiteDataId],[UrlLink],[Status],[UrlId],[IsMatch],[CountUrl]) VALUES (@taskId, @paginatorId, @siteDataId, @urlLinkResult, @status, @urlId, @isMatch, @countUrl)


        DECLARE @technologySiteDataId int
        DECLARE @siteIdResult int
        DECLARE @technologyId int
	    DECLARE @version nvarchar(max)

        DECLARE siteDataCursor CURSOR FORWARD_ONLY FOR (SELECT technologySiteDataId, siteId, technologyId, [version] FROM @siteDataList)
        OPEN siteDataCursor
        FETCH NEXT FROM siteDataCursor INTO @technologySiteDataId, @siteIdResult, @technologyId, @version

        WHILE @@FETCH_STATUS = 0 BEGIN
            IF @urlResultId=@siteIdResult
            BEGIN
                INSERT INTO [dbo].[TechnologySiteData]([SiteDataId],[TechnologyId],[Version]) VALUES(@siteDataId, @technologyId, @version)
            END
            FETCH NEXT FROM siteDataCursor INTO @technologySiteDataId, @siteIdResult, @technologyId, @version
        END;

        CLOSE siteDataCursor;
        DEALLOCATE siteDataCursor;


        DECLARE @siteResponsiveDataId int
        DECLARE @alert tinyint
        DECLARE @docWidth tinyint
        DECLARE @flash tinyint
        DECLARE @fontSize tinyint
        DECLARE @touchScreen tinyint
        DECLARE @viewPort tinyint
        DECLARE @screenShot nvarchar(200)

        DECLARE responsiveDataCursor CURSOR FORWARD_ONLY FOR (SELECT siteDataId, alert, docWidth, flash, fontSize, touchScreen, viewPort, screenShot FROM @siteResponsiveList)
        OPEN responsiveDataCursor
        FETCH NEXT FROM responsiveDataCursor INTO @siteResponsiveDataId, @alert, @docWidth, @flash, @fontSize, @touchScreen, @viewPort, @screenShot
        
        WHILE @@FETCH_STATUS = 0 BEGIN
            IF @urlResultId=@siteResponsiveDataId
            BEGIN
                INSERT INTO [dbo].[SiteResponsive]([SiteDataId],[Alert],[DocWidth],[Flash],[FontSize],[TouchScreen],[ViewPort],[ScreenShot]) VALUES(@siteDataId, @alert, @docWidth, @flash, @fontSize, @touchScreen, @viewPort, @screenShot)
            END
            FETCH NEXT FROM responsiveDataCursor INTO @siteResponsiveDataId, @alert, @docWidth, @flash, @fontSize, @touchScreen, @viewPort, @screenShot
        END;

        CLOSE responsiveDataCursor;
        DEALLOCATE responsiveDataCursor;

        SELECT @urlId = NULL

        FETCH NEXT FROM dataCursor INTO @urlResultId, @taskId, @paginatorId, @siteDataIdResult, @urlLinkResult, @status, @urlIdResult, @isMatch, @countUrl
    END;
    
    CLOSE dataCursor;
    DEALLOCATE dataCursor;
END

GO
CREATE PROCEDURE [dbo].[SearchSite] 
	@techFilterList dbo.TechFilter READONLY,
	@taskIdList dbo.TaskFilter READONLY,
	@urlList dbo.UrlFilter READONLY,
	@countUrlStart INT = NULL,
	@countUrlEnd INT = NULL,
	@checkDateStart DATETIME = NULL,
	@checkDateEnd DATETIME = NULL,
	@alertStatus TINYINT = NULL,
	@docWidth TINYINT = NULL,
	@flash TINYINT = NULL,
	@fontSize TINYINT = NULL,
	@touchScreen TINYINT = NULL,
	@viewPort TINYINT = NULL,
	@limit INT = NULL,
	@offset INT = 0
AS
BEGIN
    IF @limit IS NULL
        SELECT
            @limit = COUNT(*)
        FROM [dbo].[UrlResult]
    DECLARE @count INT
    ;WITH [CTEUrlResult]
    AS
    (SELECT
            tbl.UrlResultId
           ,tbl.TaskId
           ,tbl.PaginatorId
           ,tbl.SiteDataId
           ,tbl.UrlLink
           ,tbl.UrlId
           ,tbl.IsMatch
           ,tbl.CountUrl
           ,tbl.[Status]
           ,[Check]
           ,COUNT(*) OVER () AS [Count]
           ,[Alert]
           ,[DocWidth]
           ,[Flash]
           ,[FontSize]
           ,[TouchScreen]
           ,[ViewPort]
        FROM (SELECT
                [UrlResult].UrlResultId
               ,[UrlResult].TaskId
               ,[UrlResult].PaginatorId
               ,[UrlResult].SiteDataId
               ,[UrlResult].UrlLink
               ,[UrlResult].UrlId
               ,[UrlResult].IsMatch
               ,[UrlResult].CountUrl
               ,[UrlResult].[Status]
               ,[SiteData].[Check]
               ,[TechnologySiteData].[Version]
               ,[TechnologySiteData].TechnologyId AS tech
               ,[Alert]
               ,[DocWidth]
               ,[Flash]
               ,[FontSize]
               ,[TouchScreen]
               ,[ViewPort]
            FROM [UrlResult]
            FULL JOIN [SiteData] ON [UrlResult].SiteDataId = [SiteData].SiteDataId
            FULL JOIN [TechnologySiteData] ON [UrlResult].SiteDataId = [TechnologySiteData].SiteDataId
            FULL JOIN [SiteResponsive] ON [SiteData].SiteDataId = [SiteResponsive].SiteDataId) AS tbl
        WHERE ((tech IN (SELECT technologyId FROM @techFilterList)
        AND (([Version] IN (SELECT Value FROM (SELECT * FROM @techFilterList WHERE technologyId = tech) t OUTER APPLY OPENJSON(t.[Version])))
        OR (SELECT COUNT(*) FROM (SELECT * FROM @techFilterList) t
            OUTER APPLY OPENJSON(t.[Version])
            WHERE technologyId = tech
            AND Value IS NOT NULL) = 0))
        OR (SELECT COUNT(*) FROM @techFilterList) = 0)
        GROUP BY tbl.UrlResultId
                ,tbl.TaskId
                ,tbl.PaginatorId
                ,tbl.SiteDataId
                ,tbl.UrlLink
                ,tbl.UrlId
                ,tbl.IsMatch
                ,tbl.CountUrl
                ,tbl.[Status]
                ,[Check]
                ,[Alert]
                ,[DocWidth]
                ,[Flash]
                ,[FontSize]
                ,[TouchScreen]
                ,[ViewPort]
        HAVING (COUNT(tbl.UrlResultId) = (SELECT COUNT(*) FROM @techFilterList) OR (SELECT COUNT(*) FROM @techFilterList) = 0)
        AND (((@countUrlStart <= CountUrl) OR @countUrlStart IS NULL) AND ((@countUrlEnd >= CountUrl) OR @countUrlEnd IS NULL))
        AND (((@checkDateStart <= [Check]) OR @checkDateStart IS NULL) AND (([Check] <= @checkDateEnd) OR @checkDateEnd IS NULL))
        AND ((UrlLink IN (SELECT [url] FROM @urlList)) OR (SELECT COUNT(*) FROM @urlList) = 0)
        AND ((TaskId IN (SELECT taskId FROM @taskIdList)) OR (SELECT COUNT(*) FROM @taskIdList) = 0)
        AND (Alert = @alertStatus
        OR @alertStatus IS NULL)
        AND (DocWidth = @docWidth
        OR @docWidth IS NULL)
        AND (Flash = @flash
        OR @flash IS NULL)
        AND (FontSize = @fontSize
        OR @fontSize IS NULL)
        AND (TouchScreen = @touchScreen
        OR @touchScreen IS NULL)
        AND (ViewPort = @viewPort
        OR @viewPort IS NULL))
    SELECT
        UrlResultId
       ,TaskId
       ,PaginatorId
       ,SiteDataId
       ,UrlLink
       ,UrlId
       ,IsMatch
       ,CountUrl
       ,[Status]
       ,[count]
    FROM [CTEUrlResult]
    ORDER BY [Check] OFFSET @offset ROW FETCH NEXT @limit ROWS ONLY
END;
GO