﻿namespace UrlScraper
{
    using System;
    using System.Collections.Concurrent;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Drawing.Imaging;
    using System.IO;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;


    using AutoMapper;

    using Contract.DataAccessLayer.Models;
    using Contract.Infrastructure.DataContracts.Network.Device;
    using Contract.Infrastructure.DataContracts.Platform;
    using Contract.Infrastructure.DataContracts.Proxy;
    using Contract.Infrastructure.DataContracts.SiteAnalyzer;
    using Contract.Infrastructure.DataContracts.Sprint;
    using Contract.Infrastructure.Factories;
    using Contract.Infrastructure.Services;

    using Microsoft.Extensions.Logging;
    using Microsoft.Extensions.Options;

    using Newtonsoft.Json;

    using Sales;
    using Sales.Models;

    using Size = Contract.Infrastructure.DataContracts.Network.Device.Size;
    using TaskStatus = Contract.Infrastructure.DataContracts.Sprint.TaskStatus;
    using System.Collections.Async;

    public class Scraper
    {
        private ISiteAnalyzerService analyzerService;

        private INetworkService networkService;

        private IDbWebService<PaginatorModel> paginatorService;

        private IDbWebService<TaskModel> taskService;

        private IPlatformFactory platformFactory;

        private IDbWebService<TechnologyModel> techService;

        private IUrlResultService resultService;

        private IProxyService proxyService;

        private readonly ILogger logger;

        private UrlScraperSettings settings;

        private IMapper mapper;

        private SemaphoreSlim semaphore;

        private ConcurrentQueue<ProxyModel> proxiesForSiteScrapLow;

        private ConcurrentQueue<ProxyModel> proxiesForSiteScrapHigh;

        private ConcurrentBag<TechnologyModel> technologyModels;

        private Device device;

        public Scraper(
            IOptions<UrlScraperSettings> options,
            ISiteAnalyzerService analyzerService,
            INetworkService networkService,
            IDbWebService<PaginatorModel> paginatorService,
            IDbWebService<TaskModel> taskService,
            IPlatformFactory platformFactory,
            IDbWebService<TechnologyModel> techService,
            IUrlResultService resultService,
            IProxyService proxyService,
            ILoggerFactory logger,
            IMapper mapper)
        {
            this.analyzerService = analyzerService;
            this.settings = options.Value;
            this.networkService = networkService;
            this.paginatorService = paginatorService;
            this.taskService = taskService;
            this.platformFactory = platformFactory;
            this.techService = techService;
            this.resultService = resultService;
            this.proxyService = proxyService;
            this.logger = logger.CreateLogger<NLog.ILogger>();
            this.mapper = mapper;

            this.proxiesForSiteScrapLow = new ConcurrentQueue<ProxyModel>();
            this.proxiesForSiteScrapHigh = new ConcurrentQueue<ProxyModel>();

            this.semaphore = new SemaphoreSlim(this.settings.CountScrapeSiteThread);

            this.device = Device.MobileAndroidChrome;
            this.device.Size = Size.Mobile;
        }


        public async Task Scrap()
        {
            var tasks = this.taskService.Get(
                    model => model.TaskStatus == TaskStatus.New,
                    model => model.TechnologyFilterModels)
                .ToList();

            // Get proxy from DB
            var proxiesFromDb =
                this.proxyService.GetAll(
                        new ProxyFilterModel()
                            {
                                ProxyType =
                                    new List<ProxyAnonymity>()
                                        {
                                            ProxyAnonymity.High,
                                            ProxyAnonymity.Low,
                                            ProxyAnonymity.None
                                        },
                                limit = this.settings.CountProxyForScrapeSearchPlatform
                            });

            this.technologyModels = new ConcurrentBag<TechnologyModel>(this.techService.GetAll());

            foreach (var task in tasks)
            {
                // Check list proxy for working
                ConcurrentQueue<ProxyModel> proxiesForScrape = await this.GetCheckedProxy(proxiesFromDb.Where(model => model.IsHttpsAllowed && model.ProxyAnonymity == ProxyAnonymity.High));
                this.proxiesForSiteScrapLow = await this.GetCheckedProxy(
                                                  proxiesFromDb.Where(
                                                      model => (model.ProxyAnonymity == ProxyAnonymity.Low
                                                                || model.ProxyAnonymity == ProxyAnonymity.None) && !model.IsHttpsAllowed));

                this.proxiesForSiteScrapHigh = await this.GetCheckedProxy(
                                                   proxiesFromDb.Where(
                                                       model => (model.ProxyAnonymity == ProxyAnonymity.Low
                                                                 || model.ProxyAnonymity == ProxyAnonymity.High) && model.IsHttpsAllowed));

                task.TaskStatus = TaskStatus.InProgress;
                this.taskService.Update(task);

                SearchPlatformMetaData platformMetaData = null;

                ISearchPlatformParserService platformParserService =
                    this.platformFactory.CreatePlatformParserService(task.PlatformType);
                BaseFilterModel filters = this.platformFactory.ParseFilterModel(task.PlatformType, task.FiltersJson);

                if (platformParserService != null && filters != null)
                {
                    foreach (var proxy in proxiesForScrape)
                    {
                        platformMetaData = platformParserService.Parse(
                            filters,
                            new Proxy() { Ip = proxy.ProxyIp, Port = proxy.ProxyPort });
                        if (platformMetaData.IsSuccesses)
                        {
                            break;
                        }
                    }
                    
                    if (platformMetaData != null && platformMetaData.IsSuccesses)
                    {
                        for (int i = 0; i < platformMetaData.ScanResultModels.Count; i++)
                        {
                            var paginator = new PaginatorModel()
                                                {
                                                    TaskId = task.TaskId,
                                                    PaginatorUrl =
                                                        platformMetaData.ScanResultModels[i].UrlPage
                                                };
                            this.paginatorService.Create(paginator);

                            var listUrl = platformMetaData.ScanResultModels[i].Urls.ToList();

                            var list = listUrl.Select(
                                task1 =>
                                    Task.Run(() => {
                                        UrlResultModel site = null;

                                        try
                                        {
                                            site = this.ScanSite(task1, task, paginator).Result;
                                            if (site != null)
                                            {
                                                this.resultService.Create(new List<UrlResultModel>() { site });
                                            }
                                        }
                                        catch(Exception e)
                                        {
                                            // ignored
                                        }
                                    }));

                            Task.WaitAll(list.ToArray());
                        }

                        task.TaskStatus = TaskStatus.Executed;
                        this.taskService.Update(task);
                    }
                    else
                    {
                        this.logger.LogError(
                            LoggingEvents.GET_ALL_EXCEPTION,
                            $"Cannot get urls, any urlResult not created");

                        task.TaskStatus = TaskStatus.Failed;
                        this.taskService.Update(task);
                    }
                }
                else
                {
                    this.logger.LogError(
                        LoggingEvents.GET_ALL_EXCEPTION,
                        $"Cannot create platform parser service with id {task.TargetId}");

                    task.TaskStatus = TaskStatus.Failed;
                    this.taskService.Update(task);
                }
            }
        }

        private async Task<UrlResultModel> ScanSite(string url, TaskModel task, PaginatorModel paginator)
        {
            await this.semaphore.WaitAsync();

            SiteMetaData technologies = null;
            SiteResponsiveModel responsive = null;


            for (int i = 0; i < this.settings.CountTryToScrapeSite; i++)
            {
                this.proxiesForSiteScrapLow.TryDequeue(out ProxyModel proxy);

                var taskTest = this.networkService.GetSiteMetaDataAll(url, this.device, new Proxy() { Ip = proxy.ProxyIp, Port = proxy.ProxyPort });
                try
                {
                    technologies = await taskTest;
                }
                catch
                {
                    // ignored
                }

                // Return proxy to end of queue
                this.proxiesForSiteScrapLow.Enqueue(proxy);


                if (technologies?.Status == SiteStatus.Available)
                {
                    this.analyzerService.AnalyzeAll(technologies);
                    break;
                }
            }

            if (technologies == null || technologies.Status != SiteStatus.Available)
            {
                for (int i = 0; i < this.settings.CountTryToScrapeSite; i++)
                {
                    this.proxiesForSiteScrapHigh.TryDequeue(out ProxyModel proxy);

                    try
                    {
                        technologies = await this.networkService.GetSiteMetaDataAll(url, this.device, new Proxy() { Ip = proxy.ProxyIp, Port = proxy.ProxyPort });
                    }
                    catch
                    {
                        // ignored
                    }

                    // Return proxy to end of queue
                    this.proxiesForSiteScrapHigh.Enqueue(proxy);


                    if (technologies?.Status == SiteStatus.Available)
                    {
                        this.analyzerService.AnalyzeAll(technologies);
                        break;
                    }
                }
            }


            if (technologies != null && technologies.Status != SiteStatus.NotAvailable)
            {
                var fileName = DateTime.Now.ToFileTimeUtc() + ".png";

                using (var stream = new MemoryStream(technologies.SiteMobileMetaData.ImageScreenShot))
                using (var image = Image.FromStream(stream, false, true))
                {
                    image.Save(this.settings.ScreenShotPath + fileName, ImageFormat.Png);
                }

                responsive = this.mapper.Map<SiteResponsiveModel>(technologies.SiteMobileMetaData);
                responsive.ScreenShot = fileName;
            }


            bool isMatch = true;
            var listTech = new ConcurrentBag<TechnologySiteDataModel>();
            if (technologies != null && technologies.Status != SiteStatus.NotAvailable)
            {
                foreach (var appFull in task.TechnologyFilterModels)
                {
                    appFull.TechnologyModel = this.techService.GetById(appFull.TechnologyId ?? 0);
                    var tech =
                        technologies.AppFulls.FirstOrDefault(
                            full => full.Name.Equals(appFull?.TechnologyModel?.TechnologyName));
                    if (tech != null)
                    {
                        var version = JsonConvert.DeserializeObject<IEnumerable<string>>(appFull.VersionJson).ToList();
                        if (version.Count > 0)
                        {
                            if (version.Any(x => !x.Equals(tech.Version)))
                            {
                                isMatch = false;
                                break;
                            }
                        }
                    }
                    else
                    {
                        isMatch = false;
                        break;
                    }
                }

                foreach (var item in technologies.AppFulls)
                {
                    var id = this.technologyModels.FirstOrDefault(model => model.TechnologyName == item.Name)
                        ?.TechnologyId;

                    listTech.Add(new TechnologySiteDataModel() { Version = item.Version, TechnologyId = id });
                }
            }
            else
            {
                isMatch = false;
            }

            this.semaphore.Release();

            return new UrlResultModel()
                        {
                            TaskId = task.TaskId,
                            PaginatorId = paginator.PaginatorId,
                            UrlLink = url,
                            IsMatch = isMatch,
                            TechnologySiteDataModels = listTech.ToList(),
                            SiteStatus = technologies?.Status ?? SiteStatus.NotAvailable,
                            SiteResponsive = new List<SiteResponsiveModel>() { responsive },
                            CountUrl = technologies?.Urls.Count
                        };
        }

        private async Task<ConcurrentQueue<ProxyModel>> GetCheckedProxy(IEnumerable<ProxyModel> proxies)
        {
            ConcurrentQueue<ProxyModel> proxiesForScrape = new ConcurrentQueue<ProxyModel>();

            var list = proxies.Select(
                model => Task.Run(
                    () =>
                        {
                            var proxy = this.mapper.Map<Proxy>(model);

                            if (this.networkService.IsProxyAvailable(proxy).Result)
                            {
                                proxiesForScrape.Enqueue(model);
                            }
                        }));

            Task.WaitAll(list.ToArray());

            return proxiesForScrape;
        }
    }
}