﻿namespace UrlScraper
{
    using System.IO;
    using System.Threading.Tasks;

    using Contract.DataAccessLayer;
    using Contract.DataAccessLayer.EF;
    using Contract.DataAccessLayer.Interfaces;
    using Contract.DataAccessLayer.Models;
    using Contract.DataAccessLayer.Repository;
    using Contract.Infrastructure.DataContracts.SiteAnalyzer;
    using Contract.Infrastructure.DataContracts.Sprint;
    using Contract.Infrastructure.Factories;
    using Contract.Infrastructure.Repositories;
    using Contract.Infrastructure.Services;
    using Contract.Services.Database;
    using Contract.Services.Factories;

    using Microsoft.AspNetCore.Builder;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Routing;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;
    using Microsoft.Extensions.Logging;

    using NLog;
    using NLog.Extensions.Logging;
    using NLog.Web;

    using Sales;
    using Sales.Models;
    using Sales.Services;

    using ServiceStack;

    public class Startup
    {
        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddJsonFile("hosting.json", optional: true)
                .AddEnvironmentVariables();

            this.Configuration = builder.Build();
        }

        public IConfigurationRoot Configuration { get; set; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddLogging();
            services.AddOptions();
            services.AddRouting();

            services.AddTransient<ILoggerFactory>(provider => new LoggerFactory().AddNLog());

            services.Configure<UrlScraperSettings>(
                options => this.Configuration.GetSection("UrlScraperSettings").Bind(options));

            services.Configure<DataDotComSettings>(
                options => this.Configuration.GetSection("DataDotComSettings").Bind(options));

            services.Configure<NetworkSettings>(
                options => this.Configuration.GetSection("NetworkSettings").Bind(options));

            services.Configure<SiteAnalyzerSettings>(
                options => this.Configuration.GetSection("SiteAnalyzerSettings").Bind(options));

            services.Configure<GoogleSettings>(
                options => this.Configuration.GetSection("GoogleSettings").Bind(options));

            services.AddTransient<BaseDataAccess>(
                provider => new BaseDataAccess(this.Configuration.GetConnectionString("DefaultConnection")));

            services.AddTransient<IUrlResultRepository, UrlResultRepository>();

            services.AddTransient<IUrlResultService, UrlResultService>();

            services.AddDbContext<WebServiceContext>(
                builder => builder.UseSqlServer(this.Configuration.GetConnectionString("DefaultConnection")));

            services.AddTransient<IRepository<UrlModel>, EFGenericRepository<UrlModel>>();
            services.AddTransient<IDbWebService<UrlModel>, DbWebService<UrlModel>>();

            services.AddTransient<IRepository<TaskModel>, EFGenericRepository<TaskModel>>();
            services.AddTransient<IDbWebService<TaskModel>, DbWebService<TaskModel>>();

            services.AddTransient<IRepository<SiteDataModel>, EFGenericRepository<SiteDataModel>>();
            services.AddTransient<IDbWebService<SiteDataModel>, DbWebService<SiteDataModel>>();

            services.AddTransient<IRepository<PaginatorModel>, EFGenericRepository<PaginatorModel>>();
            services.AddTransient<IDbWebService<PaginatorModel>, DbWebService<PaginatorModel>>();

            services.AddTransient<IRepository<UrlResultModel>, EFGenericRepository<UrlResultModel>>();
            services.AddTransient<IDbWebService<UrlResultModel>, DbWebService<UrlResultModel>>();

            services.AddTransient<IRepository<TechnologySiteDataModel>, EFGenericRepository<TechnologySiteDataModel>>();
            services.AddTransient<IDbWebService<TechnologySiteDataModel>, DbWebService<TechnologySiteDataModel>>();

            services.AddTransient<IRepository<TechnologyModel>, EFGenericRepository<TechnologyModel>>();
            services.AddTransient<IDbWebService<TechnologyModel>, DbWebService<TechnologyModel>>();

            services.AddSingleton<ISiteAnalyzerService, SiteAnalyzerService>();

            services.AddTransient<INetworkService, NetworkService>();

            services.AddTransient<IUnitOfWork>(
                provider => new EFUnitOfWork(this.Configuration.GetConnectionString("DefaultConnection")));

            services.AddTransient<IProxyService, ProxyService>();

            services.AddSingleton<IPlatformFactory, PlatformFactory>();

            var config = new AutoMapper.MapperConfiguration(
                cfg =>
                    {
                        cfg.CreateMap<SiteResponsiveModel, SiteMobileMetaData>();
                        cfg.CreateMap<SiteMobileMetaData, SiteResponsiveModel>();

                        cfg.CreateMap<Proxy, ProxyModel>()
                            .ForMember(model => model.ProxyIp, expression => expression.MapFrom(proxy => proxy.Ip))
                            .ForMember(model => model.ProxyPort, expression => expression.MapFrom(proxy => proxy.Port));
                        cfg.CreateMap<ProxyModel, Proxy>()
                            .ForMember(proxy => proxy.Ip, expression => expression.MapFrom(model => model.ProxyIp))
                            .ForMember(proxy => proxy.Port, expression => expression.MapFrom(model => model.ProxyPort));
                    });
            var mapper = config.CreateMapper();
            services.AddSingleton(mapper);

            services.AddScoped<Scraper>();
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {

            loggerFactory.AddNLog();
#if RELEASE
            loggerFactory.ConfigureNLog("nlog.config");
#elif (DEBUG)
            loggerFactory.ConfigureNLog("nlog.Development.config");
#endif

            LogManager.Configuration.Variables["connectionString"] =
                this.Configuration.GetConnectionString("DefaultConnection");
            var routeBuilder = new RouteBuilder(app);

            routeBuilder.MapGet(
                "Scraper/scrapeWebsite",
                delegate(HttpContext context)
                    {
                        Task.Run(() => app.ApplicationServices.GetService<Scraper>().Scrap());
                        context.Response.StatusCode = 200;
                        return Task.FromResult(context);
                    });

            app.UseRouter(routeBuilder.Build());
        }
    }
}