﻿namespace UrlScraper
{
    using System.IO;

    using Microsoft.AspNetCore.Hosting;

    public class Program
    {
        static void Main(string[] args)
        {
            var host = new WebHostBuilder()
                .UseKestrel()
                .UseIISIntegration()
                .UseContentRoot(Directory.GetCurrentDirectory())
                .UseStartup<Startup>()
                .UseUrls("http://localhost:37932")
                .Build();

            host.Run();
        }
    }
}