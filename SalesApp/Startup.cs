﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace SalesApp
{
    using Contract.DataAccessLayer;
    using Contract.DataAccessLayer.EF;
    using Contract.DataAccessLayer.Interfaces;
    using Contract.DataAccessLayer.Repository;
    using Contract.Infrastructure.DataContracts.Sprint;
    using Contract.Infrastructure.Repositories;
    using Contract.Infrastructure.Services;
    using Contract.Services.Database;

    using Microsoft.EntityFrameworkCore;

    using NLog;
    using NLog.Extensions.Logging;

    using SalesApp.Helpers;
    using SalesApp.Models;

    public class Startup
    {
        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();

            this.Configuration = builder.Build();
        }

        public IConfigurationRoot Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddLogging();
            services.AddTransient<ILoggerFactory>(provider => new LoggerFactory().AddNLog());
            services.AddDbContext<WebServiceContext>(builder => builder.UseSqlServer(this.Configuration.GetConnectionString("DefaultConnection")));

            services.AddTransient<BaseDataAccess>(
                provider => new BaseDataAccess(this.Configuration.GetConnectionString("DefaultConnection")));

            services.AddTransient<IUrlResultRepository, UrlResultRepository>();

            services.AddTransient<IUrlResultService, UrlResultService>();

            services.AddTransient<IRepository<TechnologySiteDataModel>, EFGenericRepository<TechnologySiteDataModel>>();
            services.AddTransient<IDbWebService<TechnologySiteDataModel>, DbWebService<TechnologySiteDataModel>>();

            services.AddTransient<IRepository<TechnologyFilterModel>, EFGenericRepository<TechnologyFilterModel>>();
            services.AddTransient<IDbWebService<TechnologyFilterModel>, DbWebService<TechnologyFilterModel>>();

            services.AddTransient<IRepository<TechnologyModel>, EFGenericRepository<TechnologyModel>>();
            services.AddTransient<IDbWebService<TechnologyModel>, DbWebService<TechnologyModel>>();

            services.AddTransient<IRepository<SiteDataModel>, EFGenericRepository<SiteDataModel>>();
            services.AddTransient<IDbWebService<SiteDataModel>, DbWebService<SiteDataModel>>();

            services.AddTransient<IRepository<UrlResultModel>, EFGenericRepository<UrlResultModel>>();
            services.AddTransient<IDbWebService<UrlResultModel>, DbWebService<UrlResultModel>>();

            services.AddTransient<IRepository<UrlModel>, EFGenericRepository<UrlModel>>();
            services.AddTransient<IDbWebService<UrlModel>, DbWebService<UrlModel>>();

            services.AddTransient<IRepository<PaginatorModel>, EFGenericRepository<PaginatorModel>>();
            services.AddTransient<IDbWebService<PaginatorModel>, DbWebService<PaginatorModel>>();

            services.AddTransient<IRepository<TaskModel>, EFGenericRepository<TaskModel>>();
            services.AddTransient<IDbWebService<TaskModel>, DbWebService<TaskModel>>();
            
            services.AddTransient<IRepository<SprintModel>, EFGenericRepository<SprintModel>>();
            services.AddTransient<IDbWebService<SprintModel>, DbWebService<SprintModel>>();

            services.AddTransient<IRepository<SiteResponsiveModel>, EFGenericRepository<SiteResponsiveModel>>();
            services.AddTransient<IDbWebService<SiteResponsiveModel>, DbWebService<SiteResponsiveModel>>();

            var config = new AutoMapper.MapperConfiguration(
                cfg =>
                    {
                        cfg.CreateMap<SiteResponsiveModel, SiteResponsiveViewModel>();
                        cfg.CreateMap<SiteResponsiveViewModel, SiteResponsiveModel>();

                        cfg.CreateMap<TechnologySiteDataModel, TechnologySiteDataViewModel>();
                        cfg.CreateMap<TechnologySiteDataViewModel, TechnologySiteDataModel>();

                        cfg.CreateMap<SiteDataModel, SiteDataViewModel>();
                        cfg.CreateMap<SiteDataViewModel, SiteDataModel>();

                        cfg.CreateMap<UrlResultModel, UrlResultViewModel>();
                        cfg.CreateMap<UrlResultViewModel, UrlResultModel>();

                        cfg.CreateMap<UrlModel, UrlViewModel>();
                        cfg.CreateMap<UrlViewModel, UrlModel>();

                        cfg.CreateMap<PaginatorModel, PaginatorViewModel>();
                        cfg.CreateMap<PaginatorViewModel, PaginatorModel>();

                        cfg.CreateMap<TechnologyFilterViewModel, TechnologyFilterModel>().MaxDepth(1);
                        cfg.CreateMap<TechnologyFilterModel, TechnologyFilterViewModel>().MaxDepth(1);

                        cfg.CreateMap<TechnologyModel, TechnologyViewModel>().MaxDepth(1);
                        cfg.CreateMap<TechnologyViewModel, TechnologyModel>().MaxDepth(1);

                        cfg.CreateMap<TechnologyFilterModel, TechnologyFilterViewModel>().MaxDepth(1);
                        cfg.CreateMap<TechnologyFilterViewModel, TechnologyFilterModel>().MaxDepth(1);

                        cfg.CreateMap<CreateEditTaskViewModel, TaskViewModel>().MaxDepth(1);
                        cfg.CreateMap<TaskViewModel, CreateEditTaskViewModel>().MaxDepth(1);

                        cfg.CreateMap<CreateEditTaskViewModel, TaskModel>().MaxDepth(1);
                        cfg.CreateMap<TaskModel, CreateEditTaskViewModel>().MaxDepth(1);

                        cfg.CreateMap<TaskModel, TaskViewModel>().MaxDepth(1);
                        cfg.CreateMap<TaskViewModel, TaskModel>().MaxDepth(1);

                        cfg.CreateMap<SprintModel, SprintViewModel>().ForMember(model => model.TaskModels, expression => expression.MapFrom(model => model.TaskModels));
                        cfg.CreateMap<SprintViewModel, SprintModel>().ForMember(model => model.TaskModels, expression => expression.MapFrom(model => model.TaskModels));

                        cfg.CreateMap<SprintModel, CreateEditSprintViewModel>()
                            .ForMember(
                                model => model.From,
                                expression => expression.MapFrom(
                                    sprint => sprint.From.HasValue
                                                 ? sprint.From.Value.ToString("dd/MM/yyyy")
                                                 : string.Empty))
                            .ForMember(
                                model => model.To,
                                expression => expression.MapFrom(
                                    sprint => sprint.To.HasValue
                                                 ? sprint.To.Value.ToString("dd/MM/yyyy")
                                                 : string.Empty));
                        cfg.CreateMap<CreateEditSprintViewModel, SprintModel>()
                            .ForMember(
                                model => model.From,
                                expression => expression.MapFrom(sprint => DateHelper.ParseDate(sprint.From)))
                            .ForMember(
                                model => model.To,
                                expression => expression.MapFrom(sprint => DateHelper.ParseDate(sprint.To)));


                        cfg.CreateMap<SprintViewModel, CreateEditSprintViewModel>()
                            .ForMember(
                                model => model.From,
                                expression => expression.MapFrom(
                                    sprint => sprint.From.HasValue
                                                 ? sprint.From.Value.ToString("dd/MM/yyyy")
                                                 : string.Empty))
                            .ForMember(
                                model => model.To,
                                expression => expression.MapFrom(
                                    sprint => sprint.To.HasValue
                                                 ? sprint.To.Value.ToString("dd/MM/yyyy")
                                                 : string.Empty));
                        cfg.CreateMap<CreateEditSprintViewModel, SprintViewModel>()
                            .ForMember(
                                model => model.From,
                                expression => expression.MapFrom(sprint => DateHelper.ParseDate(sprint.From)))
                            .ForMember(
                                model => model.To,
                                expression => expression.MapFrom(sprint => DateHelper.ParseDate(sprint.To)));

                        cfg.CreateMap<UrlResultFilterModel, UrlResultFilterViewModel>();
                        cfg.CreateMap<UrlResultFilterViewModel, UrlResultFilterModel>();
                    });

            var mapper = config.CreateMapper();
            services.AddSingleton(mapper);

            services.AddMvc();
        }

        public void Configure(ILoggerFactory loggerFactory, IApplicationBuilder app, IHostingEnvironment env)
        {
            loggerFactory.AddNLog();
#if RELEASE
            loggerFactory.ConfigureNLog("nlog.config");
#elif (DEBUG)
            loggerFactory.ConfigureNLog("nlog.Development.config");
#endif

            LogManager.Configuration.Variables["connectionString"] =
                this.Configuration.GetConnectionString("DefaultConnection");

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseBrowserLink();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            app.UseStaticFiles();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
