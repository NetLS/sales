using System.Collections.Generic;

using Microsoft.AspNetCore.Mvc;

namespace SalesApp.Controllers
{
    using System.Linq;

    using AutoMapper;

    using Contract.DataAccessLayer.Models;
    using Contract.Infrastructure.DataContracts.Sprint;
    using Contract.Infrastructure.Services;

    using SalesApp.Models;

    public class SiteController : Controller
    {
        private IMapper mapper;

        private IDbWebService<UrlResultModel> urlResultService;
        private IDbWebService<UrlModel> urlService;
        private IDbWebService<SiteDataModel> siteDataService;
        private IDbWebService<TechnologySiteDataModel> techSiteDataService;
        private IDbWebService<TechnologyModel> techService;
        private IDbWebService<TaskModel> taskService;
        private IDbWebService<PaginatorModel> paginatorService;
        private IDbWebService<SiteResponsiveModel> responsiveService;

        private IUrlResultService urlResultServiceCustom;

        public SiteController(IMapper mapper, IDbWebService<UrlResultModel> urlResultService, IDbWebService<UrlModel> urlService, IDbWebService<SiteDataModel> siteDataService, IDbWebService<TechnologySiteDataModel> techSiteDataService, IDbWebService<TechnologyModel> techService, IDbWebService<TaskModel> taskService, IDbWebService<PaginatorModel> paginatorService, IDbWebService<SiteResponsiveModel> responsiveService, IUrlResultService urlResultServiceCustom)
        {
            this.mapper = mapper;
            this.urlResultService = urlResultService;
            this.urlService = urlService;
            this.siteDataService = siteDataService;
            this.techSiteDataService = techSiteDataService;
            this.techService = techService;
            this.taskService = taskService;
            this.paginatorService = paginatorService;
            this.responsiveService = responsiveService;
            this.urlResultServiceCustom = urlResultServiceCustom;
        }

        // GET: Site
        public ActionResult Index(UrlResultFilterViewModel model = null, int page = 1)
        {
            this.ViewData["Title"] = "Site";
            this.ViewData["page"] = page;
            if (model == null)
            {
                model = new UrlResultFilterViewModel();
            }

            int count = 0;

            model.limit = model.limit > 0 ? model.limit : 10;

            model.TaskViewModels = this.mapper.Map<List<TaskViewModel>>(this.taskService.GetAll());
            model.offset = (page - 1) * model.limit;

            model.TechnologyViewModels = this.mapper.Map<List<TechnologyViewModel>>(this.techService.GetAll());
            
            var list = this.urlResultServiceCustom.Get(this.mapper.Map<UrlResultFilterModel>(model), out count);
            model.count = count;

            var listUrlResult = this.mapper.Map<List<UrlResultViewModel>>(list);

            foreach (var item in listUrlResult)
            {
                item.TaskViewModel = this.mapper.Map<TaskViewModel>(this.taskService.GetById(item.TaskId ?? 0));
                item.SiteDataModel =
                    this.mapper.Map<SiteDataViewModel>(this.siteDataService.GetById(item.SiteDataId ?? 0));
            }
            
            model.ResultViewModels = listUrlResult;

            return this.View(model);
        }

        // GET: Site/Details/5
        public ActionResult Details(int id)
        {
            var urlResult = this.mapper.Map<UrlResultViewModel>(this.urlResultService.GetById(id));
            this.ViewData["Title"] = urlResult.UrlLink + " - Site";

            urlResult.TaskViewModel = this.mapper.Map<TaskViewModel>(this.taskService.GetById(urlResult.TaskId ?? 0));

            urlResult.UrlViewModel = this.mapper.Map<UrlViewModel>(this.urlService.GetById(urlResult.UrlId ?? 0));

            urlResult.UrlViewModel.UrlResultViewModels = this.mapper.Map<List<UrlResultViewModel>>(
                this.urlResultService.Get(model => model.UrlId == urlResult.UrlViewModel.UrlId));

            urlResult.UrlViewModel.SiteDataModels = this.mapper.Map<List<SiteDataViewModel>>(
                this.siteDataService.Get(model => model.UrlId == urlResult.UrlViewModel.UrlId));

            urlResult.PaginatorViewModel =
                this.mapper.Map<PaginatorViewModel>(this.paginatorService.GetById(urlResult.PaginatorId ?? 0));

            foreach (var item in urlResult.UrlViewModel.UrlResultViewModels)
            {
                item.TaskViewModel = this.mapper.Map<TaskViewModel>(this.taskService.GetById(item.TaskId ?? 0));
                item.SiteDataModel = this.mapper.Map<SiteDataViewModel>(this.siteDataService.GetById(item.SiteDataId ?? 0));
            }

            foreach (var item in urlResult.UrlViewModel.SiteDataModels)
            {
                item.TechnologySiteDataModels = this.mapper.Map<List<TechnologySiteDataViewModel>>(this.techSiteDataService.Get(model => model.SiteDataId == item.SiteDataId));

                foreach (var tech in item.TechnologySiteDataModels)
                {
                    tech.TechnologyModel =
                        this.mapper.Map<TechnologyViewModel>(this.techService.GetById(tech.TechnologyId ?? 0));
                }
            }

            urlResult.SiteDataModel =
                this.mapper.Map<SiteDataViewModel>(this.siteDataService.GetById(urlResult.SiteDataId ?? 0));

            urlResult.SiteDataModel.ResponsiveViewModels = this.mapper.Map<List<SiteResponsiveViewModel>>(
                this.responsiveService.Get(model => model.SiteDataId == urlResult.SiteDataId));

            var index = urlResult.SiteDataId;
            var t = this.techSiteDataService.Get(model => model.SiteDataId == index);
            foreach (var tech in t)
            {
                tech.TechnologyModel = this.techService.GetById(tech.TechnologyId ?? 0);
            }

            urlResult.SiteDataModel.TechnologySiteDataModels = this.mapper.Map<List<TechnologySiteDataViewModel>>(t);

            return this.View(urlResult);
        }
    }
}