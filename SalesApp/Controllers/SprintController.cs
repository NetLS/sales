using System;
using System.Collections.Generic;
using System.Linq;

using Microsoft.AspNetCore.Mvc;

namespace SalesApp.Controllers
{
    using AutoMapper;

    using Contract.Infrastructure.DataContracts.Sprint;
    using Contract.Infrastructure.Services;

    using SalesApp.Models;

    public class SprintController : Controller
    {
        private IDbWebService<SprintModel> service;
        private IMapper mapper;

        private IDbWebService<TechnologyFilterModel> techFilterService;
        private IDbWebService<TechnologyModel> techService;

        public SprintController(IDbWebService<SprintModel> service, IDbWebService<TechnologyFilterModel> techFilterService, IDbWebService<TechnologyModel> techService, IMapper mapper)
        {
            this.techService = techService;
            this.techFilterService = techFilterService;

            this.service = service;
            this.mapper = mapper;
        }

        // GET: Sprint
        public ActionResult Index()
        {
            this.ViewData["Title"] = "Sprints";

            var list = this.mapper.Map<List<SprintViewModel>>(this.service.GetAll());

            return this.View(list);
        }

        // GET: Sprint/Details/5
        public ActionResult Details(int id)
        {
            var item = this.service.GetById(id, model => model.TaskModels);

            this.ViewData["Title"] = item.Name + " - Sprint";

            var sprint = this.mapper.Map<SprintViewModel>(item);
            for (var i = 0; i < sprint.TaskModels.Count; i++)
            {
                sprint.TaskModels[i].SprintModel =
                    this.mapper.Map<SprintViewModel>(this.service.GetById(sprint.SprintId));

                sprint.TaskModels[i].TechnologyFilterViewModels = this.mapper.Map<List<TechnologyFilterViewModel>>(
                    this.techFilterService.Get(model => model.TaskId == sprint.TaskModels[i].TaskId));

                sprint.TaskModels[i].TechnologyViewModels = this.mapper.Map<List<TechnologyViewModel>>(
                    this.techService.Get(
                        model => sprint.TaskModels[i]
                            .TechnologyFilterViewModels.Any(
                                viewModel => model.TechnologyId == viewModel.TechnologyId)));
            }

            return this.View(sprint);
        }

        // GET: Sprint/Create
        public ActionResult Create()
        {
            this.ViewData["Title"] = "Create new sprint";

            var model = new CreateEditSprintViewModel();
            model.From = DateTime.Now.ToString("dd/MM/yyyy");
            model.To = DateTime.Now.ToString("dd/MM/yyyy");

            return this.PartialView("~/Views/Sprint/Create.cshtml", model);
        }

        // POST: Sprint/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(CreateEditSprintViewModel model)
        {
            var response = new SprintResponse();

            if (this.ModelState.IsValid)
            {
                var item = this.mapper.Map<SprintModel>(model);
                if (this.service.Create(item))
                {
                    response.IsSuccess = true;
                    response.Title = "Success!";
                    response.Message = "Sprint created successful";
                    response.Model = this.mapper.Map<SprintViewModel>(item);
                }
                else
                {
                    response.IsSuccess = false;
                    response.Title = "Error!";
                    response.Message = "Something wrong, check log";
                }
            }
            else
            {
                response.IsSuccess = false;
                response.Title = "Error!";
                response.Message = string.Join("<br/>", this.ModelState.Keys.SelectMany(key => this.ModelState[key].Errors.Select(x => x.ErrorMessage)));
            }

            return this.Json(response);
        }

        // GET: Sprint/Edit/5
        public ActionResult Edit(int id)
        {
            this.ViewData["Title"] = "Edit sprint";

            var item = this.service.GetById(id);

            return this.PartialView("~/Views/Sprint/Edit.cshtml", this.mapper.Map<CreateEditSprintViewModel>(item));
        }

        // POST: Sprint/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(CreateEditSprintViewModel model)
        {
            var response = new SprintResponse();

            if (this.ModelState.IsValid)
            {
                var item = this.mapper.Map<SprintModel>(model);
                if (this.service.Update(item))
                {
                    response.IsSuccess = true;
                    response.Title = "Success!";
                    response.Message = "Sprint edit successful";
                    response.Model = this.mapper.Map<SprintViewModel>(item);
                }
                else
                {
                    response.Title = "Error!";
                    response.IsSuccess = false;
                    response.Message = "Something wrong, check log";
                }
            }
            else
            {
                response.Title = "Error!";
                response.IsSuccess = false;
                response.Message = string.Join("<br/>", this.ModelState.Keys.SelectMany(key => this.ModelState[key].Errors.Select(x => x.ErrorMessage)));
            }

            return this.Json(response);
        }

        // GET: Sprint/Delete/5
        [HttpGet]
        public ActionResult Delete(int id)
        {
            this.ViewData["Title"] = "Delete sprint";

            var item = this.service.GetById(id);

            return this.PartialView("~/Views/Sprint/Delete.cshtml", this.mapper.Map<CreateEditSprintViewModel>(item));
        }

        // POST: Sprint/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(CreateEditSprintViewModel model)
        {
            var response = new SprintResponse();

            var item = this.mapper.Map<SprintModel>(model);
            if (this.service.Delete(item.SprintId))
            {
                response.IsSuccess = true;
                response.Title = "Success!";
                response.Message = "Sprint delete successful";
                response.Model = this.mapper.Map<SprintViewModel>(item);
            }
            else
            {
                response.Title = "Error!";
                response.IsSuccess = false;
                response.Message = "Something wrong, check log";
            }

            return this.Json(response);
        }
    }
}