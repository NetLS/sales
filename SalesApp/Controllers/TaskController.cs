using System.Linq;

using Microsoft.AspNetCore.Mvc;

namespace SalesApp.Controllers
{
    using System.Collections.Generic;

    using AutoMapper;

    using Contract.Infrastructure.DataContracts.Sprint;
    using Contract.Infrastructure.Services;

    using SalesApp.Models;
    using SalesApp.Models.Response;

    public class TaskController : Controller
    {

        private IDbWebService<TaskModel> service;
        private IDbWebService<SprintModel> sprintService;
        private IDbWebService<TechnologyFilterModel> techFilterService;
        private IDbWebService<TechnologyModel> techService;
        private IDbWebService<UrlResultModel> urlResultService;
        private IDbWebService<SiteDataModel> siteDataService;

        private IUrlResultService urlResultServiceCustom;

        private IMapper mapper;

        public TaskController(IDbWebService<TaskModel> service, IDbWebService<SprintModel> sprintService, IDbWebService<TechnologyFilterModel> techFilterService, IDbWebService<TechnologyModel> techService, IDbWebService<UrlResultModel> urlResultService, IDbWebService<SiteDataModel> siteDataService, IUrlResultService urlResultServiceCustom, IMapper mapper)
        {
            this.service = service;
            this.sprintService = sprintService;
            this.techFilterService = techFilterService;
            this.techService = techService;
            this.urlResultService = urlResultService;
            this.siteDataService = siteDataService;
            this.urlResultServiceCustom = urlResultServiceCustom;
            this.mapper = mapper;
        }

        // GET: Task
        public ActionResult Index()
        {
            this.ViewData["Title"] = "Task";

            var list = this.mapper.Map<List<TaskViewModel>>(this.service.GetAll());
            for (var i = 0; i < list.Count; i++)
            {
                list[i].SprintModel =
                    this.mapper.Map<SprintViewModel>(this.sprintService.GetById(list[i].SprintId ?? 0));

                list[i].TechnologyFilterViewModels = this.mapper.Map<List<TechnologyFilterViewModel>>(
                    this.techFilterService.Get(model => model.TaskId == list[i].TaskId));

                list[i].TechnologyViewModels = this.mapper.Map<List<TechnologyViewModel>>(
                    this.techService.Get(
                        model => list[i]
                            .TechnologyFilterViewModels.Any(
                                viewModel => model.TechnologyId == viewModel.TechnologyId)));
            }

            return this.View(list);
        }

        // GET: Task/Details/5
        public ActionResult Details(int id, int page = 1)
        {
            var item = this.service.GetById(id, model => model.PaginatorModels);

            this.ViewData["Title"] = item.TaskName + " - Task";
            this.ViewData["page"] = page;

            var task = this.mapper.Map<TaskViewModel>(item);
            task.Limit = 50;

            task.TechnologyFilterViewModels =
                this.mapper.Map<List<TechnologyFilterViewModel>>(
                    this.techFilterService.Get(model => model.TaskId == task.TaskId));

            task.TechnologyViewModels = this.mapper.Map<List<TechnologyViewModel>>(
                this.techService.Get(
                    model => task.TechnologyFilterViewModels.Any(
                        viewModel => model.TechnologyId == viewModel.TechnologyId)));
            int count = 0;
            var listUrlResult = this.mapper.Map<List<UrlResultViewModel>>(
                this.urlResultServiceCustom.Get(
                    new UrlResultFilterModel()
                        {
                            limit = task.Limit,
                            offset = task.Limit * (page - 1),
                            TaskIdList = new List<int>() { task.TaskId }
                        },
                    out count));
            task.CountUrlResultsAll = count;

            foreach (var url in listUrlResult)
            {
                url.TaskViewModel = this.mapper.Map<TaskViewModel>(this.service.GetById(url.TaskId ?? 0));
                url.SiteDataModel =
                    this.mapper.Map<SiteDataViewModel>(this.siteDataService.GetById(url.SiteDataId ?? 0));
            }

            task.SprintModel = this.mapper.Map<SprintViewModel>(this.sprintService.GetById(task.SprintId ?? 0));
            task.UrlResultViewModels = listUrlResult;

            return this.View(task);
        }

        // GET: Task/Create
        public ActionResult Create(int id)
        {
            this.ViewData["Title"] = "Create new task";

            var model = new CreateEditTaskViewModel() { SprintId = id };
            model.SprintViewModels = this.mapper.Map<List<SprintViewModel>>(this.sprintService.GetAll());
            model.TechnologyViewModels = this.mapper.Map<List<TechnologyViewModel>>(this.techService.GetAll());

            return this.PartialView("~/Views/Task/Create.cshtml", model);
        }

        // POST: Task/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(CreateEditTaskViewModel model)
        {
            var response = new TaskResponse();

            if (this.ModelState.IsValid)
            {
                var item = this.mapper.Map<TaskModel>(model);
                if (this.service.Create(item))
                {
                    response.IsSuccess = true;
                    response.Title = "Success!";
                    response.Message = "Task created successful";

                    for (int i = 0; i < model.TechnologyFilterViewModels?.Count; i++)
                    {
                        model.TechnologyFilterViewModels[i].TaskId = item.TaskId;

                        this.techFilterService.Create(this.mapper.Map<TechnologyFilterModel>(model.TechnologyFilterViewModels[i]));
                    }


                    var task = this.mapper.Map<TaskViewModel>(item);
                    task.SprintModel = this.mapper.Map<SprintViewModel>(this.sprintService.GetById(task.SprintId ?? 0));
                    response.Model = task;
                }
                else
                {
                    response.IsSuccess = false;
                    response.Title = "Error!";
                    response.Message = "Something wrong, check log";
                }
            }
            else
            {
                response.IsSuccess = false;
                response.Title = "Error!";
                response.Message = string.Join("<br/>", this.ModelState.Keys.SelectMany(key => this.ModelState[key].Errors.Select(x => x.ErrorMessage)));
            }

            return this.Json(response);
        }

        // GET: Task/Edit/5
        public ActionResult Edit(int id)
        {
            this.ViewData["Title"] = "Edit task";

            var model = this.mapper.Map<CreateEditTaskViewModel>(this.service.GetById(id));
            model.SprintViewModels = this.mapper.Map<List<SprintViewModel>>(this.sprintService.GetAll());
            model.TechnologyViewModels = this.mapper.Map<List<TechnologyViewModel>>(this.techService.GetAll());
            model.TechnologyFilterViewModels = this.mapper.Map<List<TechnologyFilterViewModel>>(
                           this.techFilterService.Get(filterModel => filterModel.TaskId == model.TaskId));

            return this.PartialView("~/Views/Task/Edit.cshtml", model);
        }

        // POST: Task/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(CreateEditTaskViewModel model)
        {
            var response = new TaskResponse();

            if (this.ModelState.IsValid)
            {
                var item = this.mapper.Map<TaskModel>(model);
                if (this.service.Update(item))
                {
                    response.IsSuccess = true;
                    response.Title = "Success!";
                    response.Message = "Task edited successful";

                    var currentTechFilters = this.mapper.Map<List<TechnologyFilterViewModel>>(
                        this.techFilterService.Get(filterModel => filterModel.TaskId == item.TaskId));

                    foreach (var filter in currentTechFilters)
                    {
                        this.techFilterService.Delete(this.mapper.Map<TechnologyFilterModel>(filter).TechnologyFilterId);
                    }

                    for (int i = 0; i < model.TechnologyFilterViewModels?.Count; i++)
                    {
                        model.TechnologyFilterViewModels[i].TaskId = item.TaskId;
                        if (this.techFilterService.GetById(model.TechnologyFilterViewModels[i].TechnologyFilterId) == null)
                        {
                            this.techFilterService.Create(this.mapper.Map<TechnologyFilterModel>(model.TechnologyFilterViewModels[i]));
                        }
                        else
                        {
                            this.techFilterService.Update(this.mapper.Map<TechnologyFilterModel>(model.TechnologyFilterViewModels[i]));
                        }
                    }

                    var task = this.mapper.Map<TaskViewModel>(item);
                    task.SprintModel = this.mapper.Map<SprintViewModel>(this.sprintService.GetById(task.SprintId ?? 0));
                    response.Model = task;
                }
                else
                {
                    response.IsSuccess = false;
                    response.Title = "Error!";
                    response.Message = "Something wrong, check log";
                }
            }
            else
            {
                response.IsSuccess = false;
                response.Title = "Error!";
                response.Message = string.Join("<br/>", this.ModelState.Keys.SelectMany(key => this.ModelState[key].Errors.Select(x => x.ErrorMessage)));
            }

            return this.Json(response);
        }

        // GET: Task/Delete/5
        public ActionResult Delete(int id)
        {
            this.ViewData["Title"] = "Delete task";

            var item = this.service.GetById(id);
            var model = this.mapper.Map<CreateEditTaskViewModel>(item);
            model.TechnologyFilterViewModels = this.mapper.Map<List<TechnologyFilterViewModel>>(this.techFilterService.Get(taskModel => taskModel.TaskId == id));

            return this.PartialView("~/Views/Task/Delete.cshtml", model);
        }

        // POST: Task/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(CreateEditTaskViewModel model)
        {
            var response = new TaskResponse();

            var item = this.service.GetById(model.TaskId);
            var list = this.techFilterService.Get(taskModel => taskModel.TaskId == model.TaskId).ToList();

            for (int i = 0; i < list?.Count(); i++)
            {
                this.techFilterService.Delete(list[i].TechnologyFilterId);
            }

            if (this.service.Delete(item.TaskId))
            {
                response.IsSuccess = true;
                response.Title = "Success!";
                response.Message = "Task delete successful";
            }
            else
            {
                response.Title = "Error!";
                response.IsSuccess = false;
                response.Message = "Something wrong, check log";
            }

            return this.Json(response);
        }
    }
}