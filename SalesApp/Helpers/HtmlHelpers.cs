﻿namespace SalesApp.Helpers
{
    using System;
    using System.Collections;
    using System.Reflection;

    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Routing;

    public static class HtmlHelpers
    {

        public static string IsSelected(this IHtmlHelper html, string controller = null, string action = null, string cssClass = null)
        {
            if (String.IsNullOrEmpty(cssClass))
                cssClass = "active";

            string currentAction = (string)html.ViewContext.RouteData.Values["action"];
            string currentController = (string)html.ViewContext.RouteData.Values["controller"];

            if (String.IsNullOrEmpty(controller))
                controller = currentController;

            if (String.IsNullOrEmpty(action))
                action = currentAction;

            return controller == currentController && action == currentAction ?
                       cssClass : String.Empty;
        }

        public static string PageClass(this IHtmlHelper htmlHelper)
        {
            string currentAction = (string)htmlHelper.ViewContext.RouteData.Values["action"];
            return currentAction;
        }

        public static RouteValueDictionary ToRouteValueDictionaryWithCollection(this RouteValueDictionary routeValues)
        {
            var newRouteValues = new RouteValueDictionary();

            foreach (var key in routeValues.Keys)
            {
                object value = routeValues[key];

                if (value is IEnumerable && !(value is string))
                {
                    int index = 0;
                    foreach (object val in (IEnumerable)value)
                    {
                        if (val is string || val.GetType().GetTypeInfo().IsPrimitive)
                        {
                            newRouteValues.Add(String.Format("{0}[{1}]", key, index), val);
                        }
                        else
                        {
                            var properties = val.GetType().GetProperties();
                            foreach (var propInfo in properties)
                            {
                                newRouteValues.Add(
                                    String.Format("{0}[{1}].{2}", key, index, propInfo.Name),
                                    propInfo.GetValue(val));
                            }
                        }
                        index++;
                    }
                }
                else
                {
                    if (value != null)
                    {
                        var properties = value.GetType().GetProperties();
                        if (properties.Length > 0)
                        {
                            foreach (var prop in properties)
                            {
                                if (prop.PropertyType.GetTypeInfo().IsEnum)
                                {

                                    newRouteValues.Add(
                                        String.Format("{0}.{1}", key, prop.Name), prop.GetValue(value));
                                }
                            }
                        }
                        else
                        {
                            newRouteValues.Add(key, value);
                        }
                    }
                }
            }

            return newRouteValues;
        }
    }
}