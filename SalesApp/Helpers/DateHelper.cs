﻿namespace SalesApp.Helpers
{
    using System;
    using System.Globalization;

    public class DateHelper
    {
        public static DateTime? ParseDate(string s)
        {
            DateTime dateTime = DateTime.MinValue;

            if (DateTime.TryParseExact(s, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out dateTime))
            {
                if (dateTime == DateTime.MinValue)
                {
                    return null;
                }

                return dateTime;
            }

            return null;
        }
    }
}