﻿// Init datatable with sprints on page when document load
$(document).ready(function () {

    $('#tableSprint').DataTable({

        columnDefs: [
            { "targets": [0] }
        ],
        ordering: false,
        dom: '<"html5buttons"B>lTfgitp',
        buttons: [
            { extend: 'copy' },
            { extend: 'csv' },
            { extend: 'excel', title: 'ExampleFile' },
            { extend: 'pdf', title: 'ExampleFile' },

            {
                extend: 'print',
                customize: function (win) {
                    $(win.document.body).addClass('white-bg');
                    $(win.document.body).css('font-size', '10px');

                    $(win.document.body).find('table')
                        .addClass('compact')
                        .css('font-size', 'inherit');
                }
            }
        ]
    });
});

// Init custom toasts on page
toastr.options = {
    closeButton: true,
    progressBar: false,
    preventDuplicates: true,
    positionClass: "toast-top-center",
    onclick: null,
    showDuration: "4000",
    hideDuration: "1000",
    timeOut: "7000",
    extendedTimeOut: "1000",
    showEasing: "swing",
    hideEasing: "linear",
    showMethod: "fadeIn",
    hideMethod: "fadeOut"
};

// Form callback methods
function onSuccessCreate(json) {
    if (JSON.parse(json.isSuccess)) {
        toastr.success(json.message, json.title);
        $('#tableSprint').DataTable().row.addByPos([
            json.model.name, moment(json.model.from, "YYYY-MM-DD").format("dddd, MMMM DD, YYYY"), moment(json.model.to, "YYYY-MM-DD").format("dddd, MMMM DD, YYYY"),
            '<div class="text-right footable-visible footable-last-column">' +
            '<div class="btn-group">' +
            '<a class="btn-primary btn btn-xs" href="/Sprint/Details/' + json.model.sprintId + '">View</a>' +
            '<button class="btn-warning btn btn-xs edit-button" id="edit_sprint_' + json.model.sprintId + '">Edit</button>' +
            '<button class="btn-danger btn btn-xs delete-button" id="delete_sprint_' + json.model.sprintId + '">Delete</button>' +
            '</div>' +
            '</div>'
        ], 1, "sprint_" + json.model.sprintId);

        $('#modal-form').modal('hide');
    } else {
        toastr.error(json.message, json.title);
    }
}

function onSuccessEdit(json) {
    if (JSON.parse(json.isSuccess)) {
        toastr.success(json.message, json.title);
        $("#sprint_" + json.model.sprintId + " > :nth-child(1)").text(json.model.name);
        $("#sprint_" + json.model.sprintId + " > :nth-child(2)").text(moment(json.model.from, "YYYY-MM-DD").format("dddd, MMMM DD, YYYY"));
        $("#sprint_" + json.model.sprintId + " > :nth-child(3)").text(moment(json.model.to, "YYYY-MM-DD").format("dddd, MMMM DD, YYYY"));

        $('#modal-form').modal('hide');
    } else {
        toastr.error(json.message, json.title);
    }
}

function onSuccessDelete(json) {
    if (JSON.parse(json.isSuccess)) {
        toastr.success(json.message, json.title);
        $('#tableSprint').DataTable().row($("#sprint_" + json.model.sprintId))
            .remove()
            .draw(false);

        $('#modal-form').modal('hide');
    } else {
        toastr.error(json.message, json.title);
    }
}

function onBegin() {
    $("#Name").prop('disabled', true);
    $("#From").prop('disabled', true);
    $("#To").prop('disabled', true);
    $("#submitButton").prop('disabled', true);

    $("#spiner").prop('hidden', false);
}

function onComplete() {
    $("#Name").prop('disabled', false);
    $("#From").prop('disabled', false);
    $("#To").prop('disabled', false);
    $("#submitButton").prop('disabled', false);

    $("#spiner").prop('hidden', true);
}

function onFailure() {
    $("#Name").prop('disabled', false);
    $("#From").prop('disabled', false);
    $("#To").prop('disabled', false);
    $("#submitButton").prop('disabled', false);

    $("#spiner").prop('hidden', true);

    toastr.error("Something wrong, please try again later", "Error while sending request");
}

// Event methods create, edit, delete methods 
$("#modalOpenForm").on("click", function () {
    $.ajax({
        url: "/Sprint/Create",
        type: "GET",
        dataType: "html",
        success: function (data) {
            $('#modalCreateEdit').html(data);
        },
        error: function () {
            toastr.error("Something wrong, please try again later", "Error while sending request");
        }
    });
});

$("#sprintTable").on("click", ".edit-button",function () {
    var data = 'id=' + $(this).attr('id').replace('edit_sprint_', '');
    $.ajax({
        url: "/Sprint/Edit",
        type: "GET",
        dataType: "html",
        data: data,
        success: function (data) {
            $('#modal-form').modal('show');
            $('#modalCreateEdit').html(data);
        },
        error: function () {
            toastr.error("Something wrong, please try again later", "Error while sending request");
        }
    });
});

$("#sprintTable").on("click", ".delete-button", function () {
    var data = 'id=' + $(this).attr('id').replace('delete_sprint_', '');
    $.ajax({
        url: "/Sprint/Delete",
        type: "GET",
        dataType: "html",
        data: data,
        success: function (data) {
            $('#modal-form').modal('show');
            $('#modalCreateEdit').html(data);
        },
        error: function () {
            toastr.error("Something wrong, please try again later", "Error while sending request");
        }
    });
});

// Custom adding in datatable
jQuery.fn.dataTable.Api.register('row.addByPos()', function (data, index, id) {

    var rowNode = this.row.add(data).draw(true).node();
    $(rowNode).attr("id", id);
    $(rowNode).attr("class", "gradeX");

    this.page('last').draw(false);

});