﻿$(document).ready(function () {
    $("#add_version").on('click',
        function () {

            var tech = $("#TechnologyViewModels option:selected").text();
            var tecId = $("#TechnologyViewModels option:selected").val();

            $("#tech_version").append(
                '<div class="form-group"><div class="col-lg-4 col-lg-offset-1"><input type="hidden" name="TechnologyFilterViewModels[' +
                countVersion +
                '].TechnologyId" value="' +
                tecId +
                '"><input disabled name="tech_name" class="form-control" type="text" value="' +
                tech +
                '"></div><div class="col-lg-6"><input  id="version_' +
                countVersion +
                '" class="bootstrap-tagsinput form-control version-input" type="text" data-role="tagsinput" value=""><input type="hidden" name="TechnologyFilterViewModels[' +
                countVersion +
                '].VersionJson"></div></div>');



            $("#version_" + countVersion).tagsinput();
            var itemToDisable = $("option:contains('" + tech + "')");
            itemToDisable.attr("disabled", true);

            $("#TechnologyViewModels").find('option:selected').removeAttr('selected');
            $('#TechnologyViewModels').trigger("chosen:updated");

            countVersion++;
        });

    $("#remove_version").on('click', function () {
        countVersion--;
        var tech = $("#tech_version > :nth-last-child(1)").find('input[name="tech_name"]').val();
        $("#tech_version > :nth-last-child(1)").remove();
        var itemToVisible = $("option:contains('" + tech + "')");
        itemToVisible.attr("disabled", false);
        $('#TechnologyViewModels').trigger("chosen:updated");
    });

    $("#submitButton").on('click', function () {
        $('.version-input').each(function () {
            $("input[name='TechnologyFilterViewModels[" +
                $(this).attr('id').replace("version_", "") +
                "].VersionJson']").val(JSON.stringify($(this).tagsinput('items')));
        });
    })

    $('input[name="tech_name"').each(function () {
        var itemToDisable = $("option:contains('" + $(this).val() + "')");
        itemToDisable.attr("disabled", true);
    });
    $('#TechnologyViewModels').trigger("chosen:updated");
});