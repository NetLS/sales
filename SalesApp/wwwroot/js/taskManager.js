﻿toastr.options = {
    closeButton: true,
    progressBar: false,
    preventDuplicates: true,
    positionClass: "toast-top-center",
    onclick: null,
    showDuration: "4000",
    hideDuration: "1000",
    timeOut: "7000",
    extendedTimeOut: "1000",
    showEasing: "swing",
    hideEasing: "linear",
    showMethod: "fadeIn",
    hideMethod: "fadeOut"
};

$("#modalOpenForm").on("click", function () {
    var id = $("#currSprintId").val();
    if (id == undefined) {
        id = 0;
    }
    var data = 'id=' + id;
    $.ajax({
        url: "/Task/Create",
        type: "GET",
        dataType: "html",
        data: data,
        success: function (data) {
            $('#modalCreateEdit').html(data);
        }
    });
});

function onBegin() {
    $("#SampleName").prop('disabled', true);
    $(".chosen-select").attr('disabled', true).trigger("chosen:updated");
    $("#submitButton").prop('disabled', true);

    $("#spiner").prop('hidden', false);
}

function onComplete() {
    $("#SampleName").prop('disabled', false);
    $(".chosen-select").attr('disabled', false).trigger("chosen:updated");
    $("#submitButton").prop('disabled', false);

    $("#spiner").prop('hidden', true);
}

function onFailure() {
    $("#Name").prop('disabled', false);
    $("#From").prop('disabled', false);
    $("#To").prop('disabled', false);
    $("#submitButton").prop('disabled', false);

    $("#spiner").prop('hidden', true);


    toastr.error("Something wrong, please try again later", "Error while sending request");
}

function onSuccessCreate(json) {
    if (JSON.parse(json.isSuccess)) {
        toastr.success(json.message, json.title);
        $('#modal-form').modal('hide');
        location.reload();
    } else {
        toastr.error(json.message, json.title);
    }

}

function onSuccessEdit(json) {
    if (JSON.parse(json.isSuccess)) {
        toastr.success(json.message, json.title);
        $('#modal-form').modal('hide');
        location.reload();
    } else {
        toastr.error(json.message, json.title);
    }

}

function onSuccessDelete(json) {
    if (JSON.parse(json.isSuccess)) {
        toastr.success(json.message, json.title);
        $('#modal-form').modal('hide');
        location.reload();
    } else {
        toastr.error(json.message, json.title);
    }

}

$("#tableFilter").on("click", ".edit-button", function () {
    var data = 'id=' + $(this).attr('id').replace('edit_task_', '');
    $.ajax({
        url: "/Task/Edit",
        type: "GET",
        dataType: "html",
        data: data,
        success: function (data) {
            $('#modal-form').modal('show');
            $('#modalCreateEdit').html(data);
        }
    });

});

$("#tableFilter").on("click", ".delete-button", function () {
    var data = 'id=' + $(this).attr('id').replace('delete_task_', '');
    $.ajax({
        url: "/Task/Delete",
        type: "GET",
        dataType: "html",
        data: data,
        success: function (data) {
            $('#modal-form').modal('show');
            $('#modalCreateEdit').html(data);
        }
    });
});