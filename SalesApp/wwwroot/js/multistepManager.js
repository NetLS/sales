﻿$(document).ready(function () {

    // Toolbar extra buttons
    var btnFinish = $('<button type="button"></button>').text('Finish')
        .addClass('btn btn-info')
        .on('click', function () {
            $('.version-input').each(function () {
                $("input[name='TechnologyFilterViewModels[" +
                    $(this).attr('id').replace("version_", "") +
                    "].VersionJson']").val(JSON.stringify($(this).tagsinput('items')));
            });
            $("#filtersJson").val('{ "location" : ' + JSON.stringify($("#location").tagsinput('items')) + ' , "industry" : ' + JSON.stringify($("#industry").tagsinput('items')) + ' , }');
            $("#modalSubmit").submit();

        });
    var btnCancel = $('<button></button>').text('Cancel')
        .addClass('btn btn-danger')
        .on('click', function () {
            $('#smartwizard').smartWizard("reset");
            $('#myForm').find("input, textarea").val("");
        });



    // Smart Wizard
    $('#smartwizard').smartWizard({
        selected: 0,
        theme: 'dots',
        transitionEffect: 'fade',
        toolbarSettings: {
            toolbarPosition: 'bottom',
            toolbarExtraButtons: [btnFinish, btnCancel]
        },
        anchorSettings: {
            markDoneStep: true,
            markAllPreviousStepsAsDone: true,
            removeDoneStepOnNavigateBack: true,
            enableAnchorOnDoneStep: true
        },
        showStepURLhash: false,
    });

    $("#smartwizard").on("leaveStep", function (e, anchorObject, stepNumber, stepDirection) {
        return true;
    });

    $("#smartwizard").on("showStep", function (e, anchorObject, stepNumber, stepDirection) {
        // Enable finish button only on last step
        if (stepNumber == 3) {
            $('.btn-finish').removeClass('disabled');
        } else {
            $('.btn-finish').addClass('disabled');
        }
    });

    $("#add_version").on('click',
        function () {

            var tech = $("#TechnologyViewModels option:selected").text();
            var tecId = $("#TechnologyViewModels option:selected").val();

            $("#tech_version").append(
                '<div class="form-group"><div class="col-lg-4 col-lg-offset-1"><input type="hidden" name="TechnologyFilterViewModels[' +
                countVersion +
                '].TechnologyId" value="' +
                tecId +
                '"><input disabled name="tech_name" class="form-control" type="text" value="' +
                tech +
                '"></div><div class="col-lg-6"><input  id="version_' +
                countVersion +
                '" class="bootstrap-tagsinput form-control version-input" type="text" data-role="tagsinput" value=""><input type="hidden" name="TechnologyFilterViewModels[' +
                countVersion +
                '].VersionJson"></div></div>');



            $("#version_" + countVersion).tagsinput();
            var itemToDisable = $("option:contains('" + tech + "')");
            itemToDisable.attr("disabled", true);

            $("#TechnologyViewModels").find('option:selected').removeAttr('selected');
            $('#TechnologyViewModels').trigger("chosen:updated");

            countVersion++;
        });

    $("#remove_version").on('click', function () {
        countVersion--;
        var tech = $("#tech_version > :nth-last-child(1)").find('input[name="tech_name"]').val();
        $("#tech_version > :nth-last-child(1)").remove();
        var itemToVisible = $("option:contains('" + tech + "')");
        itemToVisible.attr("disabled", false);
        $('#TechnologyViewModels').trigger("chosen:updated");
    });

    $('input[name="tech_name"').each(function () {
        var itemToDisable = $("option:contains('" + $(this).val() + "')");
        itemToDisable.attr("disabled", true);
    });
    $('#TechnologyViewModels').trigger("chosen:updated");
});