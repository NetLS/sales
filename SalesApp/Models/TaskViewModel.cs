﻿namespace SalesApp.Models
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    using Contract.Infrastructure.DataContracts.Sprint;

    public class TaskViewModel
    {
        public int TaskId { get; set; }

        public int? SprintId { get; set; }

        public string TaskName { get; set; }

        public int? TargetId { get; set; }

        public string FiltersJson { get; set; }

        public string TargetName => new Target().Targets[this.TargetId ?? 0];

        public int Status { get; set; }

        public int CountUrlResultsAll { get; set; }

        public int Limit { get; set; }

        public TaskStatus TaskStatus
        {
            get => (TaskStatus)this.Status;
            set => this.Status = (int)value;
        }

        public List<TechnologyFilterViewModel> TechnologyFilterViewModels { get; set; }

        public List<TechnologyViewModel> TechnologyViewModels { get; set; }

        public List<PaginatorViewModel> PaginatorModels { get; set; }

        public List<UrlResultViewModel> UrlResultViewModels { get; set; }

        public SprintViewModel SprintModel { get; set; }
    }
}