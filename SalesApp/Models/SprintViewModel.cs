﻿namespace SalesApp.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    using Contract.Infrastructure.DataContracts.Sprint;

    public class SprintViewModel
    {
        public int SprintId { get; set; }

        public string Name { get; set; }

        public DateTime? From { get; set; }

        public  DateTime? To { get; set; }

        public List<TaskViewModel> TaskModels { get; set; }
    }
}