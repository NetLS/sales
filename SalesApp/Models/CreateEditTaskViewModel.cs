﻿namespace SalesApp.Models
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    using Contract.Infrastructure.DataContracts.Sprint;

    public class CreateEditTaskViewModel
    {
        public int TaskId { get; set; }

        [Required(ErrorMessage = "Sprint name is Required")]
        [Range(1, int.MaxValue, ErrorMessage = "Please select Sprint")]
        [Display(Name = "Select sprint")]
        public int? SprintId { get; set; }

        [Required(ErrorMessage = "Task name is Required")]
        [Display(Name = "Task name")]
        public string TaskName { get; set; }

        [Range(1, int.MaxValue, ErrorMessage = "Please select Target")]
        [Display(Name = "Target")]
        public int? TargetId { get; set; }

        public string FiltersJson { get; set; }

        public int Status { get; set; }

        public TaskStatus TaskStatus
        {
            get => (TaskStatus)this.Status;
            set => this.Status = (int)value;
        }

        public Dictionary<int, string> Targets => new Target().Targets;

        public List<TechnologyFilterViewModel> TechnologyFilterViewModels { get; set; }

        [Display(Name = "Technology")]
        public List<TechnologyViewModel> TechnologyViewModels { get; set; }

        public List<SprintViewModel> SprintViewModels { get; set; }
    }
}