﻿namespace SalesApp.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    using Microsoft.AspNetCore.Routing;

    public class UrlResultFilterViewModel
    {
        public UrlResultFilterViewModel()
        {
            this.SiteResponsiveModel = new SiteResponsiveViewModel();
        }

        public int count { get; set; }

        [Display(Name = "Result on page")]
        public int? limit { get; set; }

        public int? offset { get; set; }

        public List<UrlResultViewModel> ResultViewModels { get; set; }

        [Display(Name = "Technologies")]
        public List<TechnologyViewModel> TechnologyViewModels { get; set; }

        public List<TechnologyFilterViewModel> TechnologyFilterViewModels { get; set; }

        [Display(Name = "Min count links")]
        public int? CountUrlStart { get; set; }

        [Display(Name = "Max count links")]
        public int? CountUrlEnd { get; set; }

        public DateTime? CheckDateStart { get; set; }

        public DateTime? CheckDateEnd { get; set; }

        public SiteResponsiveViewModel SiteResponsiveModel { get; set; }

        public List<TaskViewModel> TaskViewModels { get; set; }

        [Display(Name = "Tasks")]
        public List<int> TaskIdList { get; set; }

        public List<string> UrlList
        {
            get
            {
                return new List<string>(this.Urls?.Split(',') ?? new string[0]);
            }
            set
            {
                this.Urls = string.Join(",", value);
            }
        }

        [Display(Name = "Urls filter")]
        public string Urls { get; set; }

        public RouteValueDictionary RouteValues
        {
            get
            {
                var rvd = new RouteValueDictionary();
                rvd["count"] = this.count;
                rvd["limit"] = this.limit;
                rvd["offset"] = this.offset;
                rvd["TechnologyFilterViewModels"] = this.TechnologyFilterViewModels?.ToArray();
                rvd["CountUrlStart"] = this.CountUrlStart;
                rvd["CountUrlEnd"] = this.CountUrlEnd;
                rvd["CheckDateStart"] = this.CheckDateStart;
                rvd["CheckDateEnd"] = this.CheckDateEnd;
                rvd["UrlList"] = this.UrlList;
                rvd["Urls"] = this.Urls;
                rvd["TaskIdList"] = this.TaskIdList;
                rvd["SiteResponsiveModel"] = this.SiteResponsiveModel;
                return rvd;
            }
        }
    }
}