﻿namespace SalesApp.Models
{
    using System;
    using System.Collections.Generic;

    public class SiteDataViewModel
    {
        public int SiteDataId { get; set; }

        public int? UrlId { get; set; }

        public DateTime? Check { get; set; }

        public List<TechnologySiteDataViewModel> TechnologySiteDataModels { get; set; }

        public List<SiteResponsiveViewModel> ResponsiveViewModels { get; set; }
    }
}