﻿namespace SalesApp.Models
{
    using System.Collections.Generic;

    using Contract.Infrastructure.DataContracts.Sprint;

    public class TechnologySiteDataViewModel
    {
        public int TechnologySiteDataId { get; set; }

        public int? SiteDataId { get; set; }

        public int? TechnologyId { get; set; }

        public string Version { get; set; }

        public TechnologyViewModel TechnologyModel { get; set; }

        public SiteDataViewModel SiteDataModel { get; set; }

    }
}