﻿namespace SalesApp.Models.Response
{
    public class BaseResponse
    {
        public bool IsSuccess { get; set; }

        public string Title { get; set; }

        public string Message { get; set; }
    }
}