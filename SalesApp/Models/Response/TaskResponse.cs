﻿namespace SalesApp.Models.Response
{
    public class TaskResponse : BaseResponse
    {
        public TaskViewModel Model { get; set; }
    }
}