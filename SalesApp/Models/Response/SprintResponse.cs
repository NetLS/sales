﻿namespace SalesApp.Models
{
    using SalesApp.Models.Response;

    public class SprintResponse : BaseResponse
    {
        public SprintViewModel Model { get; set; }
    }
}