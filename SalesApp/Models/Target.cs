﻿namespace SalesApp.Models
{
    using System.Collections.Generic;

    public class Target
    {
        public Dictionary<int, string> Targets =
            new Dictionary<int, string>() { { 0, "none" }, { 1, "data.com" }, { 2, "google.com" } };
    }
}