﻿namespace SalesApp.Models
{
    using System.ComponentModel.DataAnnotations;

    public class CreateEditSprintViewModel
    {
        public int SprintId { get; set; }

        [Required(ErrorMessage = "Sprint name is Required")]
        [Display(Name = "Sprint name")]
        public string Name { get; set; }

        [RegularExpression(@"^(0[1-9]|[12][0-9]|3[01])[- \/.](0[1-9]|1[012])[- \/.](19|20)\d\d$", ErrorMessage = "Start date should be in 'dd/mm/yyyy' format")]
        [Display(Name = "Start date of sprint")]
        public string From { get; set; }

        [RegularExpression(@"^(0[1-9]|[12][0-9]|3[01])[- \/.](0[1-9]|1[012])[- \/.](19|20)\d\d$", ErrorMessage = "Finish date should be in 'dd/mm/yyyy' format")]
        [Display(Name = "Finish date of sprint")]
        public string To { get; set; }
    }
}