﻿namespace SalesApp.Models
{
    using System;
    using System.Collections.Generic;

    public class UrlViewModel
    {
        public int UrlId { get; set; }

        public string UrlLink { get; set; }

        public DateTime? LastCheck { get; set; }

        public List<SiteDataViewModel> SiteDataModels { get; set; }

        public List<UrlResultViewModel> UrlResultViewModels { get; set; }
    }
}