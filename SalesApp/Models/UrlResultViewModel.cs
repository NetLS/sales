﻿namespace SalesApp.Models
{
    using System.Collections.Generic;

    using Contract.Infrastructure.DataContracts.SiteAnalyzer;
    using Contract.Infrastructure.DataContracts.Sprint;

    public class UrlResultViewModel
    {
        public int UrlResultId { get; set; }

        public int? TaskId { get; set; }

        public int? PaginatorId { get; set; }

        public int? SiteDataId { get; set; }

        public string UrlLink { get; set; }

        public int? Status { get; set; }

        public int? UrlId { get; set; }

        public bool? IsMatch { get; set; }

        public int? CountUrl { get; set; }

        public SiteStatus SiteStatus
        {
            get => (SiteStatus)(this.Status ?? 0);
            set => this.Status = (int)value;
        }

        public UrlViewModel UrlViewModel { get; set; }

        public SiteDataViewModel SiteDataModel { get; set; }

        public TaskViewModel TaskViewModel { get; set; }

        public PaginatorViewModel PaginatorViewModel { get; set; }
    }
}