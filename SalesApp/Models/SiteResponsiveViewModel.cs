﻿namespace SalesApp.Models
{
    using System.ComponentModel.DataAnnotations.Schema;

    using Contract.Infrastructure.DataContracts.SiteAnalyzer;

    public class SiteResponsiveViewModel
    {
        public int SiteResponsiveId { get; set; }

        public int SiteDataId { get; set; }

        public DocWidthStatus DocWidthStatus { get; set; }

        public FontSizeStatus FontSizeStatus { get; set; }

        public TouchScreenStatus TouchScreenStatus { get; set; }

        public ViewPortStatus PortStatus { get; set; }

        public AlertStatus AlertStatus { get; set; }

        public FlashStatus FlashStatus { get; set; }

        public string ScreenShot { get; set; }
    }
}