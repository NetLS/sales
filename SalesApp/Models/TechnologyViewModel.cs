﻿namespace SalesApp.Models
{
    public class TechnologyViewModel
    {
        public int TechnologyId { get; set; }

        public string TechnologyName { get; set; }

        public string Icon { get; set; }

        public string Website { get; set; }
    }
}