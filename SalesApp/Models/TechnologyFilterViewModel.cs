﻿namespace SalesApp.Models
{
    public class TechnologyFilterViewModel
    {
        public int TechnologyFilterId { get; set; }

        public int? TaskId { get; set; }

        public int? TechnologyId { get; set; }

        public string VersionJson { get; set; }

    }
}