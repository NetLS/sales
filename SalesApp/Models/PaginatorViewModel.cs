﻿namespace SalesApp.Models
{
    using System.Collections.Generic;

    public class PaginatorViewModel
    {
        public int PaginatorId { get; set; }

        public int FilterId { get; set; }

        public string PaginatorUrl { get; set; }

        public List<UrlViewModel> UrlModels { get; set; }

    }
}